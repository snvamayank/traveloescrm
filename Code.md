{/\* <Stack>
<Stack
sx={{
                      borderWidth: 1,
                      p: 3,
                      m: 2,
                    }} >
<Label sx={{ textTransform: "uppercase" }}>From Date</Label>
<TextField
name="fromDate"
type=""
value={fareRuleObj.fromDate}
id="fromDate"
placeholder=""
onChange={(e) => handleFareObjChange(e, "fromDate")}
sx={{ margin: 1 }}
/>
<Label sx={{ textTransform: "uppercase" }}>To Date</Label>
<TextField
name="toDate"
value={fareRuleObj.toDate}
type=""
id="toDate"
onChange={(e) => handleFareObjChange(e, "toDate")}
sx={{ margin: 1 }}
/>
<Label sx={{ textTransform: "uppercase" }}>
Markup Type
</Label>
<Stack
sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-around",
                        alignItems: "center",
                      }} >
<LabelStyle>%</LabelStyle>
<input
name="fareType"
type="radio"
value={"%"}
checked={fareRuleObj.rule === "%"}
id="farerType"
onChange={(e) => handleFareObjChange(e, "rule")}
sx={{ margin: 1 }}
/>
</Stack>
<Stack
sx={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-around",
                        alignItems: "center",
                      }} >
<LabelStyle>FLAT</LabelStyle>
<input
name="fareType"
type="radio"
checked={fareRuleObj.rule === "FLAT"}
value={"FLAT"}
id="fareType"
placeholder="Fare Type"
onChange={(e) => handleFareObjChange(e, "rule")}
sx={{ margin: 1 }}
/>
</Stack>

                    {fareRuleObj.length === 0 ? (
                      " "
                    ) : (
                      <>
                        {apiProvider.map((item, i) => (
                          <Stack>
                            <Label sx={{ textTransform: "uppercase" }}>
                              Supplier
                            </Label>
                            <TextField
                              value={item.name}
                              name="name"
                              id="name"
                              disabled={true}
                              placeholder="Provider"
                              sx={{ margin: 1 }}
                              onChange={(e) => handleInput(e, i)}
                            />
                            <Label sx={{ textTransform: "uppercase" }}>
                              Allowed Airline Codes (ALL,AI,6E)
                            </Label>
                            <TextField
                              name="code"
                              value={item.code}
                              id="code"
                              placeholder="Airline Code (AI,6E ,G8)"
                              onChange={(e) => handleInput(e, i)}
                              sx={{ margin: 1 }}
                            />
                            <Label sx={{ textTransform: "uppercase" }}>
                              Fare Type (Published,Regular)
                            </Label>
                            <TextField
                              name="fareType"
                              value={item.fareType}
                              id="farerType"
                              placeholder="Fare Type"
                              onChange={(e) => handleInput(e, i)}
                              sx={{ margin: 1 }}
                            />
                            <Label sx={{ textTransform: "uppercase" }}>
                              Airline Markup (6E:400,G8:50,UK:99)
                            </Label>
                            <TextField
                              name="airlineMarkup"
                              value={item.airlineMarkup}
                              id="airlineMarkup"
                              placeholder="Airline Markup"
                              onChange={(e) => handleInput(e, i)}
                              sx={{ margin: 1 }}
                            />
                            <Label sx={{ textTransform: "uppercase" }}>
                              Fare Type Markup
                              (PUBLISHED:199,Regular:299,SMEFares:399)
                            </Label>
                            <TextField
                              name="fareTypeMarkup"
                              value={item.fareTypeMarkup}
                              id="fareTypeMarkup"
                              placeholder="Fare Type Markup"
                              onChange={(e) => handleInput(e, i)}
                              sx={{ margin: 1 }}
                            />
                          </Stack>
                        ))}
                      </>
                    )}
                  </Stack>
                </Stack> */}
