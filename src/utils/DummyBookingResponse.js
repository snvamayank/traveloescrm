const BOOKINGRESP = {
  searchID: "32396",
  bookingID: "RWTFF9191",
  pNR: "PBQV7D",
  isCcv: false,
  ispriceChange: false,
  isTimeChanged: false,
  newPrice: 0,
  bookingStatus: "Ticketed",
  isticketeRun: 0,
  isticketeRunRet: 0,
  responseStatus: {
    status: 1,
    messege: "Success",
    additionalProperties: {},
  },
  additionalProperties: {},
};
export default BOOKINGRESP;
