import React, { useEffect, useState, useRef } from "react";
// import { Disclosure } from "@headlessui/react";
// import { ChevronUpIcon } from "@heroicons/react/solid";
// import Accordion from "react-bootstrap/Accordion";
// import Collapse from "react-bootstrap/Collapse";
import scope from "../../assets/Image/scope.svg";
import clinic from "../../assets/Image/clinic.jpg";
import medical1 from "../../assets/Image/medical1.gif";
import medical2 from "../../assets/Image/medical2.gif";
import MenuItem from "@mui/material/MenuItem";
import medical3 from "../../assets/Image/medical3.gif";
import { styled } from "@material-ui/core/styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faPhone } from "@fortawesome/free-solid-svg-icons";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import personfill from "@iconify/icons-eva/person-fill";
import { Icon } from "@iconify/react";
import {
  Typography,
  Button,
  Card,
  CardContent,
  Stack,
  Box,
  Input,
  TextField,
  Collapse,
} from "@material-ui/core";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";
import useAuth from "src/hooks/useAuth";
import { hostname } from "src/HostName";
import { ValidationForm, TextInput } from "react-bootstrap4-form-validation";
// import { DatePicker } from "@material-ui/lab";
import * as Yup from "yup";
import Dates from "src/Api/StaticApi/Dates";
import { MONTH } from "src/Api/StaticApi/month";
import { YEAR } from "src/Api/StaticApi/year";
import { useSelector } from "react-redux";
import { SelectedData } from "../dashboard/Action";
// import { DatePicker, LocalizationProvider } from "@material-ui/lab";
// import AdapterDateFns from "@mui/lab/AdapterDateFns"; // import Contact from "../../View/Contact";
import { Formik, FormikProvider, useFormik } from "formik";
import moment from "moment";
import Grid from "src/theme/overrides/Grid";

const RootStyle = styled(Card)(({ theme }) => ({
  boxShadow: theme.customShadows.primary.z8,
}));

const DIV = styled(Card)(({ theme }) => ({
  boxShadow: "none",
  textAlign: "center",
  backgroundColor: theme.palette.primary.lighter,
  [theme.breakpoints.up("md")]: {
    textAlign: "left",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
  },
}));

const Second = (props) => {
  const [info, setInfo] = useState(true);
  const [bookingnewemail, setcontinueemail] = useState(false);
  const [open1, setOpen1] = useState(false);
  const [open2, setOpen2] = useState(false);
  const [editDetails, setEditDetails] = useState(false);
  const [third, setthird] = useState(false);
  const [continuebutton, setcontinuebutton] = useState(false);
  const [booking, setbooking] = useState(false);
  const [crmData, setcrmData] = useState([]);
  const [loadCrm, setloadCrm] = useState(false);
  const [payuMethod, setPayuMethod] = useState("");
  const [payBtnActive, setPayBtnActive] = useState(true);
  const [gstCompany, setCompanygst] = useState();
  const [contact, setcontact] = useState(false);
  const [secondcontinuebutton, setsecondcontinuebutton] = useState(false);
  const [baggage, setbaggage] = useState(false);
  const [pay, setpay] = useState(false);
  const [validated, setValidated] = useState(false);
  const [infantTravellers, setInfantTravellers] = useState();
  const [stopValue, setStopValue] = useState(0);
  const [gatewaysName, setGatewaysName] = useState("");
  const [noOfTravellers, setNoOfTravellers] = useState(1);
  const [childrenTravellers, setChildrenTravellers] = useState(0);
  const [count, setCount] = useState(1);
  const [value, setValue] = React.useState(null);
  const [age, setAge] = useState("");
  const [gstnum, setGstnum] = useState("");
  const [gstinfo, setGstinfo] = useState(false);
  const [continueSave, setContinueSave] = useState(true);
  const [confirmdetail, setConfirmdetail] = useState(true);
  const [address, setAddress] = useState(false);
  const Login = localStorage.getItem("LoginData");
  const user = JSON.parse(Login);
  const FlightDetails = props.flightDetails;
  const EngineData = localStorage.getItem("body");
  const data = JSON.parse(EngineData);
  const formRefs = useRef();

  const handleChange = () => {
    setGstinfo(!gstinfo);
  };

  // test
  const [inputList, setInputList] = useState([
    {
      title: "",
      firstName: "",
      middleName: "",
      lastName: "",
      dateOfBirth: "",
      gender: "",
      gstnumber: "",
      companyName: "",
      passportNumber: "",
      issueDate: "",
      expiryDate: "",
      email: "",
      phone: ``,
      issueCountry: "",
      passengerType: "1",
      gstNumber: "",
      gstCompany: "",
    },
  ]);

  const [childinputList, setChildinputList] = useState([
    {
      title: "",
      firstName: "",
      middleName: "",
      lastName: "",
      dateOfBirth: "",
      gender: "",
      gstnumber: "",
      companyName: "",
      passportNumber: "",
      issueDate: "",
      expiryDate: "",
      email: "",
      phone: "",
      issueCountry: "",
      passengerType: "2",
    },
  ]);

  const [infantinputList, setInfantinputList] = useState([
    {
      title: "",
      firstName: "",
      middleName: "",
      lastName: "",
      dateOfBirth: "",
      gender: "",
      gstnumber: "",
      companyName: "",
      passportNumber: "",
      issueDate: "",
      expiryDate: "",
      email: "",
      phone: "",
      issueCountry: "",
      passengerType: "3",
    },
  ]);

  function checkContinue() {
    setGstinfo(false);

    var validRegex =
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    var mobilePhone = /^\d{10}$/;
    var registrationNumber = /^\d{16}$/;
    var companyName = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;

    if (gstinfo == true) {
    } else {
      {
        inputList.map((items, i) => {
          if (
            items.email !== "" &&
            items.phone !== "" &&
            items.email.match(validRegex) &&
            items.phone.match(mobilePhone)
          ) {
            setContinueSave(false);
          } else {
            setContinueSave(true);
          }
        });
      }
    }
  }
  function confirmDetailPassenger() {
    var Name = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;

    {
      inputList.map((items, i) => {
        if (
          items.firstName !== "" &&
          items.lastName !== "" &&
          items.firstName.match(Name) &&
          items.lastName.match(Name)
        ) {
          setConfirmdetail(false);
        } else {
          setConfirmdetail(true);
        }
      });
    }
  }
  const handleInputChangedetails = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
    // checkContinue();
  };

  const AddInputChildren = (e, index) => {
    const { name, value } = e.target;
    const list = [...childinputList];
    list[index][name] = value;
    setChildinputList(list);
  };

  const AddInputInfant = (e, index) => {
    const { name, value } = e.target;
    const list = [...infantinputList];
    list[index][name] = value;
    setInfantinputList(list);
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputList];

    if (name === "phone") {
      list[index][name] = value;
      setInputList(list);
    } else {
      list[index][name] = value;
      setInputList(list);
    }

    list[index][name] = value;
    setInputList(list);
    checkContinue();
    confirmDetailPassenger();
  };
  function checkReg() {
    // var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    var regnumber = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/;
    var companyName = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;

    // if (gstinfo == true) {
    {
      inputList.map((items, i) => {
        if (
          items.gstNumber !== "" &&
          items.gstNumber.match(regnumber) &&
          items.gstCompany !== "" &&
          items.gstCompany.match(companyName)
        ) {
          setContinueSave(false);
        } else {
          setContinueSave(true);
        }
      });
    }
  }

  const handleInputChangeReg = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputList];

    list[index][name] = value;
    setInputList(list);
    checkReg();
  };
  function saveCustomerDetail() {
    // e.preventDefault();
    // setEditDetails(false);
    // setInfo(!info);
    // setOpen2(true);
    // setcontact(true);
    // setcontinuebutton(false);
    // setsecondcontinuebutton(true);
  }

  function saveTravellersDetail() {
    setthird(true);
    setOpen2(true);
    setcontact(false);
  }

  const handleAddTraveller = () => {
    if (inputList.length !== data.adult) {
      setInputList([
        ...inputList,
        {
          title: "",
          firstName: "",
          middleName: "",
          lastName: "",
          dateOfBirth: "",
          gender: "",
          gstnumber: "",
          companyName: "",
          passportNumber: "",
          issueDate: "",
          expiryDate: "",
          email: "",
          phone: ``,
          issueCountry: "",
          passengerType: "1",
          gstNumber: "",
          gstCompany: "",
        },
      ]);
    } else {
      return null;
    }
  };

  // handle input change
  // const AdultInputChange = (e, index) => {
  //   const { name, value } = e.target;
  //   const list = [...inputList];
  //   list[index][name] = value;
  //   setInputList(list);
  // };

  // handle click event of the Remove button
  // const handleRemoveClick = (index) => {
  //   const list = [...inputList];
  //   list.splice(index, 1);
  //   setInputList(list);
  // };

  // // done the value
  // const Done = (e, index) => {
  //   const { name, value } = e.target;
  //   const list = [...inputList];

  //   list[index][name] = "";
  //   setInputList(list);
  // };

  // const AddAdult = (cnt) => {
  //   setInputList([
  //     ...inputList,
  //     {
  //       firstName: "",
  //       middleName: "",
  //       lastName: "",
  //       weight: "",
  //       addBagage: "",
  //       gender: "",
  //       passengerType: 1,
  //       dob: "",
  //     },
  //   ]);
  //   setNoOfTravellers(cnt - 1);
  // };
  // const AddChildren = (cnt) => {
  //   setChildrenInputList([
  //     ...childrenInputList,
  //     {
  //       firstName: "",
  //       middleName: "",
  //       lastName: "",

  //       weight: "",
  //       addBagage: "",
  //       gender: "",
  //       passengerType: 2,
  //       dob: "",
  //     },
  //   ]);
  //   setChildrenTravellers(cnt - 1);
  // };
  // const AddInfant = (cnt) => {
  //   setInfantList([
  //     ...infantList,
  //     {
  //       firstName: "",
  //       middleName: "",
  //       lastName: "",
  //       weight: "",
  //       gender: "",
  //       passengerType: 3,
  //       dob: "",
  //     },
  //   ]);

  //   setInfantTravellers(cnt - 1);
  // };

  const DebitBalance = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      userid: user.userid,
      amount: props.flight.fare.grandOrgTotal,
      type: "dr",
      method: "Booking",
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    // fetch(`${hostname}agent/transaction`, requestOptions)
    //   .then((response) => response.json())
    //   .then((result) => console.log("result", result))
    //   .catch((error) => console.log("error", error));
  };

  const TotalTravllers = data.adult + data.children + data.infant;

  useEffect(() => {
    setNoOfTravellers(TotalTravllers);
  }, []);

  var Passenger = [];

  if (data.adult !== 0 && data.children !== 0 && data.infant !== 0) {
    Passenger.push(...inputList);
    Passenger.push(...childinputList);
    Passenger.push(...infantinputList);
  } else if (data.adult !== 0 && data.children !== 0) {
    Passenger.push(...inputList);
    Passenger.push(...childinputList);
  } else if (data.adult !== 0 && data.infant !== 0) {
    Passenger.push(...inputList);
    Passenger.push(...infantinputList);
  } else if (data.adult !== 0) {
    Passenger.push(...inputList);
  }

  /* ==================================================== */
  const adutlSingleArray = [];
  if (inputList.length === 1) {
    let newDate =
      inputList[0].year + "-" + inputList[0].month + "-" + inputList[0].date;

    const mapped = inputList.map((element) => ({
      ...element,
      dateOfBirth: newDate,
      additionalProperties: {},
    }));
    adutlSingleArray.push(...mapped);
  } else if (inputList.length >= 1) {
    let newDate = inputList.map(
      (item) => item.year + "-" + item.month + "-" + item.date
    );

    const mapped = inputList.map((element) => ({
      ...element,
      dateOfBirth: newDate,
    }));
  }

  const handleAddChildTraveller = () => {
    if (childinputList.length !== data.children) {
      setChildinputList([
        ...childinputList,
        {
          title: "",
          firstName: "",
          middleName: "",
          lastName: "",
          dateOfBirth: "",
          gender: "",
          gstnumber: "",
          companyName: "",
          passportNumber: "",
          issueDate: "",
          expiryDate: "",
          email: "",
          phone: "",
          issueCountry: "",
          passengerType: "2",
        },
      ]);
    } else {
      return null;
    }
  };

  const handleAddinfantTraveller = () => {
    if (infantinputList.length !== data.infant) {
      setInfantinputList([
        ...infantinputList,
        {
          title: "",
          firstName: "",
          middleName: "",
          lastName: "",
          dateOfBirth: "",
          gender: "",
          gstnumber: "",
          companyName: "",
          passportNumber: "",
          issueDate: "",
          expiryDate: "",
          email: "",
          phone: "",
          issueCountry: "",
          passengerType: "2",
        },
      ]);
    } else {
      return null;
    }
  };

  const traveFormValidation = Yup.array(
    Yup.object({
      email: Yup.string()
        .required("Email is required")
        .email("Email is invalid"),
      phone: Yup.number()
        .positive("A phone number can't start with a minus")
        .integer("A phone number can't include a decimal point")
        .min(10, "too short")
        .max(10, "too long")
        .required("A phone number is required"),
      gstNumber: Yup.number()
        .integer("A phone number can't include a decimal point")
        .required("A Registration number is required")
        .min(15, "too short")
        .max(15, "too long"),
      companyName: Yup.string().required("Company name is required"),
      firstName: Yup.string().required("first name is required"),
      lastName: Yup.string().required("last name is required"),
      gender: Yup.string().required("Gender is Required"),
      dateOfBirth: Yup.string().required("date of birth is required"),
    })
  );
  const formik = useFormik({
    initialValues: {
      firstName: "",
      middleName: "",
      lastName: "",
      dateOfBirth: "",
      gender: "",
      companyName: "",
      gstnumber: "",
      passportNumber: "",
      issueDate: "",
      expiryDate: "",
      email: "",
      phone: ``,
      issueCountry: "",
      passengerType: "1",
      gstNumber: "",
      gstCompany: "",
    },
    validationSchema: traveFormValidation,
    onSubmit: async (values, { setErrors, setSubmitting, resetForm }) => {
      try {
        // await fakeRequest(500);
        resetForm();
        // handleClosePreview();
        setSubmitting(false);
        // enqueueSnackbar("Post success", { variant: "success" });
      } catch (error) {
        console.error(error);
        setSubmitting(false);
      }
    },
  });

  const handleSubmitdetails = (e) => {
    e.preventDefault();
    setInfo(!info);
    setOpen2(true);
    setcontact(true);
    setInfo(false);
    setEditDetails(false);
    setAddress(false);
  };

  const HandleSubmitdetails = (e) => {
    e.preventDefault();
    setInfo(!info);
    setOpen2(true);
    setcontact(false);
    setInfo(false);
    setthird(true);
    setEditDetails(false);
  };
  const Title = [
    { id: 1, name: "Mr." },
    { id: 2, name: "Mrs." },
    { id: 3, name: "Ms." },
  ];

  return (
    <>
      <RootStyle>
        <Collapse in={info}>
          <div>
            <ValidationForm onSubmit={handleSubmitdetails} ref={formRefs}>
              <div>
                <DIV style={{ borderRadius: "0" }}>
                  <span className="ml-3">
                    <i class="fas fa-user-check fa-lg "></i>
                  </span>
                  <span className="text-md ml-3 text-black font-bold fs-5">
                    {" "}
                    &nbsp; Contact Information
                  </span>
                </DIV>
              </div>
              <div className="grid grid-cols-2 gap-8 px-6 py-3">
                {inputList.map((item, i) => {
                  return (
                    <>
                      <div>
                        <TextField
                          variant="standard"
                          type="email"
                          label="Enter Your Email"
                          name="email"
                          className="mx-3  my-3 "
                          style={{ width: "300px" }}
                          autoComplete="Off"
                          value={item.email}
                          onChange={(e) => handleInputChangedetails(e, i)}
                          required
                        />
                      </div>
                      <div>
                        <TextField
                          variant="standard"
                          type="text"
                          label="Enter Your Phone No."
                          name="phone"
                          className="mx-3  my-3 "
                          style={{ width: "300px" }}
                          autoComplete="Off"
                          value={item.phone}
                          onChange={(e) => handleInputChangedetails(e, i)}
                          required
                        />
                      </div>
                    </>
                  );
                })}
              </div>
              <div className="col-12 col-xl-6  mt-1 mb-2 ">
                <div className="loginbox-journey-2 p-0">
                  <div className="form-check gstcheck d-flex align-item-center">
                    <input
                      type="checkbox"
                      id="gst"
                      className="mr-2 height-mint"
                      name="gst"
                      value={gstnum}
                      checked={gstinfo}
                      onChange={handleChange}
                    />
                    <label htmlFor="gst">I have a GST number (Optional)</label>
                  </div>
                </div>
              </div>

              {gstinfo === true ? (
                <>
                  {inputList.map((item, i) => {
                    return (
                      <div className="grid grid-cols-2 gap-8 px-6 py-3">
                        <div>
                          <div>
                            <TextField
                              variant="standard"
                              type="0123456789"
                              label="Registration Number"
                              name="gstnumber"
                              required
                              className="mx-3  my-3 "
                              style={{ width: "300px" }}
                              autoComplete="Off"
                              value={item.gstnumber}
                              onChange={(e) => handleInputChangedetails(e, i)}
                            />
                            {/* <input
                              value={gstCompany}
                              type="0123456789"
                              name="gstNumber"
                              maxLength={15}
                              minLength={15}
                              onChange={(e) => handleInputChangeReg(e, i)}
                              required="required"
                              className={
                                "form-control " +
                                (inputList[0].gstNumber.length > 0 ? "active" : "")
                              }
                              id="frmcheck-3"
                              placeholder="Registration Number"
                            /> */}
                            {/* <label className="form-label" for="frmcheck-3">
                        Registration Number
                      </label> */}
                          </div>
                        </div>

                        <div>
                          <div>
                            <TextField
                              variant="standard"
                              type="text"
                              required
                              label="Company Name"
                              name="companyName"
                              className="mx-3  my-3 "
                              style={{ width: "300px" }}
                              autoComplete="Off"
                              value={item.companyName}
                              onChange={(e) => handleInputChangedetails(e, i)}
                            />
                            {/* <input
                              type="text"
                              name="gstCompany"
                              required="required"
                              onChange={(e) => handleInputChangeReg(e, i)}
                              className={
                                "form-control " +
                                (inputList[0].gstCompany.length > 0
                                  ? "active"
                                  : "")
                              }
                              id="frmcheck-4"
                              placeholder="Company Name"
                            /> */}
                            {/* <label className="form-label" for="frmcheck-4">
                        Company Name
                      </label> */}
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </>
              ) : null}
              <div className="modal-footer-svcustom p-3 text-right">
                <Button
                  type="submit"
                  variant="contained"
                  onClick={() => saveCustomerDetail()}
                  // disabled={continueSave}
                >
                  Continue Details
                </Button>
              </div>
            </ValidationForm>
          </div>
        </Collapse>

        <div>
          <div>
            <Collapse in={open2}>
              <div>
                <DIV style={{ borderRadius: "0" }}>
                  <span className="ml-3">
                    <i class="fas fa-user-check fa-lg "></i>
                  </span>
                  <span className="text-md ml-3 text-black font-bold fs-5">
                    {" "}
                    &nbsp; Travellers Details
                  </span>
                </DIV>

                <div className="grid grid-cols-2  px-4 pt-3 pb-4 gap-2">
                  <div className="text-sm font-bold">
                    <span>
                      <FontAwesomeIcon icon={faEnvelope} />
                    </span>
                    &nbsp; Email : {inputList[0].email}
                  </div>
                  <div className="text-sm  font-bold">
                    <span>
                      <FontAwesomeIcon icon={faPhone} />
                    </span>
                    &nbsp; Mobile No : {inputList[0].phone}
                  </div>
                </div>
                <div>
                  {inputList.map((items) => {
                    return (
                      <>
                        {items.title == "" ? null : (
                          <div className="pl-5">Adult</div>
                        )}
                        <div className="row">
                          <div className="col">
                            <span className=" pl-5">
                              {items.title == "" ? null : (
                                <>
                                  Name: {items.title}
                                  {items.firstName} {items.middleName}{" "}
                                  {items.lastName}
                                </>
                              )}
                            </span>
                          </div>
                          <div className="col">
                            <span className=" pl-5">
                              {items.gender == "" ? null : (
                                <>
                                  Gender:{" "}
                                  {items.gender == 1 ? <>Male</> : <>Female</>}
                                </>
                              )}
                            </span>
                          </div>
                          <div className="col">
                            <span className=" pl-5">
                              {items.dateOfBirth == "" ? null : (
                                <>
                                  DOB:{" "}
                                  {moment(items.dateOfBirth).format(
                                    "MMM DD, YYYY"
                                  )}
                                </>
                              )}
                            </span>
                          </div>
                        </div>
                      </>
                    );
                  })}
                  {data.children > 0 ? (
                    <>
                      {childinputList.map((items) => {
                        return (
                          <>
                            {items.title == "" ? null : (
                              <div className="pl-5">Child</div>
                            )}
                            <div className="row">
                              <div className="col">
                                <span className=" pl-5">
                                  {items.title == "" ? null : (
                                    <>
                                      Name: {items.title}
                                      {items.firstName} {items.middleName}{" "}
                                      {items.lastName}
                                    </>
                                  )}
                                </span>
                              </div>
                              <div className="col">
                                <span className=" pl-5">
                                  {items.gender == "" ? null : (
                                    <>
                                      Gender:{" "}
                                      {items.gender == 1 ? (
                                        <>Male</>
                                      ) : (
                                        <>Female</>
                                      )}
                                    </>
                                  )}
                                </span>
                              </div>
                              <div className="col">
                                <span className=" pl-5">
                                  {items.dateOfBirth == "" ? null : (
                                    <>
                                      DOB:{" "}
                                      {moment(items.dateOfBirth).format(
                                        "MMM DD, YYYY"
                                      )}
                                    </>
                                  )}
                                </span>
                              </div>
                            </div>
                          </>
                        );
                      })}
                    </>
                  ) : null}

                  {data.infant > 0 ? (
                    <>
                      {infantinputList.map((items) => {
                        return (
                          <>
                            {items.title == "" ? null : (
                              <div className="pl-5">Infant</div>
                            )}

                            <div className="row">
                              <div className="col">
                                <span className=" pl-5">
                                  {items.title == "" ? null : (
                                    <>
                                      Name: {items.title}
                                      {items.firstName} {items.middleName}{" "}
                                      {items.lastName}
                                    </>
                                  )}
                                </span>
                              </div>
                              <div className="col">
                                <span className=" pl-5">
                                  {items.gender == "" ? null : (
                                    <>
                                      Gender:{" "}
                                      {items.gender == 1 ? (
                                        <>Male</>
                                      ) : (
                                        <>Female</>
                                      )}
                                    </>
                                  )}
                                </span>
                              </div>
                              <div className="col">
                                <span className=" pl-5">
                                  {items.dateOfBirth == "" ? null : (
                                    <>
                                      DOB:{" "}
                                      {moment(items.dateOfBirth).format(
                                        "MMM DD, YYYY"
                                      )}
                                    </>
                                  )}
                                </span>
                              </div>
                            </div>
                          </>
                        );
                      })}
                    </>
                  ) : null}
                </div>
              </div>
            </Collapse>
          </div>
        </div>
        <Collapse in={contact}>
          <div>
            <ValidationForm onSubmit={HandleSubmitdetails} ref={formRefs}>
              <div>
                {inputList.map((items, i) => {
                  return (
                    <>
                      <div style={{ marginLeft: "18px", display: "flex" }}>
                        <Icon icon={personfill} width={25} height={25} />
                        <Typography variant="h6" className="text-black pl-2">
                          Adult
                        </Typography>
                      </div>
                      <div className="d-flex flex-wrap align-items-center">
                        <FormControl
                          variant="standard"
                          sx={{ m: 2, minWidth: 120 }}
                        >
                          <InputLabel id="title">title</InputLabel>
                          <Select
                            labelId="Title"
                            id="title"
                            value={items.title}
                            label="title"
                            name="title"
                            onChange={(e) => handleInputChangedetails(e, i)}
                          >
                            {Title.map((ite) => (
                              <MenuItem value={ite.name}>{ite.name}</MenuItem>
                            ))}
                          </Select>
                        </FormControl>

                        <TextField
                          variant="standard"
                          label="FirstName"
                          name="firstName"
                          className="mx-3  my-3 "
                          style={{ width: "300px" }}
                          value={items.firstName}
                          onChange={(e) => handleInputChangedetails(e, i)}
                          required
                          autoComplete="Off"
                        />

                        <TextField
                          variant="standard"
                          label="LastName"
                          name="lastName"
                          className="mx-3  my-3 "
                          style={{ width: "300px" }}
                          value={items.lastName}
                          onChange={(e) => handleInputChangedetails(e, i)}
                          required
                          autoComplete="Off"
                        />

                        <FormControl
                          variant="standard"
                          sx={{ m: 2, minWidth: 120 }}
                        >
                          <InputLabel id="gender">Gender</InputLabel>
                          <Select
                            label="Gender"
                            id="Gender"
                            name="gender"
                            value={items.gender}
                            onChange={(e) => handleInputChangedetails(e, i)}
                            required
                          >
                            <MenuItem value="1">Male</MenuItem>
                            <MenuItem value="2">Female</MenuItem>
                          </Select>
                        </FormControl>

                        <Box
                          sx={{
                            minWidth: 220,
                            marginTop: "14px",
                            marginLeft: "14px",
                            marginBottom: "24px",
                          }}
                        >
                          <TextField
                            variant="standard"
                            id="date"
                            label="dateOfBirth"
                            type="date"
                            name="dateOfBirth"
                            value={items.dateOfBirth}
                            onChange={(e) => handleInputChangedetails(e, i)}
                            style={{ width: "220px" }}
                            required
                          />
                        </Box>
                      </div>
                    </>
                  );
                })}
                <div className="pl-5 mb-4">
                  {data.adult > 1 &&
                  (inputList.length !== data.adult) == true ? (
                    <>
                      <Button variant="contained" onClick={handleAddTraveller}>
                        Add Traveller
                      </Button>
                    </>
                  ) : null}
                </div>

                {data.children > 0 ? (
                  <>
                    {childinputList.map((item, i) => {
                      return (
                        <>
                          <div style={{ marginLeft: "18px", display: "flex" }}>
                            <Icon icon={personfill} width={25} height={25} />
                            <Typography
                              variant="h6"
                              className="text-black pl-2"
                            >
                              Children
                            </Typography>
                          </div>
                          <div className="d-flex flex-wrap align-items-center">
                            <FormControl
                              variant="standard"
                              sx={{ m: 2, minWidth: 120 }}
                            >
                              <InputLabel id="title">title</InputLabel>
                              <Select
                                labelId="Title"
                                id="title"
                                value={item.title}
                                label="title"
                                name="title"
                                onChange={(e) => AddInputChildren(e, i)}
                              >
                                {Title.map((ite) => (
                                  <MenuItem value={ite.name}>
                                    {ite.name}
                                  </MenuItem>
                                ))}
                              </Select>
                            </FormControl>

                            <TextField
                              variant="standard"
                              label="FirstName"
                              name="firstName"
                              className="mx-3  my-3 "
                              style={{ width: "300px" }}
                              value={item.firstName}
                              onChange={(e) => AddInputChildren(e, i)}
                              required
                              autoComplete="Off"
                            />

                            <TextField
                              variant="standard"
                              label="LastName"
                              name="lastName"
                              className="mx-3  my-3 "
                              style={{ width: "300px" }}
                              value={item.lastName}
                              onChange={(e) => AddInputChildren(e, i)}
                              required
                              autoComplete="Off"
                            />

                            <FormControl
                              variant="standard"
                              sx={{ m: 2, minWidth: 120 }}
                            >
                              <InputLabel id="gender">Gender</InputLabel>
                              <Select
                                id="Gender"
                                name="gender"
                                value={item.gender}
                                onChange={(e) => AddInputChildren(e, i)}
                                variant="standard"
                                required
                              >
                                <MenuItem value="1">Male</MenuItem>
                                <MenuItem value="2">Female</MenuItem>
                              </Select>
                            </FormControl>
                            <Box
                              sx={{
                                minWidth: 220,
                                marginTop: "14px",
                                marginLeft: "14px",
                                marginBottom: "24px",
                              }}
                            >
                              <TextField
                                variant="standard"
                                id="date"
                                label="dateOfBirth"
                                type="date"
                                name="dateOfBirth"
                                value={item.dateOfBirth}
                                onChange={(e) => AddInputChildren(e, i)}
                                style={{ width: "220px" }}
                                required
                              />
                            </Box>
                          </div>
                        </>
                      );
                    })}
                    <div className="pl-5 mb-4">
                      {data.children > 1 &&
                      (childinputList.length !== data.children) == true ? (
                        <>
                          <Button
                            variant="contained"
                            onClick={handleAddChildTraveller}
                          >
                            Add Traveller
                          </Button>
                        </>
                      ) : null}
                    </div>
                  </>
                ) : null}
                {data.infant > 0 ? (
                  <>
                    {infantinputList.map((item, i) => {
                      return (
                        <>
                          <div style={{ marginLeft: "18px", display: "flex" }}>
                            <Icon icon={personfill} width={25} height={25} />
                            <Typography
                              variant="h6"
                              className="text-black pl-2"
                            >
                              Infant
                            </Typography>
                          </div>
                          <div className="d-flex flex-wrap align-items-center">
                            <FormControl
                              variant="standard"
                              sx={{ m: 2, minWidth: 120 }}
                            >
                              <InputLabel id="title">title</InputLabel>
                              <Select
                                labelId="Title"
                                id="title"
                                value={item.title}
                                label="title"
                                name="title"
                                onChange={(e) => AddInputInfant(e, i)}
                              >
                                {Title.map((ite) => (
                                  <MenuItem value={ite.name}>
                                    {ite.name}
                                  </MenuItem>
                                ))}
                              </Select>
                            </FormControl>

                            <TextField
                              variant="standard"
                              label="FirstName"
                              name="firstName"
                              className="mx-3  my-3 "
                              style={{ width: "300px" }}
                              value={item.firstName}
                              onChange={(e) => AddInputInfant(e, i)}
                              required
                              autoComplete="Off"
                            />

                            <TextField
                              variant="standard"
                              label="LastName"
                              name="lastName"
                              className="mx-3  my-3 "
                              style={{ width: "300px" }}
                              value={item.lastName}
                              onChange={(e) => AddInputInfant(e, i)}
                              required
                              autoComplete="Off"
                            />

                            <FormControl
                              variant="standard"
                              sx={{ m: 2, minWidth: 120 }}
                            >
                              <InputLabel id="gender">Gender</InputLabel>
                              <Select
                                id="Gender"
                                name="gender"
                                value={item.gender}
                                onChange={(e) => AddInputInfant(e, i)}
                                variant="standard"
                                required
                              >
                                <MenuItem value="1">Male</MenuItem>
                                <MenuItem value="2">Female</MenuItem>
                              </Select>
                            </FormControl>
                            <Box
                              sx={{
                                minWidth: 220,
                                marginTop: "14px",
                                marginLeft: "14px",
                                marginBottom: "24px",
                              }}
                            >
                              <TextField
                                variant="standard"
                                id="date"
                                label="dateOfBirth"
                                type="date"
                                name="dateOfBirth"
                                value={item.dateOfBirth}
                                onChange={(e) => AddInputInfant(e, i)}
                                style={{ width: "220px" }}
                                required
                              />
                            </Box>
                          </div>
                        </>
                      );
                    })}
                    <div className="pl-5 mb-4">
                      {data.infant > 1 &&
                      (infantinputList.length !== data.infant) == true ? (
                        <>
                          <Button
                            variant="contained"
                            onClick={handleAddinfantTraveller}
                          >
                            Add Traveller
                          </Button>
                        </>
                      ) : null}
                    </div>
                  </>
                ) : null}
              </div>
              <div className="modal-footer-svcustom p-3 text-right">
                <Button
                  type="submit"
                  variant="contained"
                  // onClick={() => saveTravellersDetail()}
                  // disabled={continueSave}
                >
                  Continue Details
                </Button>
              </div>
            </ValidationForm>
          </div>
        </Collapse>

        <Collapse in={third}>
          <div>
            <div className=" grid grid-cols-1 up pl-6 pt-3 mx-1  ">
              <div className="py-4">
                <input
                  type="radio"
                  name="medical"
                  id="medical"
                  value="false"
                  onClick={() => props.setValue(false)}
                />
                <span className="text-sm">
                  {" "}
                  &nbsp; By clicking Pay Now, I agree that I have read and
                  accepted traveloes.com andTerms and Conditions Privacy Policy{" "}
                </span>
              </div>
              <Stack direction={"row"}>
                <span className="text-blue-600  relative text-3xl">
                  <i class="fas fa-rupee-sign"></i>{" "}
                  {props.flight.fare.grandOrgTotal} /-
                </span>
                {/* <Link
                  to="/Flight/CreateBooking"
                  state={{
                    data: FlightDetails,
                    passenderDetails: Passenger,
                  }}
                  style={{ textDecorationLine: "none" }}
                > */}
                <Button variant="contained" onClick={() => DebitBalance()}>
                  Pay Now
                </Button>
                {/* </Link> */}
              </Stack>

              <div className="pr-4 mt-3">
                <p className="text-xs">
                  Please confirm that the names and spelling of travelers are
                  correct & accurate. Please confirm that the dates and timing
                  of flight departures are correct. Tickets are non-transferable
                  as well as non-refundable. Name changes on tickets are not
                  permitted. Our service fees are non-refundable. Total ticket
                  cost included all the taxes and service fees. Date and routing
                  changes will be subject to airline penalties and our service
                  fees. Fares are not guaranteed until ticketed.
                </p>
              </div>
            </div>
          </div>
        </Collapse>
      </RootStyle>
    </>
  );
};
export default Second;
