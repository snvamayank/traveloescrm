import React from "react";
import { Link } from "react-router-dom";
import SG from "../../assets/Image/SG.png";
import { styled } from "@material-ui/core/styles";
import moment from "moment";
import {
  Typography,
  Button,
  Card,
  CardContent,
  Stack,
  Grid,
  Tooltip,
} from "@material-ui/core";
import AirPortData from "src/Api/SampleData";
const RootStyle = styled(Card)(({ theme }) => ({
  boxShadow: theme.customShadows.primary.z24,
  borderWidth: "1px",
  borderStyle: "dashed",
  borderColor: "dark",
}));
const DIV = styled(Card)(({ theme }) => ({
  boxShadow: "none",
  textAlign: "center",
  backgroundColor: theme.palette.primary.lighter,
  [theme.breakpoints.up("md")]: {
    textAlign: "left",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
    margin: 10,
  },
}));

const First = ({ FlightResult }) => {
  const ConvertMinsToTime = ({ data }) => {
    let hours = Math.floor(data / 60);
    let minutes = data % 60;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    return `${hours}h:${minutes}m`;
  };
  const percentAmount = FlightResult.fare.grandTotal / 2;
  return (
    <RootStyle>
      <>
        <Stack
          direction="row"
          sx={{
            mx: 3,
            my: 3,
          }}
        >
          <i class="fas fa-plane-departure fa-lg relative top-1 mr-3"></i>
          <Typography variant="h5">Flight Details</Typography>
        </Stack>
        {FlightResult.outBound.map((item) => (
          <>
            <Stack
              direction="row"
              sx={{
                mx: 3,
                my: 3,
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <div className="pt-2 pb-3">
                <div>
                  <img
                    src={`https://www.travomint.com/resources/images/airline-logo/${item.airline}.png`}
                    width="50px"
                    height="50px"
                    className="rounded"
                  />
                </div>
                <div className="text-center">
                  <Typography>{item.airlineName}</Typography>
                  <span className="text-xs text-black font-sans font-bold">
                    {" "}
                    {item.flightID}
                  </span>
                </div>
              </div>
              <div className="py-3">
                <Tooltip
                  title={
                    item.fromAirport == null ? (
                      []
                    ) : (
                      <>
                        {AirPortData.filter(
                          (items) => items.airportCode === item.fromAirport
                        ).map((item) => item.airportName)}
                      </>
                    )
                  }
                  arrow
                  placement="top-start" //
                >
                  <Typography>
                    <span className="text-black font-bold">
                      {item.fromAirport}&nbsp;
                    </span>
                    {AirPortData.filter(
                      (items) => items.airportCode === item.fromAirport
                    ).map((item) => item.cityName)}
                  </Typography>
                </Tooltip>
                {/* <Typography>
                  <span className="text-black font-bold">
                    {item.fromAirport}&nbsp;
                  </span>
                  {AirPortData.filter(
                    (items) => items.airportCode === item.fromAirport
                  ).map((item) => item.cityName)}
                </Typography> */}

                <span className="text-lg text-black font-bold">
                  {/* {item.depDate.split("T")[1].substring(0, 5)}{" "} */}
                  {moment(item.depDate).format("hh:mm A")}
                </span>
                <Typography>
                  {moment(item.depDate).format("MMM DD, YYYY")}
                </Typography>

                {/* <Typography>
                  {AirPortData.filter(
                    (items) => items.airportCode === item.fromAirport
                  ).map((item) => item.airportName)}
                </Typography> */}
              </div>
              <div className="col-span-1 w-25">
                <div className="mt-3 text-center">
                  {/* <div className="col-span-3 pl-2">
                        <hr className="bg-black opacity-100" />
                      </div> */}
                  <div className="col-span-3 text-center relative -top-2">
                    <span className=" text-black font-bold  ">
                      <ConvertMinsToTime data={item.eft + item.layOverTime} />
                    </span>
                    <br />
                  </div>
                  {/* <div className="col-span-3 pr-2">
                        <hr className="bg-black opacity-100" />
                      </div> */}
                </div>
              </div>
              <div className="py-3 w-25">
                <Tooltip
                  title={
                    item.toAirport == null ? (
                      []
                    ) : (
                      <>
                        {AirPortData.filter(
                          (items) => items.airportCode === item.toAirport
                        ).map((item) => item.airportName)}
                      </>
                    )
                  }
                  arrow
                  placement="top-start"
                >
                  <Typography>
                    <span className="text-black font-bold">
                      {item.toAirport}&nbsp;
                    </span>
                    {AirPortData.filter(
                      (items) => items.airportCode === item.toAirport
                    ).map((item) => item.cityName)}
                  </Typography>
                </Tooltip>
                {/* <Typography>
                  <span className="text-black font-bold">
                    {item.toAirport}&nbsp;
                  </span>
                  {AirPortData.filter(
                    (items) => items.airportCode === item.toAirport
                  ).map((item) => item.cityName)}
                </Typography> */}

                <span className="text-black font-bold">
                  {/* {item.reachDate.split("T")[1].substring(0, 5)}{" "} */}
                  {moment(item.reachDate).format("hh:mm A")}
                </span>
                <Typography>
                  {moment(item.reachDate).format("MMM DD, YYYY")}
                </Typography>

                {/* <Typography>
                    {AirPortData.filter(
                      (items) => items.airportCode === item.toAirport
                    ).map((item) => item.airportName)}
                  </Typography> */}
              </div>
            </Stack>
            {/* <div className="grid grid-cols-4">
              <div className="pt-2 pb-3">
                <div className="d-flex justify-content-center">
                  <img
                    src={`https://www.travomint.com/resources/images/airline-logo/${item.airline}.png`}
                    width="50px"
                    height="50px"
                  />
                </div>
                <div className="text-center">
                  <span className="text-xl text-black font-sans font-bold">
                    {item.airlineName}
                  </span>
                  <br />
                  <span className="text-xs text-black font-sans font-bold">
                    {" "}
                    {item.flightID}
                  </span>
                </div>
              </div>
              <div className="text-center py-3">
                <span className="text-lg text-black font-bold">
                  <Typography>{item.fromAirport}</Typography>
                  {item.depDate.split("T")[1].substring(0, 5)}{" "}
                  {moment(item.depDate).format("hh:mm A")}
                </span>
                <Typography>
                  {moment(item.depDate).format("MMM DD, YYYY")}
                </Typography>

                <Typography>
                  {AirPortData.filter(
                    (items) => items.airportCode === item.fromAirport
                  ).map((item) => item.airportName)}
                </Typography>
              </div>
              <div className="col-span-1">
                <div className="mt-3 ">
                  <div className="col-span-3 pl-2">
                    <hr className="bg-black opacity-100" />
                  </div>
                  <div className="col-span-3 text-center relative -top-2">
                    <span className="text-xs text-black font-sans">
                      <ConvertMinsToTime data={item.eft + item.layOverTime} />
                    </span>
                    <br />
                  </div>
                  <div className="col-span-3 pr-2">
                    <hr className="bg-black opacity-100" />
                  </div>
                </div>
              </div>
              <div className="text-center py-3">
                <span className="text-lg text-black font-bold">
                  <Typography>{item.toAirport}</Typography>
                  {item.reachDate.split("T")[1].substring(0, 5)}{" "}
                  {moment(item.reachDate).format("hh:mm A")}
                </span>
                <Typography>
                  {moment(item.reachDate).format("MMM DD, YYYY")}
                </Typography>

                <Typography>
                  {AirPortData.filter(
                    (items) => items.airportCode === item.toAirport
                  ).map((item) => item.airportName)}
                </Typography>
              </div>
            </div> */}
            {/* </div> */}
            {/* </div> */}
            {/* </Grid> */}
            <div className="grid grid-cols-3 up rounded-2xl my-3 mx-2 ">
              <div className="grid grid-cols-5 gap-2 py-2">
                <div className="pt-2">
                  <i class="fas fa-briefcase fa-lg float-right"></i>
                </div>
                <div className="col-span-4 pl-2">
                  <p className="font-bold mb-0 text-lg">Cabin Baggage</p>
                  <span className="text-sm">{item.cabinBaggage} kgs</span>
                </div>
              </div>

              <div className="grid grid-cols-5 gap-2 py-2">
                <div className="pt-2">
                  <i class="fas fa-luggage-cart fa-lg float-right"></i>
                </div>
                <div className="col-span-4 pl-2">
                  <p className="font-bold mb-0 text-lg">Check-In Baggage</p>
                  <span className="text-sm">{item.baggage} kgs</span>
                </div>
              </div>

              <div className="grid grid-cols-5 gap-2 py-2">
                <div className="pt-2">
                  <i class="fas fa-exchange-alt fa-lg float-right"></i>
                </div>
                <div className="col-span-4 pl-2 ">
                  <p className="font-bold mb-0 text-lg">Refundable</p>
                </div>
              </div>
            </div>
          </>
        ))}
      </>
    </RootStyle>
  );
};
export default First;
