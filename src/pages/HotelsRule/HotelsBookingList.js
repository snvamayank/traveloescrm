import React from "react";
import BookingListing from "src/components/hotelComponent/BookingListing";
import LoadingScreen from "src/components/LoadingScreen";
import ErrorPages from "src/components/Error_Pages/ErrorPages";
import { useGetHotelBoooking } from "src/Api/Hotel/usecustomApi";
import { gethotelsBooking } from "src/Api/Hotel/ApiUrl";
const HotelsBooking = () => {
  const result = useGetHotelBoooking(gethotelsBooking);
  console.log("result", result);

  if (result.loading) {
    return <LoadingScreen />;
  }

  if (result.error) {
    return <ErrorPages title={result.error} />;
  }

  return (
    <div>
      <div className="text-center">
        <h2>Hotels Booking History</h2>
      </div>
      <BookingListing data={result.data} />
    </div>
  );
};

export default HotelsBooking;
