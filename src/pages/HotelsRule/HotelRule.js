import axios from "axios";
import React, { useEffect } from "react";
import { hostname } from "src/HostName";
import { useLocation } from "react-router-dom";
import { useState } from "react";
import LoadingScreen from "src/components/LoadingScreen";
import ErrorPages from "src/components/Error_Pages/ErrorPages";
import HotelAgent from "src/components/hotelComponent/HotelAgent";
import { Card } from "@material-ui/core";
import AgentHeader from "src/components/hotelComponent/component/AgentHeader";
import { useGetHotelRule } from "src/Api/Hotel/usecustomApi";
import { gethotelfarerule } from "src/Api/Hotel/ApiUrl";

const HotelRule = () => {
  const { state } = useLocation();
  const result = useGetHotelRule(gethotelfarerule, state.userid);

  if (result.error) {
    return <ErrorPages title={result.error} />;
  }

  if (result.loading) {
    return <LoadingScreen />;
  }

  return (
    <div>
      {result.data &&
        result.data.data.map((item, i) => (
          <Card key={i}>
            <AgentHeader state={state.name} />
            <HotelAgent data={item.hotelProvider} userid={item.userid} />
          </Card>
        ))}
    </div>
  );
};

export default HotelRule;
