import React, { useEffect, useState } from "react";
import {
  Grid,
  Stack,
  Card,
  Typography,
  CardHeader,
  CardBody,
  Divider,
  Box,
} from "@material-ui/core";
import { useLocation } from "react-router";
import { moment } from "moment";
import { hostname } from "src/HostName";
import Success from "./createBooking/Success";
import Failure from "./createBooking/Failure";
import Lottie from "react-lottie";
import LOADER from "../LottieView/loading.json";
import useAuth from "src/hooks/useAuth";
const CreateBooking = () => {
  const { state } = useLocation();
  // const { user } = useAuth();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  const flightDetails = state.data;
  const sessionId = state.sessionId;
  const SingleTravellersArray = state.SingleTravellersArray;

  const passenderDetails =
    state.passenderDetails == "undefined" ? [] : state.passenderDetails;

  const [booking, setBooking] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const PassengeDetails = passenderDetails.map((item, index) => ({
    Pax_Id: 1,
    Pax_type: item.passengerType,
    Title: item.title,
    First_Name: item.firstName,
    Last_Name: item.lastName,
    Gender: item.gender,
    Age: 33,
    DOB: item.dateOfBirth,
    Passport_Number: null,
    Passport_Issuing_Country: null,
    Passport_Expiry: null,
    Nationality: null,
    FrequentFlyerDetails: null,
  }));
  console.log("PassengeDetails;'", PassengeDetails);
  const BookingFlight = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      Customer_Mobile: passenderDetails[0].phone,
      Passenger_Email: passenderDetails[0].email,
      PAX_Details: PassengeDetails,
      GST: false,
      GST_Number: "",
      GST_HolderName: "GST COM Name",
      GST_Address: "GST Address AREA",
      BookingFlightDetails: flightDetails,
    });
    console.log("raw:", raw);

    // var requestOptions = {
    //   method: "POST",
    //   headers: myHeaders,
    //   body: raw,
    //   redirect: "follow",
    // };
    // const creatingBooking = await fetch(
    //   `${hostname}flightBooking`,
    //   requestOptions,
    // )
    // const bookingData = await creatingBooking.json();
    // setIsLoading(false);
    // setBooking(bookingData);
  };
  useEffect(() => {
    BookingFlight();
  }, []);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: LOADER,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <Grid>
      <Success />
    </Grid>
  );
};

export default CreateBooking;

/*  {isLoading === true ? (
        <Box sx={{ w: 110 }}>
          <Lottie options={defaultOptions} height={400} width={400} />
        </Box>
      ) : (
        <>
          {booking.status === 'Failure' ? (
            <>
              <Failure />
            </>
          ) : (
            <>
            ////////////////////////////////////////////
               </>
          )}
        </>
      )}*/
