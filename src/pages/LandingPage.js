// material
import { styled } from "@material-ui/core/styles";
import MainFooter from "src/layouts/main/MainFooter";
// components
import Page from "../components/Page";
import {
  LandingHero,
  LandingMinimal,
  LandingDarkMode,
  LandingThemeColor,
  LandingPricingPlans,
  LandingAdvertisement,
  LandingCleanInterfaces,
  LandingHugePackElements,
} from "../components/_external-pages/landing";

// ----------------------------------------------------------------------

const RootStyle = styled(Page)({
  height: "100%",
});

const ContentStyle = styled("div")(({ theme }) => ({
  overflow: "hidden",
  position: "relative",
  backgroundColor: theme.palette.background.default,
}));

// ----------------------------------------------------------------------

export default function LandingPage() {
  return (
    <RootStyle
      title="The starting point for your next project | Traveloes"
      id="move_top"
    >
      <LandingHero /> {/* Start Booking Tickets Section */}
      <ContentStyle>
        <LandingMinimal /> {/* We Develop Best Software For People section */}
        <LandingThemeColor />
        {/* Express your own style with just one click Section */}
        <LandingPricingPlans />{" "}
        {/* Express your own style with just one click Section */}
        <LandingAdvertisement />
        <MainFooter />{" "}
        {/* (Traveloes.com is a legal entity of SNVA Travel Tech Pvt Ltd) Footer */}
      </ContentStyle>
    </RootStyle>
  );
}
