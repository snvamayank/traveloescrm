import {
  Grid,
  Stack,
  Typography,
  styled,
  Paper,
  Card,
  TextField,
  Button,
  FormControlLabel,
  RadioGroup,
  Radio,
  Autocomplete,
  Chip,
  Dialog,
  DialogTitle,
  List,
} from "@material-ui/core";
import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router";
import HeaderBreadcrumbs from "../../components/HeaderBreadcrumbs";
import { PATH_DASHBOARD } from "src/routes/paths";
import { Form, FormikProvider, useFormik } from "formik";
import { DatePicker, LocalizationProvider } from "@material-ui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import moment from "moment";
import { hostname } from "src/HostName";
import { Page } from "@react-pdf/renderer";
import ProductNewForm from "./CreateFareRule";
import { useSnackbar } from "notistack5";
import * as Yup from "yup";
import PropTypes from "prop-types";
import fakeRequest from "src/utils/fakeRequest";

ProductNewForm.propTypes = {
  isEdit: PropTypes.bool,
  currentProduct: PropTypes.object,
};

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  display: "flex",
  color: theme.palette.text.secondary,
  borderWidth: 1,
  borderRadius: 10,
  alignItems: "center",
  justifyContent: "space-around",
}));
const RootStyle = styled(Card)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  color: theme.palette.text.secondary,
}));

const label = { inputProps: { "aria-label": "Checkbox demo" } };
const ApplyRule = ({ isEdit, currentProduct }) => {
  const [amount, setAmount] = useState("");
  const [value, setValue] = React.useState(null);
  const [secondValue, setSecondValue] = React.useState(null);
  const [isloading, setIsloading] = useState(true);
  const [postApiData, setPostApiData] = useState([]);
  const [provider, setProvider] = useState("");
  const [radioValue, setRadioValue] = useState("");
  const [open, setOpen] = React.useState(false);
  const [airlineCode, setAirlineCode] = useState([]);
  const [codeOfAirline, setCodeOfAirline] = useState([]);
  const [providerObject, setProviderobject] = useState([]);
  const [fareRuleObj, setFareRuleObj] = useState([
    {
      name: "",
      code: "ALL",
      gdsType: 0,
      fareType: "",
    },
  ]);
  const { state } = useLocation();
  const providers = state.allProviderData;
  const AgentName = state.name;
  const USERID = state.userid;
  const handleChange = (event) => {
    setAmount(event.target.value);
  };
  const FromDate = moment(value).format("MM/DD/YYYY");
  const ToDate = moment(secondValue).format("MM/DD/YYYY");
  const GENDER_OPTION = ["%", "FLAT"];

  const handleClickOpen = (c) => {
    setProvider(c);
  };

  const handleClose = () => {
    setOpen(false);
    setProviderobject({ name: provider.provider, code: values.tags });
  };
  var ArrayObj = [];
  var ArrayOfProvider = [providerObject];
  ArrayObj.push(...ArrayOfProvider);

  const TAGS_OPTION = airlineCode.filter(
    (item) => item.name === provider.provider
  );

  const LabelStyle = styled(Typography)(({ theme }) => ({
    ...theme.typography.subtitle2,
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing(1),
  }));

  // ----------------------------------------------------------------------

  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const NewProductSchema = Yup.object().shape({
    name: Yup.string().required("Name is required"),
    description: Yup.string().required("Description is required"),
    images: Yup.array().min(1, "Images is required"),
    price: Yup.number().required("Price is required"),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: currentProduct?.name || "",
      description: currentProduct?.description || "",
      images: currentProduct?.images || [],
      code: currentProduct?.code || "",
      sku: currentProduct?.sku || "",
      price: currentProduct?.price || "",
      priceSale: currentProduct?.priceSale || "",
      tags: currentProduct?.tags || [codeOfAirline],
      inStock: Boolean(currentProduct?.inventoryType !== "out_of_stock"),
      taxes: true,
      gender: currentProduct?.gender || GENDER_OPTION[2],
    },
    validationSchema: NewProductSchema,
    onSubmit: async (values, { setSubmitting, resetForm, setErrors }) => {
      try {
        await fakeRequest(500);
        resetForm();
        setSubmitting(false);
        enqueueSnackbar(!isEdit ? "Create success" : "Update success", {
          variant: "success",
        });
        navigate(PATH_DASHBOARD.eCommerce.list);
      } catch (error) {
        console.error(error);
        setSubmitting(false);
        setErrors(error);
      }
    },
  });

  const {
    errors,
    values,
    touched,
    handleSubmit,
    isSubmitting,
    setFieldValue,
    getFieldProps,
  } = formik;

  const handleRadioValueChange = (event) => {
    setRadioValue(event.target.value);
    setOpen(false);
  };

  // const options = {
  //   animationData: Loading,
  //   loop: true,
  //   autoplay: true,
  // };
  // const { View } = useLottie(options);
  // const { user } = useAuth();
  /* ---------------------------------------Post API-------------------------------------------- */
  const PostApi = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      userid: USERID,
      provider: fareRuleObj,
      rule: radioValue,
      amount: amount,
      fromDate: FromDate,
      toDate: ToDate,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    // const PostingApi = await fetch(
    //   `${hostname}post_all_provider_data`,
    //   requestOptions
    // );
    const PostingApi = await fetch(
      `${hostname}post_all_provider_data`,
      requestOptions
    );
    const response = await PostingApi.json();
    if (response.length === 0) {
      setIsloading(false);
    } else {
      setIsloading(true);
    }
    if (response) {
      alert("add Data Sucessfully ");
      setPostApiData(response);
      setIsloading(false);
    }
  };
  /* ---------------------------------------Post API-------------------------------------------- */
  /* -------------------------------Adding element To Array------------------------------------ */
  /* -------------------------------Adding element To Array------------------------------------ */
  /* ---------------------------------------Get API-------------------------------------------- */
  const Get = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
    const GettingApi = await fetch(
      `${hostname}get/airline/code`,
      requestOptions
    );
    const response = await GettingApi.json();
    if (response.length === 0) {
      setIsloading(false);
    } else {
      setIsloading(true);
    }
    if (response) {
      setAirlineCode(response);
      setIsloading(false);
    }
  };
  /* ---------------------------------------Get API-------------------------------------------- */

  const handleInput = (e, index) => {
    const { name, value } = e.target;
    const list = [...fareRuleObj];
    list[index][name] = value;
    setFareRuleObj(list);
  };
  const AddDetailsObj = () => {
    setFareRuleObj([
      ...fareRuleObj,
      {
        name: "",
        code: "",
        gdsType: 0,
        fareType: "",
      },
    ]);
  };

  const handleRemoveClick = (index) => {
    const list = [...fareRuleObj];
    list.splice(index, 1);
    setFareRuleObj(list);
  };

  return (
    <Page title="Apply New Rule | Traveloes">
      <HeaderBreadcrumbs
        heading={AgentName}
        links={[
          { name: "Dashboard", href: PATH_DASHBOARD.root },
          {
            name: "Fare Rule",
            href: PATH_DASHBOARD.farerule.farerule,
          },
          {
            name: AgentName,
            href: PATH_DASHBOARD.farerule.ApplyRule,
          },
        ]}
      />
      <>
        <RootStyle>
          <Grid container spacing={2}>
            {providers.data.map((c, i) => (
              <Grid item xs={2}>
                <Item>
                  {c.provider}
                  <TextField
                    type="checkbox"
                    name="name"
                    id="name"
                    value={c.provider}
                    onChange={(e) => handleInput(e, i)}
                    variant="standard"
                  />
                </Item>
              </Grid>
            ))}
            <Item sx={{ marginLeft: 2, marginTop: 2 }}>
              <Button size={"small"} onClick={() => AddDetailsObj()}>
                Add More Provider
              </Button>
            </Item>
          </Grid>

          <Stack sx={{ display: "flex", flexDirection: "row" }}>
            {fareRuleObj.map((item, i) => (
              <Stack
                sx={{
                  borderWidth: 1,
                  width: "20rem",
                  padding: 2,
                  margin: 2,
                  borderRadius: 1,
                }}
              >
                <Stack
                  sx={{
                    borderRadius: 10,
                    display: "flex",
                    flexDirection: "row-reverse",
                    justifyContent: "space-between",
                  }}
                >
                  {fareRuleObj.length !== 1 && (
                    <Button
                      variant="contained"
                      onClick={() => handleRemoveClick(i)}
                    >
                      X
                    </Button>
                  )}
                </Stack>
                <LabelStyle>{item.name}</LabelStyle>

                <TextField
                  value={item.name}
                  disabled={true}
                  placeholder="Provider"
                  sx={{ margin: 1 }}
                />
                <TextField
                  name="code"
                  value={item.code}
                  id="code"
                  placeholder="Airline Code (AI,6E ,G8)"
                  onChange={(e) => handleInput(e, i)}
                  sx={{ margin: 1 }}
                />
                <TextField
                  name="fareType"
                  value={item.fareType}
                  id="farerType"
                  placeholder="Fare Type"
                  onChange={(e) => handleInput(e, i)}
                  sx={{ margin: 1 }}
                />
              </Stack>
            ))}
          </Stack>
        </RootStyle>
      </>

      {/* <ProductNewForm />-*/}
      <Card sx={{ p: 3, mt: 5 }}>
        <Stack spacing={3}>
          <Grid item xs={4}>
            <LabelStyle>Dates</LabelStyle>
            <Stack direction="row" justify="space-between" spacing={5}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  label="From Date"
                  value={value}
                  onChange={(newValue) => {
                    setValue(newValue);
                  }}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>

              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  label="To Date"
                  value={secondValue}
                  onChange={(newSecondValue) => {
                    setSecondValue(newSecondValue);
                  }}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
            </Stack>
          </Grid>

          <div>
            <LabelStyle>Percent Or Flat</LabelStyle>
            <RadioGroup {...getFieldProps("gender")} row>
              <Stack
                spacing={1}
                direction="row"
                sx={{ borderWidth: 1, width: "100rem", p: 1, borderRadius: 1 }}
              >
                {GENDER_OPTION.map((gender) => (
                  <FormControlLabel
                    key={gender}
                    value={gender}
                    control={<Radio />}
                    label={gender}
                    onChange={handleRadioValueChange}
                  />
                ))}
              </Stack>
            </RadioGroup>
          </div>

          <div>
            <LabelStyle>Enter Amount</LabelStyle>
            <TextField
              onChange={handleChange}
              sx={{
                width: "89rem",
              }}
            />
          </div>
          <Stack
            direction="row"
            spacing={2}
            sx={{ height: "2.5rem", mt: 1, ml: 3 }}
          >
            <Button variant="contained" onClick={() => PostApi()}>
              Submit
            </Button>
          </Stack>
        </Stack>
      </Card>

      {/* Modal For Airline */}
      {/* {provider.provider === } */}
      <Dialog onClose={handleClose} open={open}>
        <DialogTitle>Airlines</DialogTitle>
        <List sx={{ pt: 0 }}>
          <Stack direction={"row"}>
            {values.tags.length !== 0 ? (
              <>
                {codeOfAirline.map((option, index) => (
                  <Stack direction={"row"} sx={{ p: 1 }}>
                    <Chip key={option} size="small" label={option} />
                  </Stack>
                ))}
              </>
            ) : (
              <Typography>Please Select any ariline</Typography>
            )}
          </Stack>
          <Stack direction="row" sx={{ p: 1, m: 1 }}>
            {TAGS_OPTION.length !== 0 ? (
              <>
                <Autocomplete
                  multiple
                  freeSolo
                  value={values.tags}
                  sx={{
                    m: 5,
                    width: 300,
                  }}
                  onChange={(event, newValue) => {
                    setFieldValue("tags", newValue);
                  }}
                  options={TAGS_OPTION[0].code}
                  renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                      <Chip
                        key={option}
                        size="small"
                        label={option}
                        {...getTagProps({ index })}
                      />
                    ))
                  }
                  renderInput={(params) => (
                    <TextField label="Tags" {...params} />
                  )}
                />
              </>
            ) : (
              "ALL"
            )}
          </Stack>
        </List>
      </Dialog>
      {/* Modal For Airline */}
    </Page>
  );
};

export default ApplyRule;
