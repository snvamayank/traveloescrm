import { useLocation } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { useTheme } from "@material-ui/core/styles";
import FullViewDisplayForm from "src/components/ModalBody/FullViewDisplayForm";
import ReactPaginate from "react-paginate";
import FullViewdisplayMarkup from "src/components/ModalBody/FullViewdisplayMarkup";
import axios from "axios";
import { hostname } from "src/HostName";
import Modal from "react-bootstrap/Modal";
import EditAirport from "src/components/ModalBody/EditAirport";
import HeaderBreadcrumbs from "src/components/HeaderBreadcrumbs";
import { PATH_DASHBOARD } from "src/routes/paths";
import LoadingScreen from "src/components/LoadingScreen";

const ActionStyle = {
  color: "#00ab55",
};

const FlightRule = () => {
  const [itemOffset, setItemOffset] = useState(0);
  const [checkedairportMarkup, setCheckedairportMarkup] = useState([]);
  const [checkedairportMarkupRuleName, setCheckedairportMarkupRuleName] =
    useState([]);
  const [isCheckAll, setIsCheckAll] = useState(false);
  const [checked, setChecked] = useState(false);
  const [agent, setagent] = useState({});
  const [loading, setloading] = useState(true);
  const [editAirportMarkup, setEditAirportMarkup] = useState({});
  const [ruleName, setRuleName] = useState();
  const [aiportedit, setAiportedit] = useState(false);
  const [ruleData, setruleData] = useState({});
  const [filter, setFilter] = useState({
    Origin: "",
    Destination: "",
    RuleName: "",
    CappingAmount: "",
    Airline: "",
    TripType: "",
  });
  const location = useLocation();
  const data = location.state;

  console.log("data", data);

  const GetAgentRule = async () => {
    const agentData = await axios.get(
      `${hostname}get/farerule/info/${data.userid}`
    );
    const getprovider = agentData.data.data.map((item) => item.apiprovider);
    const singlegetprovider = getprovider.map((items, index) =>
      items.find((items, index) => items.gdsType === data.gdsType)
    );

    setagent(agentData.data.data);
    setruleData(singlegetprovider[0]);
    setloading(false);
  };

  useEffect(() => {
    GetAgentRule();
  }, [data.userid]);

  console.log("agent", agent);
  console.log("ruleData", ruleData);

  const allAirportMarkup = ruleData?.airportMarkup;
  const theme = useTheme();
  const itemsPerPage = 200;
  const endOffset = itemOffset + itemsPerPage;
  const listAirportMarkup = allAirportMarkup?.slice(itemOffset, endOffset);
  const pageCount = Math.ceil(allAirportMarkup?.length / itemsPerPage);

  const buttonBorder = {
    borderRadius: 100,
    border: "1px solid ",
    borderColor: theme.palette.primary.light,
  };

  const handleChangeFilter = (e) => {
    if (e.target.name === "CappingAmount") {
      setFilter({ ...filter, [e.target.name]: e.target.value });
    } else if (e.target.name === "RuleName") {
      setFilter({ ...filter, [e.target.name]: e.target.value });
    } else {
      setFilter({ ...filter, [e.target.name]: e.target.value.toUpperCase() });
    }
  };

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % allAirportMarkup.length;
    console.log(
      `User requested page number ${event.selected}, which is offset ${newOffset}`
    );
    setItemOffset(newOffset);
  };

  const handleSelectAll = (e) => {
    setIsCheckAll(!isCheckAll);
    setChecked(!checked);
    setCheckedairportMarkupRuleName(
      listAirportMarkup &&
        listAirportMarkup
          .sort((x, y) => x.capAmount - y.capAmount)
          .filter(
            (ite, ind) =>
              ite.From.includes(filter.Origin) &&
              ite.To.includes(filter.Destination) &&
              ite.AirCode.includes(filter.Airline) &&
              ite.ruleName.includes(filter.RuleName) &&
              ite.cappingDays.toString().includes(filter.CappingAmount) &&
              ite.tripType.includes(filter.TripType)
          )
          .map((data) => data.ruleName)
    );
    setCheckedairportMarkup(
      listAirportMarkup &&
        listAirportMarkup
          .sort((x, y) => x.capAmount - y.capAmount)
          .filter(
            (ite, ind) =>
              ite.From.includes(filter.Origin) &&
              ite.To.includes(filter.Destination) &&
              ite.AirCode.includes(filter.Airline) &&
              ite.ruleName.includes(filter.RuleName) &&
              ite.cappingDays.toString().includes(filter.CappingAmount) &&
              ite.tripType.includes(filter.TripType)
          )
    );
    if (isCheckAll) {
      setCheckedairportMarkup([]);
      setCheckedairportMarkupRuleName([]);
    }
  };

  const handleslecterule = (item) => {
    const data = checkedairportMarkupRuleName.includes(item.ruleName);
    if (data) {
      const filteredArray = checkedairportMarkupRuleName.filter(
        (itemValue, index) => itemValue !== item.ruleName
      );
      setCheckedairportMarkupRuleName(filteredArray);
      const filtercheckedairportMarkup = checkedairportMarkup.filter(
        (items) => items.ruleName !== item.ruleName
      );
      setCheckedairportMarkup(filtercheckedairportMarkup);
    } else {
      setCheckedairportMarkup((oldArray) => [...oldArray, item]);
      setCheckedairportMarkupRuleName((oldArray) => [
        ...oldArray,
        item.ruleName,
      ]);
    }
  };

  const handleEditAirportMarkup = (item) => {
    const rule = item.ruleName;
    setEditAirportMarkup(item);
    setRuleName(rule);
    setAiportedit(true);
  };

  const handleEditAirportMarkupclose = () => {
    setAiportedit(false);
  };

  return (
    <div>
      {loading ? (
        <LoadingScreen/>
      ) : (
        <>
          <HeaderBreadcrumbs
            heading="All Flight Fare Rule"
            links={[
              { name: "Dashboard", href: PATH_DASHBOARD.root },
              {
                name: data.name,
              },
            ]}
          />

          <div>
            <h4 className="text-uppercase">
              {ruleData.name}&nbsp; ({ruleData.airportMarkup.length})
            </h4>
            <div>
              <FullViewDisplayForm
                filter={filter}
                handleChangeFilter={handleChangeFilter}
                checkedairportMarkup={checkedairportMarkup}
                buttonBorder={buttonBorder}
                handleSelectAll={handleSelectAll}
                isCheckAll={isCheckAll}
                allAirportMarkup={allAirportMarkup}
                ruleData={ruleData}
                agentData={agent[0]}
              />

              <div>
                <FullViewdisplayMarkup
                  filter={filter}
                  listAirportMarkup={listAirportMarkup}
                  checkedairportMarkupRuleName={checkedairportMarkupRuleName}
                  ActionStyle={ActionStyle}
                  handleslecterule={handleslecterule}
                  ruleData={ruleData}
                  agentData={agent[0]}
                  handleEditAirportMarkup={handleEditAirportMarkup}
                />
              </div>
            </div>
          </div>

          <ReactPaginate
            nextLabel="next >"
            onPageChange={handlePageClick}
            pageRangeDisplayed={itemsPerPage}
            pageCount={pageCount}
            previousLabel="< previous"
            renderOnZeroPageCount={null}
            breakLabel={<a href="">...</a>}
            breakClassName={"break-me"}
            marginPagesDisplayed={2}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
          <Modal show={aiportedit} onHide={handleEditAirportMarkupclose}>
            <Modal.Header closeButton>
              <Modal.Title>Edit Airport Markup</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <EditAirport
                Data={editAirportMarkup}
                ruleName={ruleName}
                setEditAirportMarkup={setEditAirportMarkup}
                ruleData={ruleData}
                agentData={agent[0]}
                handleEditAirportMarkupclose={handleEditAirportMarkupclose}
              />
            </Modal.Body>
          </Modal>
        </>
      )}
    </div>
  );
};

export default FlightRule;
