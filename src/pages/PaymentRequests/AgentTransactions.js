import { Icon } from "@iconify/react";
import {
  Avatar,
  Box,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  Menu,
  MenuItem,
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import HeaderBreadcrumbs from "src/components/HeaderBreadcrumbs";
import Page from "src/components/Page";
import useSettings from "src/hooks/useSettings";
import { PATH_DASHBOARD } from "src/routes/paths";
import { useTheme, styled } from "@material-ui/core/styles";
import useAuth from "src/hooks/useAuth";
import { hostname } from "src/HostName";
import { useLocation } from "react-router";
import Scrollbar from "src/components/Scrollbar";
import Label from "src/components/Label";
import { sentenceCase } from "change-case";
import { CardHeader } from "@mui/material";
import { format } from "date-fns";
import shareFill from "@iconify/icons-eva/share-fill";
import printerFill from "@iconify/icons-eva/printer-fill";
import downloadFill from "@iconify/icons-eva/download-fill";
import trash2Outline from "@iconify/icons-eva/trash-2-outline";
import moreVerticalFill from "@iconify/icons-eva/more-vertical-fill";
import arrowIosForwardFill from "@iconify/icons-eva/arrow-ios-forward-fill";
import { MIconButton } from "src/components/@material-extend";
import Alert from "src/theme/overrides/Alert";
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  display: "flex",
  color: theme.palette.text.secondary,
  borderWidth: 1,
  borderRadius: 10,
  alignItems: "center",
}));
function MoreMenuButton({ onUpdate, onPrint, onShare, onDelete }) {
  const menuRef = useRef(null);
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <>
        <MIconButton ref={menuRef} size="large" onClick={handleOpen}>
          <Icon icon={moreVerticalFill} width={20} height={20} />
        </MIconButton>
      </>

      <Menu
        open={open}
        anchorEl={menuRef.current}
        onClose={handleClose}
        PaperProps={{
          sx: { width: 200, maxWidth: "100%" },
        }}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
      >
        <MenuItem onClick={onUpdate}>
          <Icon icon={downloadFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Update balance
          </Typography>
        </MenuItem>
        <MenuItem onClick={onPrint}>
          <Icon icon={printerFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Print
          </Typography>
        </MenuItem>
        <MenuItem onClick={onShare}>
          <Icon icon={shareFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Share
          </Typography>
        </MenuItem>
        <Divider />
        <MenuItem onClick={onDelete} sx={{ color: "error.main" }}>
          <Icon icon={trash2Outline} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Delete
          </Typography>
        </MenuItem>
      </Menu>
    </>
  );
}
const AgentTransactions = () => {
  const { themeStretch } = useSettings();
  // const { user } = useAuth();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  const theme = useTheme();
  const [loading, setLoading] = useState(false);
  const [allTransaction, setAllTransaction] = useState([]);
  const [updateResponse, setUpdateResponse] = useState("");
  const [transactionId, setTransactionId] = useState("");
  const { state } = useLocation();

  /* -------------------------------------------------APi---------------------------------------------- */
  const getAlltranscation = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      userid: state.userid,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(`${hostname}all/agent/transactions`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.length === undefined) {
          setLoading(false);
        } else {
          setLoading(true);
        }
        if (result) {
          setAllTransaction(result);
          setLoading(false);
        }
      })
      .catch((error) => console.log("error"));
  };

  const UpdateBalance = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      userid: state.userid,
      _id: transactionId,
      isactive: true,
    });

    var requestOptions = {
      method: "PATCH",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    await fetch(`${hostname}update/transaction/status`, requestOptions)
      .then((response) => response.json())
      .then((result) => setUpdateResponse(result))
      .catch((error) => console.log("error"));
  };

  useEffect(() => getAlltranscation(), []);
  /* -------------------------------------------------APi---------------------------------------------- */
  const handleUpdateBalance = (id) => {
    setTransactionId(id);
    UpdateBalance();
  };

  const handleClickPrint = (id) => {};
  const handleClickShare = () => {};
  const handleClickDelete = () => {};

  return (
    <Page title="User: List | Traveloes">
      <Container maxWidth={themeStretch ? false : "lg"}>
        <HeaderBreadcrumbs
          heading="Approval Requests"
          links={[
            { name: "Dashboard", href: PATH_DASHBOARD.root },
            { name: "Requests", href: PATH_DASHBOARD.user.root },
          ]}
        />

        {/* {updateResponse.status === true
          ? alert("Your balance has been updated")
          : alert("Your balance has not been updated")} */}

        <Card>
          <CardHeader title="Booking Details" sx={{ mb: 3 }} />
          <Scrollbar>
            <TableContainer sx={{ minWidth: 720 }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell sx={{ minWidth: 240 }}>Payment Source</TableCell>
                    <TableCell sx={{ minWidth: 160 }}>CreatedAt</TableCell>
                    <TableCell sx={{ minWidth: 120 }}>Type</TableCell>
                    <TableCell sx={{ minWidth: 200 }}>Amount </TableCell>
                    <TableCell sx={{ minWidth: 120 }}>Status</TableCell>
                    <TableCell />
                  </TableRow>
                </TableHead>
                {allTransaction.status === true ? (
                  <TableBody>
                    {allTransaction.data
                      .filter((item) => item.type === "cr")
                      .map((row) => (
                        <TableRow key={row.id}>
                          <TableCell>
                            <Stack
                              direction="row"
                              alignItems="center"
                              spacing={2}
                            >
                              <Avatar alt={row.userid} src={row.avatar} />
                              <Typography variant="subtitle2">
                                {row.paymentSource}
                              </Typography>
                            </Stack>
                          </TableCell>

                          <TableCell>
                            <Label>
                              {format(new Date(row.createdAt), "dd/MM/yyyy")}
                            </Label>
                          </TableCell>

                          <TableCell>
                            <Label xs={{ textTransform: "uppercase" }}>
                              {row.type === "cr" ? "cr" : null}
                            </Label>
                          </TableCell>

                          {/* <TableCell>{row.phoneNumber}</TableCell> */}
                          <TableCell sx={{ textTransform: "capitalize" }}>
                            <Label>₹ {row.amount}</Label>
                          </TableCell>
                          <TableCell sx={{ textTransform: "capitalize" }}>
                            <Label
                              sx={{
                                backgroundColor:
                                  row.isactive === true ? "#d6feab" : "#fff6e5",
                                color:
                                  row.isactive === true ? "#49ab81" : "#f97300",
                              }}
                            >
                              {" "}
                              {row.isactive === true
                                ? "Approved"
                                : "Approval pending"}
                            </Label>
                          </TableCell>

                          <TableCell align="right">
                            <MoreMenuButton
                              onUpdate={() => handleUpdateBalance(row._id)}
                              onPrint={handleClickPrint}
                              onShare={handleClickShare}
                              onDelete={handleClickDelete}
                            />
                          </TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                ) : (
                  <Typography as="h3" sx={{ padding: 5 }}>
                    {allTransaction.message}
                  </Typography>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <Divider />

          <Box sx={{ p: 2, textAlign: "right" }}>
            <Button
              to="#"
              size="small"
              color="inherit"
              //   component={RouterLink}
              endIcon={<Icon icon={arrowIosForwardFill} />}
            >
              View All
            </Button>
          </Box>
        </Card>
      </Container>
    </Page>
  );
};

export default AgentTransactions;
