// material
import { Container, Grid, Stack } from "@material-ui/core";
// hooks
import useAuth from "../../hooks/useAuth";
import useSettings from "../../hooks/useSettings";
// components
import Page from "../../components/Page";
import {
  AppWelcome,
  AppWidgets1,
  AppWidgets2,
  AppFeatured,
  AppNewInvoice,
  AppTopAuthors,
  AppTopRelated,
  AppAreaInstalled,
  AppTotalDownloads,
  AppTotalQueries,
  AppTotalInstalled,
  AppCurrentDownload,
  AppTotalActiveUsers,
  AppTopInstalledCountries,
} from "../../components/_dashboard/general-app";
import { BookingDetails } from "../../components/_dashboard/general-booking";
// ----------------------------------------------------------------------

export default function GeneralApp() {
  const { themeStretch } = useSettings();
  // const { user } = useAuth();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);

  return (
    <Page title="General: App | Traveloes">
      <Container maxWidth={themeStretch ? false : "xxxl"}>
        <Grid container spacing={1}>
          <Grid item xs={12} md={3}>
            <AppTotalActiveUsers />
          </Grid>
          <Grid item xs={12} md={3}>
            <AppTotalInstalled />
          </Grid>
          <Grid item xs={12} md={3}>
            <AppTotalDownloads />
          </Grid>
          <Grid item xs={12} md={3}>
            <AppTotalQueries />
          </Grid>
          <Grid item xs={14} md={12}>
            <AppWelcome />
          </Grid>
          {/* <Grid item xs={12}>
            <BookingDetails />
          </Grid> */}
          {/* <Grid item xs={12}>
            <BookingDetails />
          </Grid> */}
          {/* <Grid item xs={12} md={4}>
            <AppFeatured />
          </Grid> */}

          <Grid item xs={12} md={6} lg={5}>
            <AppCurrentDownload />
          </Grid>

          <Grid item xs={12} md={6} lg={7}>
            <AppAreaInstalled />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopRelated />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopInstalledCountries />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopAuthors />
          </Grid>

          {/* <Grid item xs={12} md={6} lg={4}>
            <Stack spacing={3}>
              <AppWidgets1 />
              <AppWidgets2 />
            </Stack>
          </Grid> */}
        </Grid>
      </Container>
    </Page>
  );
}
