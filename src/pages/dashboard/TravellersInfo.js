import React, { useState, useEffect } from "react";
import Footer from "../FinalPages/FinalpageFooter";
// import Header from "../FinalPages/FinalPageheader";
// import TopNav from "../FinalPages/FinalpageFooter";
import First from "../FinalPages/First";
import Second from "../FinalPages/Second";
// import Third from "../FinalPages/Third";
import Collapse from "react-bootstrap/Collapse";
import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";
import { styled } from "@material-ui/core/styles";
import axios from "axios";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import {
  List,
  ListItem,
  Typography,
  Button,
  Card,
  CardContent,
  Container,
  Grid,
  Stack,
  Skeleton,
} from "@material-ui/core";
import LoadingScreen from "src/components/LoadingScreen";
import SecondPage from "../FinalPages/secondPage";
import useSettings from "src/hooks/useSettings";
import { PATH_DASHBOARD } from "src/routes/paths";
import HeaderBreadcrumbs from "src/components/HeaderBreadcrumbs";
import { useLocation, useNavigate } from "react-router-dom";
import useAuth from "src/hooks/useAuth";
import { Icon } from "@iconify/react";
import { flightHost, hostname } from "src/HostName";
import AccountPopover from "src/layouts/dashboard/AccountPopover";
import { AppWidgets1 } from "src/components/_dashboard/general-app";
import { SelectedData } from "../../redux/slices/Action";
import { useSelector } from "react-redux";
const TravellersInfo = (props) => {
  const [value, setValue] = useState(false);
  const [isLoading, setIsloading] = useState(true);
  const [fares, setFares] = useState([]);
  const { themeStretch } = useSettings();
  // const { user } = useAuth();

  const location = useLocation();
  const navigate = useNavigate();
  // const data = useSelector(SelectedData);
  const bodyDataa = localStorage.getItem("body");
  const data = JSON.parse(bodyDataa);
  const flightResult = location.state.data;
  // const sessionId = location.state.sessionId.SessionID;
  // const TRAVELLERS_COUNT = location.state.sessionId;

  /* ----------------------------------Styles------------------------------------------- */
  const DIV = styled(Card)(({ theme }) => ({
    boxShadow: "none",
    textAlign: "center",
    backgroundColor: theme.palette.primary.lighter,
    [theme.breakpoints.up("md")]: {
      textAlign: "left",
      alignItems: "center",
      justifyContent: "space-between",
      padding: 10,
      margin: 10,
    },
  }));
  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
    ...theme.typography.body2,
    width: "25rem",
    padding: theme.spacing(1),
    color: theme.palette.text.secondary,
  }));
  const RootStylee = styled(Card)(({ theme }) => ({
    backgroundColor: theme.palette.gradients.lighter,
    paddingTop: theme.spacing(5),
  }));
  /* --------------------------------Styles---------------------------------------------- */
  /* -------------------------------Api CAll--------------------------------------------- */
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  const FareQuote = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      userid: user.userid,
      SessionID: "",
      SelectedFlightDetail: {
        flightResult: flightResult,
      },
    });
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(`${flightHost}v1/api/FarePrice`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setFares(result.BookingFlightDetails);
          setIsloading(false);
        }
      })
      .catch((error) => console.log("error"));
  };
  useEffect(() => FareQuote(), []);

  const percentAmount = flightResult.fare.grandTotal / 2;

  /* -------------------------------Api CAll--------------------------------------------- */
  if (isLoading) {
  }
  return (
    <>
      <Stack
        direction="row-reverse"
        sx={{ marginRight: 2, marginTop: 3 }}
        alignItems="center"
        spacing={{ xs: 0.5, sm: 1.5 }}
      >
        <AppWidgets1 />
        <Button variant="contained" onClick={() => navigate("/dashboard/app")}>
          Go to dashboard
        </Button>
      </Stack>
      <Container
        maxWidth={themeStretch ? false : "xxxl"}
        spacing={4}
        sx={{ margin: 1 }}
      >
        <HeaderBreadcrumbs
          heading="Travellers Info"
          links={[
            { name: "Flight Engine", href: PATH_DASHBOARD.root },
            {
              name: "Result",
              href: PATH_DASHBOARD.farerule.farerule,
            },
          ]}
        />
        <RootStylee>
          <CardContent
            sx={{
              p: { md: 0 },
              pl: { md: 5 },
            }}
          >
            <Box sx={{ flexGrow: 1 }}>
              <Grid container spacing={2}>
                <Grid xs={2} md={12} sm={5}></Grid>
                <Grid item xs={8}>
                  <Stack sx={{ margin: 1 }}>
                    <First FlightResult={flightResult} />
                  </Stack>
                  <DIV>
                    <Stack sx={{ margin: 2 }}>
                      <Stack>
                        <Typography variant="h4">Good to Know</Typography>
                      </Stack>
                      <Stack>
                        <Typography variant="subtitle1">
                          Information you should know
                        </Typography>
                      </Stack>

                      <List
                        sx={{
                          listStyleType: "circle",
                          pl: 2,
                          "& .MuiListItem-root": {
                            display: "list-item",
                          },
                        }}
                      >
                        <ListItem className="ps-0 pb-0 pt-1">
                          Airline Cancellation Fee is Rs{" "}
                          {percentAmount.toFixed(0)} per person per sector for
                          your selected flight on the sector{" "}
                          {flightResult.outBound[0].fromAirport} to{" "}
                          {flightResult.outBound[0].toAirport}
                        </ListItem>

                        <ListItem className="ps-0 pb-0 pt-1">
                          Certify your health status through the Aarogya Setu
                          app or the self-declaration form at the airport
                        </ListItem>
                        <ListItem className="ps-0 pb-0 pt-1">
                          Remember to web check-in before arriving at the
                          airport
                        </ListItem>
                        <ListItem className="ps-0 pb-0 pt-1">
                          Face masks are compulsory
                        </ListItem>
                      </List>
                    </Stack>
                  </DIV>
                  <DIV>
                    <Stack sx={{ margin: 2 }}>
                      <Stack>
                        <Typography variant="h4">More Information</Typography>
                      </Stack>
                      <Stack>
                        <List
                          sx={{
                            listStyleType: "circle",
                            pl: 2,
                            "& .MuiListItem-root": {
                              display: "list-item",
                            },
                          }}
                        >
                          <ListItem className="ps-0 pb-0 pt-1">
                            You have selected Hand Baggage Only fare on the
                            sector Departure - Arrival. Check-In baggage needs
                            to be purchased separately with this ticket.
                          </ListItem>

                          <ListItem className="ps-0 pb-0 pt-1">
                            Transit visa may be required for your selected
                            flight.Please reverify this from airlines.
                          </ListItem>

                          <ListItem className="ps-0 pb-0 pt-1">
                            <b>Airline Guidelines</b>&nbsp; Please check
                            airline-specific guidelines before booking.
                            Individual airlines may have different visa & entry
                            regulation criteria.
                          </ListItem>

                          <ListItem className="ps-0 pb-0 pt-1">
                            <b>Country Guidelines</b> &nbsp; In addition,
                            country guidelines may also change without notice,
                            hence please do check travel rules on their
                            regulatory website prior to booking
                          </ListItem>
                        </List>
                      </Stack>
                    </Stack>
                  </DIV>
                </Grid>
                {isLoading ? (
                  <Grid item xs={4}>
                    <Item sx={{ mt: 4 }}>
                      <Skeleton animation="wave" />
                      <Skeleton animation="wave" />
                      <Skeleton animation="wave" />
                      <Skeleton animation="wave" />
                    </Item>
                  </Grid>
                ) : (
                  <>
                    {" "}
                    <Grid item xs={4}>
                      <Item sx={{ mt: 3, overflow: "hidden" }} className="p-0">
                        <h3 className="font-bold text-lg  px-3">
                          <i class="fas fa-credit-card text-blue-500 pr-3"></i>
                          PRICE DETAILS{" "}
                        </h3>
                        <Stack
                          direction="row"
                          className="d-flex align-items-center justify-content-between px-2"
                        >
                          <Stack>
                            <p className="text-black font-bold px-2 text-sm">
                              {data.adult} Traveler(s) : Adult{" "}
                            </p>{" "}
                            <p className="text-sm  pb-2 mb-0 px-2">
                              Flight charges per adult
                            </p>
                            <p className="text-sm  pb-2 mb-0 px-2">
                              Taxes & Fees per adult{" "}
                            </p>
                          </Stack>
                          <Stack className="text-right">
                            <p className="text-black font-bold text-sm">
                              ₹&nbsp;{" "}
                              {(
                                (fares[0].fare.adultFare +
                                  fares[0].fare.adultMarkup +
                                  fares[0].fare.adultTax) *
                                data.adult
                              ).toFixed(0)}
                            </p>
                            <p className="text-sm  pb-2 mb-0">
                              ₹&nbsp;{fares[0].fare.adultFare.toFixed(0)}
                            </p>
                            <p className="text-sm  pb-0 mb-2 mt-0 ">
                              ₹&nbsp;
                              {(
                                fares[0].fare.adultTax +
                                fares[0].fare.adultMarkup
                              ).toFixed(0)}
                            </p>
                          </Stack>
                        </Stack>
                        {data.children === 0 ? null : (
                          <>
                            <Stack
                              direction="row"
                              className="d-flex align-items-center justify-content-between px-2"
                            >
                              <Stack justifyContent="flex-start">
                                <p className="text-black font-bold px-2 text-sm">
                                  {data.children} Traveler(s) : child{" "}
                                </p>{" "}
                                <p className="text-sm  pb-2 mb-0 px-2">
                                  Flight charges per child
                                </p>
                                <p className="text-sm  pb-2 mb-0 px-2">
                                  Taxes & Fees per child{" "}
                                </p>
                              </Stack>
                              <Stack className="text-right">
                                <p className="text-black font-bold text-sm">
                                  ₹&nbsp;
                                  {(
                                    (fares[0].fare.childFare +
                                      fares[0].fare.childMarkup +
                                      fares[0].fare.childTax) *
                                    data.children
                                  ).toFixed(0)}
                                </p>
                                <p className="text-sm  pb-2 mb-0">
                                  ₹&nbsp;{fares[0].fare.childFare.toFixed(0)}
                                </p>
                                <p className="text-sm  pb-0 mb-2 mt-0 ">
                                  ₹&nbsp;{" "}
                                  {(
                                    fares[0].fare.childTax +
                                    fares[0].fare.childMarkup
                                  ).toFixed(0)}
                                </p>
                              </Stack>
                            </Stack>
                          </>
                        )}
                        {data.infant === 0 ? null : (
                          <>
                            {fares[0].fare.infantFare === 0 ? null : (
                              <>
                                <Stack
                                  direction="row"
                                  className="d-flex align-items-center justify-content-between px-2"
                                >
                                  <Stack>
                                    <p className="text-black font-bold px-2 text-sm">
                                      {data.infant} Traveler(s) : infant{" "}
                                    </p>{" "}
                                    <p className="text-sm  pb-2 mb-0 px-2">
                                      Flight charges per infant
                                    </p>
                                    <p className="text-sm  pb-2 mb-0 px-2">
                                      Taxes & Fees per infant{" "}
                                    </p>
                                  </Stack>
                                  <Stack className="text-right">
                                    <p className="text-black font-bold text-sm">
                                      ₹&nbsp;
                                      {(
                                        (fares[0].fare.infantFare +
                                          fares[0].fare.infantMarkup +
                                          fares[0].fare.infantTax) *
                                        data.infant
                                      ).toFixed(0)}
                                    </p>
                                    <p className="text-sm  pb-2 mb-0">
                                      ₹&nbsp;
                                      {fares[0].fare.infantFare.toFixed(0)}
                                    </p>
                                    <p className="text-sm  pb-0 mb-2 mt-0 ">
                                      ₹&nbsp;
                                      {(
                                        fares[0].fare.infantTax +
                                        fares[0].fare.infantMarkup
                                      ).toFixed(0)}
                                    </p>
                                  </Stack>
                                </Stack>
                              </>
                            )}
                          </>
                        )}
                        <DIV
                          className="m-0 d-flex "
                          style={{ borderRadius: 0 }}
                        >
                          <Typography
                            className="text-black font-bold  "
                            variant="h6"
                          >
                            Total Price :
                          </Typography>
                          <Typography className="text-black" variant="h6">
                            ₹&nbsp;{" "}
                            {(
                              (fares[0].fare.adultFare +
                                fares[0].fare.adultMarkup +
                                fares[0].fare.adultTax) *
                                data.adult +
                              (fares[0].fare.childFare +
                                fares[0].fare.childMarkup +
                                fares[0].fare.childTax) *
                                data.children +
                              (fares[0].fare.infantFare +
                                fares[0].fare.infantMarkup +
                                fares[0].fare.infantTax) *
                                data.infant
                            ).toFixed(0)}
                          </Typography>
                        </DIV>
                      </Item>
                    </Grid>
                  </>
                )}
                <Grid item xs={8}>
                  <Stack sx={{ marginLeft: 1, marginRight: 1 }}>
                    <Second
                      TravellersCount={1}
                      flight={flightResult}
                      setValue={setValue}
                      flightDetails={fares}
                    />
                  </Stack>
                </Grid>
              </Grid>
            </Box>
          </CardContent>
        </RootStylee>
      </Container>
    </>
  );
};
export default TravellersInfo;
