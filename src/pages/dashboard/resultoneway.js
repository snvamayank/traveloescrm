import React, { useEffect } from "react";
import { useState } from "react";
import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";
import gif from "../../assets/Image/plane3.gif";
import { Link } from "react-router-dom";
import morning from "../../assets/Image/morning.svg";
import noon from "../../assets/Image/noon.svg";
import night from "../../assets/Image/night.svg";
import evening from "../../assets/Image/evening.svg";
import { styled } from "@material-ui/core/styles";
import {
  Typography,
  Card,
  CardContent,
  Box,
  Container,
  Grid,
  Stack,
  Button,
  Slider,
  Divider,
} from "@material-ui/core";
import Modal from "react-bootstrap/Modal";
import AirPortData from "../../Api/SampleData";
import { useSelector } from "react-redux";
import { SelectedData } from "./Action";
import { Router, useNavigate } from "react-router";
import { flightHost, hostname } from "src/HostName";
import useAuth from "src/hooks/useAuth";
import arrowheadrightfill from "@iconify/icons-eva/arrowhead-right-fill";
import calendaroutline from "@iconify/icons-eva/calendar-outline";
import personfill from "@iconify/icons-eva/person-fill";
import briefcaseoutline from "@iconify/icons-eva/briefcase-outline";
import { Icon } from "@iconify/react";
import paperPlane from "../../LottieView/paper-flight.json";
import Lottie from "react-lottie";
import { AppWidgets1 } from "src/components/_dashboard/general-app";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlane } from "@fortawesome/free-solid-svg-icons";
import moment from "moment";
import AccountPopover from "src/layouts/dashboard/AccountPopover";
import { useTheme } from "@material-ui/core/styles";
import LoadingScreen from "src/components/LoadingScreen";
import { index } from "d3-array";
import OneWayFlightCard from "src/components/customFlightComponent/OneWayFlightCard";
import FlightDetailModal from "src/components/customFlightComponent/FlightDetailModal";
import { stop } from "src/utils/stop";
// import OneWayFilter from "src/components/customFlightComponent/OneWayFilter";

const DIV = styled(Card)(({ theme }) => ({
  boxShadow: "none",
  textAlign: "center",
  backgroundColor: theme.palette.primary.light,
  [theme.breakpoints.up("md")]: {
    textAlign: "left",
    paddingLeft: 20,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },
}));

const Resultoneway = (props) => {
  const navigate = useNavigate();
  const theme = useTheme();
  // const { user } = useAuth();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  const [result, setResult] = useState([]);
  const [range, setRange] = useState(0);
  const [modal, setModal] = useState();
  const [show, setShow] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const [isloading, setIsloading] = useState(true);
  const [AirResult, setAirResult] = useState([]);
  const [AirResult2, setAirResult2] = useState([]);
  const [filteredDataPrice, setFilteredDataPrice] = useState([]);
  const [filtersort, setfiltersort] = useState(false);
  const [sort, setsort] = useState([]);
  const [sortStop, setsortStop] = useState([]);
  const [priceRange, setPriceRange] = useState([0, 30]);
  const handleClose = () => setShow(false);
  const handleShow = (items) => {
    setShow(true);
    setModal(items);
  };
  const bodyDataa = localStorage.getItem("body");
  const bodyData = JSON.parse(bodyDataa);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: paperPlane,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  // const priceRange = (value) => {
  //   setRange(value);
  // };

  /* Converting Time  */
  const ConvertMinsToTime = ({ data }) => {
    let hours = Math.floor(data / 60);
    let minutes = data % 60;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    return `${hours}h:${minutes}m`;
  };
  const convertFrom24To12Format = (time24) => {
    const [sHours, minutes] = time24.match(/([0-9]{1,2}):([0-9]{2})/).slice(1);
    const period = +sHours < 12 ? "AM" : "PM";
    const hours = +sHours % 12 || 12;

    return `${hours}:${minutes} ${period}`;
  };

  function CallApi() {
    var data = {
      Trip: 1,
      TripType: 1,
      Adult: bodyData.adult,
      Child: bodyData.children,
      Infant: bodyData.infant,
      NonStop: false,
      PreferredClass: 0,
      PreferredCarrier: "",
      Currency: "INR",
      Meta: "traveloes_himani",
      Segments: [
        {
          Origin: bodyData.departure,
          Destination: bodyData.arrival,
          DepartDate: bodyData.singleDate,
          PreferredClass: 0,
          ReturnDate: bodyData.endDates,
        },
      ],
    };
    var config = {
      method: "post",
      url: `${flightHost}v1/api/searchResultAll`,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        if (response.data.length === undefined) {
          setIsloading(false);
          setResult(response);
          setAirResult2(response.data.flightResult);
          setAirResult(response.data.flightResult);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  const handleFilterstop = (e) => {
    setAirResult([
      ...result.data.flightResult.filter(
        (item) => item.outBound.length == e.target.value
      ),
    ]);
  };
  const handleFilterAirline = (value) => {
    setAirResult([
      ...result.data.flightResult.filter((item) => item.valCarrier === value),
    ]);
  };
  const handleChange = (event, newValue) => {
    // const priceFilter = result.filter(
    //   (items, index) => priceRange[0] <= items.fare.grandTotal
    // );
    setPriceRange(newValue);
    // setFilteredDataPrice(priceFilter);
  };
  const MaxFareRange = Math.max(
    ...AirResult.map((item) => item.fare.grandTotal)
  ).toFixed(0);

  const MinfareRange = Math.min(
    ...AirResult.map((item) => item.fare.grandTotal)
  ).toFixed(0);

  const findoutBleng = AirResult.map((item) => item.outBound.length);

  function find_duplicate_in_array(array) {
    const count = {};
    const result = [];

    array.forEach((item) => {
      if (count[item]) {
        count[item] += 1;
        return;
      }
      count[item] = 1;
    });

    for (let prop in count) {
      if (count[prop] >= 2) {
        result.push(prop);
      }
    }
    return result;
  }

  const Stop = [
    { id: 1, name: "Non Stop" },
    { id: 2, name: "1 Stop" },
    { id: 3, name: "2 Stop" },
  ];

  const getlengoutB = find_duplicate_in_array([...findoutBleng]);

  /* -----------------------------------------Departure & Return Shift Times----------------------------------------------------- */
  function departure(e) {
    if (e == "morning") {
      setAirResult([
        ...result.data.flightResult.filter(
          (items) =>
            items.outBound[0].depDate.slice(11, 13) >= 5 &&
            items.outBound[0].depDate.slice(11, 13) < 12
        ),
      ]);
    }
    if (e == "afternoon") {
      setAirResult([
        ...result.data.flightResult.filter(
          (items) =>
            items.outBound[0].depDate.slice(11, 13) >= 12 &&
            items.outBound[0].depDate.slice(11, 13) < 18
        ),
      ]);
    }
    if (e == "evening") {
      setAirResult([
        ...result.data.flightResult.filter(
          (items) =>
            items.outBound[0].depDate.slice(11, 13) >= 18 &&
            items.outBound[0].depDate.slice(11, 13) < 24
        ),
      ]);
    }
    if (e === "night") {
      setAirResult([
        ...result.data.flightResult.filter(
          (items) =>
            items.outBound[0].depDate.slice(11, 13) >= 0 &&
            items.outBound[0].depDate.slice(11, 13) < 5
        ),
      ]);
    }
  }

  function arrival(e) {
    if (e == "morning") {
      setAirResult([
        ...result.data.flightResult.filter(
          (items) =>
            items.outBound[0].reachDate.slice(11, 13) >= 5 &&
            items.outBound[0].reachDate.slice(11, 13) < 12
        ),
      ]);
    }
    if (e == "afternoon") {
      setAirResult([
        ...result.data.flightResult.filter(
          (items) =>
            items.outBound[0].reachDate.slice(11, 13) >= 12 &&
            items.outBound[0].reachDate.slice(11, 13) < 18
        ),
      ]);
    }
    if (e == "evening") {
      setAirResult([
        ...result.data.flightResult.filter(
          (items) =>
            items.outBound[0].reachDate.slice(11, 13) >= 18 &&
            items.outBound[0].reachDate.slice(11, 13) < 24
        ),
      ]);
    }
    if (e === "night") {
      setAirResult([
        ...result.data.flightResult.filter(
          (items) =>
            items.outBound[0].reachDate.slice(11, 13) >= 0 &&
            items.outBound[0].reachDate.slice(11, 13) < 5
        ),
      ]);
    }
  }
  /* -----------------------------------------Departure & Return Shift Times----------------------------------------------------- */
  /* --------------------------------------------------------Airline sort---------------------------------------------- */

  const airlinePrice = AirResult2.reduce(function (r, a) {
    r[a.airline] = r[a.airline] || [];
    r[a.airline].push(a);
    return r;
  }, Object.create(null));

  const modAirlinearray = Object.keys(airlinePrice).map(function (key) {
    var value = airlinePrice[key];

    return {
      airlineCode: key.substring(0, 2),
      firstPrice:
        value[0].fare.adultFare +
        value[0].fare.adultTax +
        value[0].fare.adultMarkup,
      alldata: value,
    };
  });

  const getUniqueSearches = uniqueByMultipleKey(modAirlinearray, [
    "airlineCode",
  ]);

  function uniqueByMultipleKey(arr, keyProps) {
    return Object.values(
      arr.reduce((uniqueMap, entry) => {
        const key = keyProps.map((k) => entry[k]).join("|");

        if (!(key in uniqueMap)) uniqueMap[key] = entry;
        return uniqueMap;
      }, {})
    );
  }

  const selectFile = (e) => {
    const checked = e.target.checked;
    const airlineFly = e.target.value;

    if (airlineFly == airlineFly) {
      {
        checked
          ? setAirResult([...sort, ...airlinePrice[airlineFly]])
          : setAirResult(sort.filter((items) => items.airline != airlineFly));
      }
      {
        checked
          ? setsort([...sort, ...airlinePrice[airlineFly]])
          : setsort(sort.filter((items) => items.airline != airlineFly));
      }
    }
  };

  /* --------------------------------------------------------Airline sort---------------------------------------------- */
  /* ---------------------------------------------------------------------Stops-------------------------------------------------------------------- */
  const stopFunction = (e) => {
    const checking = e.target.checked;
    const value = e.target.value;

    if (value == value) {
      if (checking) {
        // setAirResult(...sortStop,...AirResult.filter(item => item.outBound.length == value))
      } else {
        // setAirResult(AirResult.filter(item => item.outBound.length != value))
      }
    }
  };
  var Stops = [];
  /* ---------------------------------------------------------------------Stops-------------------------------------------------------------------- */

  function valuetext(value) {
    return `$${value}°C`;
  }

  const marks = [
    { value: 3000, label: "3k" },
    { value: 100000, label: "100k" },
  ];

  useEffect(() => {
    CallApi();
  }, []);

  if (result.length !== 0) {
    const airline = result.data.airline;
    return (
      <>
        {" "}
        {airline.length !== 0 ? (
          <Box
            sx={{
              marginTop: "20px",
              marginLeft: "60px",
              marginRight: "60px",
            }}
          >
            <Stack>
              <Stack
                direction="row-reverse"
                // sx={{ marginRight: "52px" }}
                alignItems="center"
                spacing={{ xs: 0.5, sm: 1.5 }}
              >
                {/* <AccountPopover /> */}
                <AppWidgets1 />
                <Button
                  variant="contained"
                  onClick={() => navigate("/dashboard/app")}
                >
                  Go to dashboard
                </Button>
              </Stack>
            </Stack>
            {isloading === true ? (
              <Box sx={{ w: 110 }}>
                <Lottie options={defaultOptions} height={400} width={400} />
              </Box>
            ) : (
              <>
                <Card
                  sx={{
                    p: 3,
                    marginTop: "25px",
                    marginLeft: "10px",
                    // marginRight: "50px",
                  }}
                >
                  <Grid>
                    <Stack direction={"row"} sx={{ alignItems: "center" }}>
                      <Typography variant="h4">
                        {bodyData.departure}, &nbsp;
                        <Typography
                          sx={{ color: "#718096" }}
                          variant="h4"
                          component="span"
                        >
                          {AirPortData.filter(
                            (item) => item.airportCode == bodyData.departure
                          ).map((items) => items.airportName)}
                          ,&nbsp;
                        </Typography>
                        <Typography
                          component="span"
                          variant="h4"
                          sx={{ color: "primary.main" }}
                        >
                          {AirPortData.filter(
                            (item) => item.airportCode == bodyData.departure
                          ).map((items) => items.cityName)}
                        </Typography>
                      </Typography>
                      <Typography>
                        <Divider
                          style={{
                            width: "100px",
                            height: "1px",
                            marginLeft: "10px",
                            marginRight: "10px",
                          }}
                        />

                        {/* <Icon icon={arrowheadrightfill} width={20} height={20} /> */}
                      </Typography>
                      <Typography variant="h4">
                        {bodyData.arrival},&nbsp;
                        <Typography
                          sx={{ color: "#718096" }}
                          variant="h4"
                          component="span"
                        >
                          {AirPortData.filter(
                            (item) => item.airportCode == bodyData.arrival
                          ).map((items) => items.airportName)}
                          ,&nbsp;
                        </Typography>
                        <Typography
                          component="span"
                          variant="h4"
                          sx={{ color: "primary.main" }}
                        >
                          {AirPortData.filter(
                            (item) => item.airportCode == bodyData.arrival
                          ).map((items) => items.cityName)}
                        </Typography>
                      </Typography>
                    </Stack>
                  </Grid>
                  <Grid sx={{ display: "flex", alignItems: "center" }}>
                    <Stack sx={{ marginRight: "70px" }}>
                      <Stack direction={"row"} className="text-gray-500">
                        <Icon icon={calendaroutline} width={20} height={20} />
                        &nbsp; Travel Date &nbsp;
                        <Typography variant="subtitle1" className="text-black">
                          {moment(bodyData.singleDate).format("MMM DD, YYYY")}
                        </Typography>
                      </Stack>
                      <Stack direction={"column"} sx={{ mt: 1 }}>
                        <Stack direction={"row"} className="text-gray-500">
                          <Icon icon={personfill} width={25} height={25} />
                          &nbsp; Number of Adult &nbsp;
                          <Typography
                            variant="subtitle1"
                            className="text-black"
                          >
                            {bodyData.adult}
                          </Typography>
                        </Stack>
                        <Stack direction={"row"} className="text-gray-500">
                          {bodyData.children > 0 ? (
                            <>
                              <Icon icon={personfill} width={25} height={25} />
                              &nbsp; Number of Children &nbsp;
                              <Typography variant="subtitle1">
                                {bodyData.children}
                              </Typography>
                            </>
                          ) : null}
                        </Stack>
                        <Stack direction={"row"} className="text-gray-500">
                          {bodyData.infant > 0 ? (
                            <>
                              <Icon icon={personfill} width={25} height={25} />
                              &nbsp; Number of Infant &nbsp;
                              <Typography variant="subtitle1">
                                {bodyData.infant}
                              </Typography>
                            </>
                          ) : null}
                        </Stack>
                      </Stack>
                      <Stack direction={"row"} sx={{ mt: 2 }}>
                        <Typography variant="subtitle1">
                          {bodyData.class === 1 ? (
                            <Stack direction={"row"} className="text-gray-500">
                              <i class="fas fa-wheelchair text-gray-500 pt-1 mr-2"></i>
                              Airline Classes -{" "}
                              <Stack className="text-black">
                                &nbsp;Economy
                              </Stack>
                            </Stack>
                          ) : bodyData.class === 2 ? (
                            <Stack direction="row" className="text-gray-500">
                              <i class="fas fa-chair text-gray-500 pt-1 mr-2"></i>
                              Airline Classes -
                              <Stack className="text-black">
                                {" "}
                                &nbsp;Premium Economy
                              </Stack>
                            </Stack>
                          ) : bodyData.class === 3 ? (
                            <Stack direction="row" className="text-gray-500">
                              <i class="fas fa-couch text-gray-500 pt-1 mr-2"></i>
                              Airline Classes -{" "}
                              <Stack className="text-black">
                                &nbsp;Business class
                              </Stack>
                            </Stack>
                          ) : bodyData.class === 4 ? (
                            <Stack direction="row" className="text-gray-500">
                              <i class="fas fa-couch text-gray-500 pt-1 mr-2"></i>
                              Airline Classes -{" "}
                              <Stack className="text-black">
                                &nbsp;First class
                              </Stack>
                            </Stack>
                          ) : (
                            <Stack direction={"row"} className="text-gray-500">
                              <i class="fas fa-wheelchair text-gray-500 pt-1 mr-2"></i>
                              Airline Classes -{" "}
                              <Stack className="text-black">
                                &nbsp;Economy
                              </Stack>
                            </Stack>
                          )}
                        </Typography>
                      </Stack>
                    </Stack>
                    <Stack direction={"row"}>
                      {result.data.airline.map((items, i) => (
                        <div
                          key={i}
                          onClick={() => console.log("DivOnClickIsWorking")}
                          className="text-center  shadow-md bg-white p-3 rounded-md ml-10"
                        >
                          <div className="justify-center inline-flex flex-wrap">
                            <img
                              src={`https://www.travomint.com/resources/images/airline-logo/${items.code}.png`}
                              className="w-11 rounded-3xl"
                            />
                            <br />
                          </div>
                          <Typography>{items.name}</Typography>
                        </div>
                      ))}
                    </Stack>
                  </Grid>
                </Card>

                <div className="grid grid-cols-5 bg-white-200 mt-2">
                  <div className="overflow-hidden">
                    <div
                      className="mt-3 ml-3"
                      style={{
                        border: "1px solid ",
                        borderColor: theme.palette.primary.light,
                        borderRadius: "10px",
                      }}
                    >
                      <div className="up rounded-2xl mt-4 pb-2">
                        <div className="px-2 grid grid-cols-1 ">
                          <div className="grid grid-cols-2 px-2 py-2 text-left">
                            <p className="mb-0 text-black font-bold font-sans">
                              Filter
                            </p>
                            <p className="text-right mb-0 font-sans">
                              Reset All
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className=" py-2 grid grid-cols-1 ">
                        <div className="grid grid-cols-1 text-left">
                          <Disclosure defaultOpen="true">
                            {({ open }) => (
                              <>
                                <Disclosure.Button className="flex up justify-between w-full px-2 py-2  text-sm font-medium text-left text-gray-900 rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                                  <span className="text-md font-bold ml-2 font-sans">
                                    Stop From{" "}
                                    {AirPortData.filter(
                                      (items) =>
                                        items.airportCode === bodyData.departure
                                    ).map((ite) => (
                                      <> {ite.cityName}</>
                                    ))}
                                  </span>
                                  <ChevronUpIcon
                                    className={`${
                                      open ? "transform rotate-180" : ""
                                    } w-5 h-5 text-gray-900`}
                                  />
                                </Disclosure.Button>

                                <Disclosure.Panel className="px-4 pt-2 pb-2 text-sm text-gray-900">
                                  {getlengoutB.map((item, i) => {
                                    const minStopValue = Math.min(
                                      ...result.data.flightResult
                                        .filter(
                                          (items) =>
                                            items.outBound.length == item
                                        )
                                        .map((ite) => ite.fare.grandTotal)
                                    );

                                    const stops = stop(item);
                                    return (
                                      <div
                                        key={i}
                                        className="d-flex align-items-center mb-2"
                                      >
                                        <div className="d-flex align-items-center ">
                                          <input
                                            type="checkbox"
                                            id={i}
                                            className="mr-2 height-mint"
                                            style={{
                                              height: "15px",
                                              width: "15px",
                                            }}
                                            // checked={isChecked}
                                            value={item}
                                            onClick={(e) => handleFilterstop(e)}
                                            // name={item}
                                          />
                                        </div>
                                        <div className="d-flex align-items-center justify-content-between w-full">
                                          <div>
                                            <span className="text-xs font-bold font-sans">
                                              {stops}
                                            </span>
                                          </div>
                                          <div>
                                            <span className="text-xs font-bold font-sans">
                                              {" "}
                                              <i class="fas fa-rupee-sign fa-sm "></i>
                                              &nbsp; {minStopValue.toFixed(0)}
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                    );
                                  })}
                                </Disclosure.Panel>
                              </>
                            )}
                          </Disclosure>
                        </div>
                      </div>
                      <div className=" py-2 grid grid-cols-1 up rounded-2xl ">
                        <div className="grid grid-cols-1 text-left">
                          <Disclosure defaultOpen="true">
                            {({ open }) => (
                              <>
                                <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                                  <span className="text-md ml-2 font-bold font-sans">
                                    Price
                                  </span>
                                  <ChevronUpIcon
                                    className={`${
                                      open ? "transform rotate-180" : ""
                                    } w-5 h-5 text-gray-900`}
                                  />
                                </Disclosure.Button>

                                <Disclosure.Panel className="px-4 pt-2 pb-2 text-sm text-gray-900">
                                  <Slider
                                    getAriaLabel={() => "Temperature range"}
                                    value={priceRange}
                                    onChange={handleChange}
                                    valueLabelDisplay="auto"
                                    max={MaxFareRange}
                                    min={MinfareRange}
                                  />
                                </Disclosure.Panel>
                              </>
                            )}
                          </Disclosure>
                        </div>
                      </div>

                      <div className=" py-2 grid grid-cols-1 up rounded-2xl ">
                        <div className="grid grid-cols-1 text-left">
                          <Disclosure defaultOpen="true">
                            {({ open }) => (
                              <>
                                <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                                  <span className="text-md font-bold ml-2 font-sans">
                                    Departure From
                                    {AirPortData.filter(
                                      (items) =>
                                        items.airportCode === bodyData.departure
                                    ).map((ite) => (
                                      <> {ite.cityName}</>
                                    ))}
                                  </span>
                                  <ChevronUpIcon
                                    className={`${
                                      open ? "transform rotate-180" : ""
                                    }w-5 h-5 text-gray-900`}
                                  />
                                </Disclosure.Button>
                                <Disclosure.Panel className="px-2 pt-2 pb-2 text-sm text-gray-900">
                                  <div className="grid grid-cols-2   ">
                                    <div
                                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                                      onClick={() => departure("morning")}
                                    >
                                      <img src={morning} className="w-1/4 " />
                                      <span className="text-sh font-sans">
                                        5 AM - 12 PM
                                      </span>
                                    </div>
                                    <div
                                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                                      onClick={() => departure("afternoon")}
                                    >
                                      <img src={noon} className="w-1/4 " />
                                      <span className="text-sh font-sans">
                                        12 PM - 6 PM
                                      </span>
                                    </div>
                                  </div>
                                  <div className="grid grid-cols-2  mt-4">
                                    <div
                                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                                      onClick={() => departure("evening")}
                                    >
                                      <img src={evening} className="w-1/4" />
                                      <span className="text-sh font-sans">
                                        6 PM - 12 AM
                                      </span>
                                    </div>
                                    <div
                                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                                      onClick={() => departure("night")}
                                    >
                                      <img src={night} className="w-1/4" />
                                      <span className="text-sh font-sans">
                                        12 AM - 5 AM
                                      </span>
                                    </div>
                                  </div>
                                </Disclosure.Panel>
                              </>
                            )}
                          </Disclosure>
                        </div>
                      </div>

                      <div className=" py-2 grid grid-cols-1 up rounded-2xl">
                        <div className="grid grid-cols-1 text-left">
                          <Disclosure defaultOpen="true">
                            {({ open }) => (
                              <>
                                <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                                  <span className="text-md font-bold ml-2 font-sans">
                                    Arrival at{" "}
                                    {AirPortData.filter(
                                      (items) =>
                                        items.airportCode === bodyData.arrival
                                    ).map((ite) => (
                                      <> {ite.cityName}</>
                                    ))}
                                  </span>
                                  <ChevronUpIcon
                                    className={`${
                                      open ? "transform rotate-180" : ""
                                    } w-5 h-5 text-gray-900`}
                                  />
                                </Disclosure.Button>
                                <Disclosure.Panel className="px-2 pt-2 pb-2 text-sm text-gray-900">
                                  <div className="grid grid-cols-2   ">
                                    <div
                                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                                      onClick={() => arrival("morning")}
                                    >
                                      <img src={morning} className="w-1/4 " />
                                      <span className="text-sh font-sans">
                                        5 AM - 12 PM
                                      </span>
                                    </div>
                                    <div
                                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                                      onClick={() => arrival("afternoon")}
                                    >
                                      <img src={noon} className="w-1/4 " />
                                      <span className="text-sh font-sans">
                                        12 PM - 6 PM
                                      </span>
                                    </div>
                                  </div>
                                  <div
                                    className="grid grid-cols-2  mt-4"
                                    onClick={() => arrival("evening")}
                                  >
                                    <div className="justify-center d-flex flex-column align-items-center cursor-pointer">
                                      <img src={evening} className="w-1/4" />
                                      <span className="text-sh font-sans">
                                        6 PM - 12 PM
                                      </span>
                                    </div>
                                    <div
                                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                                      onClick={() => arrival("night")}
                                    >
                                      <img src={night} className="w-1/4" />
                                      <span className="text-sh font-sans ">
                                        12 AM - 5 AM
                                      </span>
                                    </div>
                                  </div>
                                </Disclosure.Panel>
                              </>
                            )}
                          </Disclosure>
                        </div>
                      </div>

                      <div className=" py-2 grid grid-cols-1 up rounded-2xl ">
                        <div className="grid grid-cols-1 text-left">
                          <Disclosure defaultOpen="true">
                            {({ open }) => (
                              <>
                                <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                                  <span className="text-md font-bold font-sans ml-2">
                                    Airlines
                                  </span>
                                  <ChevronUpIcon
                                    className={`${
                                      open ? "transform rotate-180" : ""
                                    } w-5 h-5 text-gray-900`}
                                  />
                                </Disclosure.Button>
                                <Disclosure.Panel className="px-3 pt-1  mt-1  rounded-2xl  pb-2 text-sm text-gray-900">
                                  {result.data.airline.map((item, i) => {
                                    const filterflightResult =
                                      result.data.flightResult.filter(
                                        (item) =>
                                          item.valCarrier ==
                                          result.data.airline[i].code
                                      );
                                    const minAirlineValue = Math.min(
                                      ...filterflightResult.map(
                                        (item) => item.fare.grandTotal
                                      )
                                    );

                                    return (
                                      <div
                                        className="d-flex align-items-center mb-2"
                                        key={i}
                                      >
                                        <div className="d-flex align-items-center ">
                                          <input
                                            type="checkbox"
                                            id="name"
                                            className="mr-2 height-mint"
                                            style={{
                                              height: "15px",
                                              width: "15px",
                                            }}
                                            // checked={isChecked}
                                            value={item.code}
                                            onClick={(e) =>
                                              handleFilterAirline(
                                                e.target.value
                                              )
                                            }
                                            name="name"
                                          />
                                        </div>
                                        <div className="d-flex align-items-center justify-content-between w-full">
                                          <div>
                                            <span className="text-xs font-bold font-sans">
                                              {item.name} &nbsp;(
                                              {filterflightResult.length})
                                            </span>
                                          </div>
                                          <div>
                                            <span className="text-xs font-bold font-sans">
                                              {" "}
                                              <i class="fas fa-rupee-sign fa-sm "></i>
                                              &nbsp;{" "}
                                              {minAirlineValue.toFixed(0)}
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                    );
                                  })}
                                </Disclosure.Panel>
                              </>
                            )}
                          </Disclosure>
                        </div>
                      </div>
                    </div>
                    {/* <OneWayFilter
                      handleFilterAirline={handleFilterAirline}
                      result={result}
                      bodyData={bodyData}
                    /> */}
                  </div>
                  <div className="col-span-4">
                    {isloading ? (
                      <img src={gif} className="w-full" />
                    ) : (
                      <>
                        {/* {AirResult.map((item, index) => {
                          const outBoundCount = item.outBound.length;

                          const fromAirport = item.outBound[0].fromAirport;
                          const toAirport =
                            item.outBound[outBoundCount - 1].toAirport;
                          const price = item.fare.grandTotal;

                          return (
                            <div
                              className="grid lg:grid-cols-6 grid-cols-1 px-10 py-3 mt-3 shadow rounded-2xl"
                              key={index}
                              style={{ marginLeft: "60px" }}
                            >
                              <div>
                                <div>
                                  <div className="pl-4">
                                    <img
                                      src={`https://www.travomint.com/resources/images/airline-logo/${item.outBound[0].airline}.png`}
                                      className=" rounded-3 "
                                      height="50px"
                                      width="50px"
                                      alt="airline"
                                    />
                                  </div>

                                  <div className="pl-4  col-span-2">
                                    <span className="text-sm text-black font-sans font-bold">
                                      {item.outBound[0].airlineName}
                                    </span>
                                    <span className="fs-6 text-gray-500 font-sans font-bold ">
                                      {" "}
                                      {item.outBound[0].airline}-
                                      {item.outBound[0].flightNo}
                                    </span>
                                  </div>
                                </div>
                                <button
                                  onClick={(e) => handleShow(item)}
                                  className="down border-gray-500 border-2 font-bold pt-1 pb-1 pl-3 text-sm pr-3 text-gray-600 mt-2 rounded-lg"
                                >
                                  <i className="far fa-hand-point-right"></i>{" "}
                                  Flight Details
                                </button>
                              </div>
                              <div className="col-span-4 lg:pl-5 lg:pr-5 pl-0 pr-0">
                                <div className="grid lg:grid-cols-4 grid-cols-1">
                                  <div className="text-center py-3">
                                    <span className="text-black font-bold">
                                      <div className="text-gray-600">
                                        {fromAirport}
                                      </div>
                                      <div>
                                        {convertFrom24To12Format(
                                          item.outBound[outBoundCount - 1]
                                            .depDate
                                        )}
                                      </div>
                                      <div className="text-gray-600">
                                        {moment(
                                          item.outBound[
                                            outBoundCount - 1
                                          ].depDate.split("T")[0]
                                        ).format("MMM DD, YYYY")}
                                      </div>
                                    </span>
                                  </div>
                                  <div className="col-span-2 text-center py-3">
                                    <div className="progress">
                                      <div
                                        className="progress-bar progress-bar-striped progress-bar-animated"
                                        role="progressbar"
                                        aria-valuenow="75"
                                        aria-valuemin="0"
                                        aria-valuemax="100"
                                        style={{ width: "100%" }}
                                      >
                                        <span>
                                          <FontAwesomeIcon
                                            icon={faPlane}
                                            className="text-gray-500"
                                          />
                                        </span>
                                      </div>
                                    </div>
                                    <div>
                                      {item.outBound[0].eft}
                                      <span className="d-block">
                                        Layover Time{" "}
                                      </span>
                                      <div className=" text-black font-bold">
                                        <ConvertMinsToTime
                                          data={item.outBound[0].eft}
                                        />
                                      </div>
                                      <div>
                                        {outBoundCount === 1
                                          ? "non-stop"
                                          : outBoundCount === 2
                                          ? "One-Stop"
                                          : outBoundCount === 3
                                          ? "Two-Stop"
                                          : outBoundCount === 4
                                          ? "Three-Stop"
                                          : null}{" "}
                                      </div>
                                    </div>
                                  </div>
                                  <div className="text-center py-3">
                                    <span className="text-black font-bold">
                                      <div className="text-gray-600">
                                        {toAirport}
                                      </div>
                                      <div>
                                        {convertFrom24To12Format(
                                          item.outBound[outBoundCount - 1]
                                            .reachDate
                                        )}
                                      </div>
                                      <div className="text-gray-600">
                                        {moment(
                                          item.outBound[
                                            outBoundCount - 1
                                          ].reachDate.split("T")[0]
                                        ).format("MMM DD, YYYY")}
                                      </div>
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div className="text-center">
                                <div className="mb-2 pt-3">
                                  <div className=" px-2 pt-0 ">
                                    <span className=" font-bold  font-sans text-gray-600">
                                      From
                                    </span>
                                  </div>
                                  <div className="pl-2 col-span-3">
                                    <span className="text-lg font-bold  font-sans text-black">
                                      <i class="fas fa-rupee-sign fa-sm "></i>{" "}
                                      {item.fare.grandTotal.toFixed(0)}
                                    </span>
                                    <br />
                                  </div>
                                </div>
                                <div>
                                  <Link
                                    to={"/Flight/travellersinfo"}
                                    state={{ data: item }}
                                    style={{ textDecoration: "none" }}
                                  >
                                    <Button variant="contained">
                                      Book Now&nbsp;
                                      {item.gdsType === 23
                                        ? "E-trav"
                                        : item.gdsType === 22
                                        ? "Scrapping"
                                        : item.gdsType === 20
                                        ? "Kehar"
                                        : item.gdsType === 21
                                        ? "Brightsun"
                                        : null}
                                    </Button>
                                  </Link>
                                </div>

                                {item.fare.FareType}
                              </div>
                            </div>
                          );
                        })} */}
                        <OneWayFlightCard
                          AirResult={AirResult}
                          handleShow={handleShow}
                          convertFrom24To12Format={convertFrom24To12Format}
                          ConvertMinsToTime={ConvertMinsToTime}
                        />
                      </>
                    )}
                    {/* <Modal
                      className="mt-20 overflow-hidden"
                      size="xl"
                      show={show}
                      onHide={handleClose}
                    >
                      <DIV>
                        <Modal.Header
                          className="text-black d-flex justify-content-between"
                          closeButton
                          closeVariant="black"
                        >
                          <Modal.Title>Flight Details</Modal.Title>
                        </Modal.Header>
                      </DIV>

                      <Modal.Body>
                        The baggage information is just for reference. Please
                        Check with airline before check-in. For more
                        information, visit the airline's official website.
                      </Modal.Body>
                      <>
                        {modal === undefined ? null : (
                          <>
                            {modal.outBound.map((item, index) => (
                              <Stack
                                spacing={{ xs: 2, md: 3 }}
                                columns={{ xs: 4, sm: 8, md: 12 }}
                                key={index}
                              >
                                <Stack
                                  direction="row"
                                  spacing={{ xs: 2, md: 3 }}
                                  columns={{ xs: 4, sm: 8, md: 12 }}
                                  className="align-items-center justify-content-around"
                                >
                                  <Stack>
                                    <Stack direction={"row"}>
                                      <Stack>
                                        <img
                                          src={`https://www.travomint.com/resources/images/airline-logo/${item.airline}.png`}
                                          className="w-11"
                                        />
                                      </Stack>
                                      <Typography
                                        sx={{
                                          color: " black",
                                          fontWeight: "bold",
                                          marginLeft: "5px",
                                        }}
                                      >
                                        {item.airlineName}
                                      </Typography>
                                      <Typography
                                        sx={{
                                          color: " black",
                                          fontWeight: "bold",
                                          marginLeft: "5px",
                                        }}
                                      >
                                        {item.airline}
                                      </Typography>
                                      <Typography
                                        sx={{
                                          color: " black",
                                          fontWeight: "bold",
                                          marginLeft: "5px",
                                        }}
                                      >
                                        {item.flightNo}
                                      </Typography>
                                    </Stack>
                                    <Stack>
                                      <Typography>
                                        Operated By {item.airlineName}
                                      </Typography>
                                    </Stack>
                                  </Stack>

                                  <Stack dircetion="row">
                                    <Stack>
                                      <Button
                                        size="small"
                                        sx={{
                                          width: "80px",
                                          height: "80px",
                                          borderRadius: "100px",
                                        }}
                                      >
                                        <Icon
                                          icon={briefcaseoutline}
                                          width={40}
                                          height={40}
                                        />
                                      </Button>
                                      <Typography>Cabin Baggage</Typography>
                                    </Stack>
                                    <Stack>
                                      <Typography>
                                        {item.cabinBaggage} Per Person
                                      </Typography>
                                    </Stack>
                                  </Stack>
                                  <Stack dircetion="row">
                                    <Stack>
                                      <Button
                                        size="small"
                                        sx={{
                                          width: "80px",
                                          height: "80px",
                                          borderRadius: "100px",
                                        }}
                                      >
                                        <Icon
                                          icon={briefcaseoutline}
                                          width={40}
                                          height={40}
                                        />
                                      </Button>
                                      <Typography>Check-In Baggage</Typography>
                                    </Stack>
                                    <Stack>
                                      <Typography>
                                        {item.baggage} Per Person
                                      </Typography>
                                    </Stack>
                                  </Stack>
                                </Stack>

                                <Grid
                                  spacing={{ xs: 2, md: 3 }}
                                  columns={{ xs: 4, sm: 8, md: 12 }}
                                  sx={{
                                    borderWidth: "1px",
                                    borderStyle: "dashed",
                                    borderColor: "grey",
                                    boxShadow: (theme) =>
                                      theme.customShadows.z8,
                                    borderRadius: "10px",
                                  }}
                                  className="d-flex justify-content-around align-items-center"
                                  style={{ margin: 10 }}
                                >
                                  <Stack
                                    spacing={{ xs: 2, md: 3 }}
                                    columns={{ xs: 4, sm: 8, md: 12 }}
                                    className="justify-content-center align-items-center"
                                  >
                                    <Stack
                                      sx={{
                                        backgroundColor: "whitesmoke",
                                        padding: "10px",
                                        borderRadius: "20px",
                                        fontSize: 12,
                                        marginTop: "10px",
                                        width: "60px",
                                        marginBottom: "-10px",
                                      }}
                                      className="justify-content-center align-items-center fw-bold"
                                    >
                                      <Typography
                                        sx={{
                                          fontSize: 12,
                                          fontWeight: "bold",
                                        }}
                                      >
                                        {item.fromAirport}
                                      </Typography>
                                    </Stack>
                                    <Stack
                                      style={{
                                        marginBottom: "-20px",
                                      }}
                                    >
                                      <Typography
                                        sx={{
                                          fontWeight: "bold",
                                        }}
                                      >
                                        {convertFrom24To12Format(
                                          item.depDate.split("T")[1]
                                        )}
                                      </Typography>
                                    </Stack>
                                    <Typography>
                                      {AirPortData.filter(
                                        (items) =>
                                          items.airportCode === item.fromAirport
                                      ).map((item) => item.cityName)}
                                    </Typography>
                                    <Typography>
                                      {AirPortData.filter(
                                        (items) =>
                                          items.airportCode === item.fromAirport
                                      ).map((item) => item.airportName)}
                                    </Typography>

                                    <Stack sx={{ marginBottom: "10px" }}>
                                      <Typography>
                                        {moment(
                                          item.depDate.split("T")[0]
                                        ).format("MMMM Do YYYY")}
                                      </Typography>
                                    </Stack>
                                  </Stack>
                                  <Stack
                                    spacing={{ xs: 2, md: 3 }}
                                    columns={{ xs: 4, sm: 8, md: 12 }}
                                    justifyContent="space-between"
                                    alignItems="center"
                                  >
                                    <Stack
                                      sx={{
                                        backgroundColor: "whitesmoke",
                                        padding: "10px",
                                        borderRadius: "20px",
                                        fontSize: 12,
                                        marginTop: "10px",
                                        width: "60px",
                                        marginBottom: "-10px",
                                      }}
                                      className="justify-content-center align-items-center fw-bold"
                                    >
                                      <Typography
                                        sx={{
                                          fontSize: 12,
                                          fontWeight: "bold",
                                        }}
                                      >
                                        {item.toAirport}
                                      </Typography>
                                    </Stack>
                                    <Stack
                                      style={{
                                        marginBottom: "-20px",
                                      }}
                                    >
                                      <Typography
                                        sx={{
                                          fontWeight: "bold",
                                        }}
                                      >
                                        {convertFrom24To12Format(
                                          item.reachDate.split("T")[1]
                                        )}
                                      </Typography>
                                    </Stack>

                                    <Typography>
                                      {AirPortData.filter(
                                        (items) =>
                                          items.airportCode === item.toAirport
                                      ).map((item) => item.cityName)}
                                    </Typography>
                                    <Typography>
                                      {AirPortData.filter(
                                        (items) =>
                                          items.airportCode === item.toAirport
                                      ).map((item) => item.airportName)}
                                    </Typography>
                                    <Stack>
                                      <Typography>
                                        {moment(
                                          item.reachDate.split("T")[0]
                                        ).format("MMMM Do YYYY")}
                                      </Typography>
                                    </Stack>
                                  </Stack>
                                </Grid>
                              </Stack>
                            ))}
                          </>
                        )}
                      </>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                          Close
                        </Button>
                      </Modal.Footer>
                    </Modal> */}
                    <FlightDetailModal
                      modal={modal}
                      show={show}
                      handleClose={handleClose}
                      convertFrom24To12Format={convertFrom24To12Format}
                    />
                  </div>
                </div>
              </>
            )}
          </Box>
        ) : (
          <div>
            <div
              className="fs-1"
              style={{
                position: "absolute",
                top: "40%",
                left: "40%",
              }}
            >
              Flight Not Found
            </div>
            <div
              style={{
                position: "absolute",
                top: "46.5%",
                left: "46.5%",
              }}
            >
              <Link to={"/flightbooking"}>Flight Booking</Link>
            </div>
          </div>
        )}
      </>
    );
  } else {
    return (
      <>
        <LoadingScreen />
      </>
    );
  }
};

export default Resultoneway;
