import React, { useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";
import morning from "../../assets/Image/morning.svg";
import noon from "../../assets/Image/noon.svg";
import night from "../../assets/Image/night.svg";
import evening from "../../assets/Image/evening.svg";
import Modal from "react-bootstrap/Modal";
import { useTheme } from "@material-ui/core/styles";
// import Button from "react-bootstrap/Button";
import plane from "../../assets/Image/plane6.gif";
import AirPortData from "../../Api/SampleData";
import { useSelector } from "react-redux";
import { SelectedData } from "./Action";
import { flightHost, hostname } from "src/HostName";
import useAuth from "src/hooks/useAuth";
import LoadingScreen from "src/components/LoadingScreen";
import { Box, Stack, Typography, Card } from "@material-ui/core";
import { alpha, styled } from "@material-ui/core/styles";
import moment from "moment";
import TwoFooter from "../../FlightEngine/TwoFooter";
import arrowheadrightfill from "@iconify/icons-eva/arrowhead-right-fill";
import calendaroutline from "@iconify/icons-eva/calendar-outline";
import personfill from "@iconify/icons-eva/person-fill";
import briefcaseoutline from "@iconify/icons-eva/briefcase-outline";
import { Icon } from "@iconify/react";
import axios from "axios";
import { AppWidgets1 } from "src/components/_dashboard/general-app";
import { Router, useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlane } from "@fortawesome/free-solid-svg-icons";
import {
  CardContent,
  Container,
  Grid,
  Button,
  Slider,
  Divider,
} from "@material-ui/core";
const RootStyle = styled(Box)({
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
  backgroundColor: "#eee",
});

const DIV = styled(Card)(({ theme }) => ({
  boxShadow: "none",
  textAlign: "center",
  backgroundColor: theme.palette.primary.light,
  [theme.breakpoints.up("md")]: {
    textAlign: "left",
    paddingLeft: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
  },
}));

const SearchEngine = () => {
  const theme = useTheme();
  const navigate = useNavigate();
  const bodyDataa = localStorage.getItem("body");
  const bodyData = JSON.parse(bodyDataa);
  const [result, setResult] = useState([]);
  const [range, setRange] = useState(0);
  const [modal, setModal] = useState();
  const [show, setShow] = useState(false);
  const [isloading, setIsloading] = useState(true);
  const [AirResult, setAirResult] = useState([]);
  const [AirResult2, setAirResult2] = useState([]);
  const [filtersort, setfiltersort] = useState(false);
  const [sort, setsort] = useState([]);
  const [sortStop, setsortStop] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [inBound, setInBound] = useState();
  const [outBound, setOutBound] = useState();
  const [priceRange, setPriceRange] = React.useState([0, 30000]);

  const handleClose = () => setShow(false);

  const handleShow = (items) => {
    setShow(true);
    setModal(items);
  };

  function closeModal() {
    setShow(false);
  }

  // const { user } = useAuth();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);

  const ConvertMinsToTime = ({ data }) => {
    let hours = Math.floor(data / 60);
    let minutes = data % 60;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    return `${hours}h:${minutes}m`;
  };

  const convertFrom24To12Format = (time24) => {
    const [sHours, minutes] = time24.match(/([0-9]{1,2}):([0-9]{2})/).slice(1);
    const period = +sHours < 12 ? "AM" : "PM";
    const hours = +sHours % 12 || 12;

    return `${hours}:${minutes} ${period}`;
  };

  function CallApi() {
    var data = {
      Trip: 2,
      TripType: 2,
      Adult: bodyData.adult,
      Child: bodyData.children,
      Infant: bodyData.infant,
      NonStop: false,
      PreferredClass: 0,
      PreferredCarrier: "",
      Currency: "INR",
      Meta: "traveloes_himani",
      Segments: [
        {
          Origin: bodyData.departure,
          Destination: bodyData.arrival,
          DepartDate: bodyData.startDates,
          PreferredClass: 0,
          ReturnDate: bodyData.endDates,
        },
      ],
    };
    var config = {
      method: "post",
      url: `${flightHost}v1/api/searchResultAll`,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        if (response.data) {
          setIsloading(false);
          setResult(response.data);
          setOutBound(
            response.data.flightResult.find((item) => {
              return item.outBound;
            })
          );
          setInBound(
            response.data.flightResult.find((items) => {
              return items.inBound;
            })
          );
        } else {
          setIsloading(true);
        }
      })
      .catch(function (error) {
        console.log(error.message);
      });
  }

  console.log("modal", modal);
  const handleChange = (event, newValue) => {
    setPriceRange(newValue);
  };
  useEffect(() => CallApi(), []);

  // const startDateFormat = moment(startDate).format("MM/DD/YYYY");
  // const endDateFormat = moment(endDate).format("MM/DD/YYYY");
  if (isloading) {
    return <LoadingScreen />;
  }

  return (
    <>
      {result.flightResult === undefined || result.flightResult.length === 0 ? (
        <div>
          <div
            className="fs-1"
            style={{
              position: "absolute",
              top: "40%",
              left: "40%",
            }}
          >
            Flight Not Found
          </div>
          <div
            style={{
              position: "absolute",
              top: "46.5%",
              left: "46.5%",
            }}
          >
            <Link to={"/flightbooking"}>Flight Booking</Link>
          </div>
        </div>
      ) : (
        <Box
          sx={{
            marginTop: "20px",
            marginLeft: "50px",
            marginRight: "50px",
          }}
        >
          <Stack>
            <Stack
              direction="row-reverse"
              alignItems="center"
              sx={{ marginBottom: "20px" }}
              spacing={{ xs: 0.5, sm: 1.5 }}
            >
              <AppWidgets1 />
              <Button
                variant="contained"
                onClick={() => navigate("/dashboard/app")}
              >
                Go to dashboard
              </Button>
            </Stack>
          </Stack>
          <Card sx={{ display: "flex", alignItems: "center", p: 3 }}>
            <Stack>
              <Stack direction={"row"} sx={{ alignItems: "center" }}>
                <h1>{bodyData.departure}</h1>
                <Icon icon={arrowheadrightfill} width={20} height={20} />
                <h1>{bodyData.arrival}</h1>
              </Stack>
              <Stack direction={"row"}>
                <Icon icon={calendaroutline} width={20} height={20} />
                <Typography variant="subtitle1">
                  {bodyData.startDates}
                </Typography>
                <Typography variant="subtitle1">{bodyData.endDates}</Typography>
              </Stack>
              <Stack direction={"row"} sx={{ mt: 1 }}>
                <Stack direction={"row"}>
                  <Icon icon={personfill} width={20} height={20} />
                  <Typography variant="subtitle1">{bodyData.adult}</Typography>
                </Stack>
                <Stack direction={"row"}>
                  <Icon icon={personfill} width={20} height={20} />
                  <Typography variant="subtitle1">
                    {bodyData.children}
                  </Typography>
                </Stack>
                <Stack direction={"row"}>
                  <Icon icon={personfill} width={20} height={20} />
                  <Typography variant="subtitle1">{bodyData.infant}</Typography>
                </Stack>
              </Stack>
            </Stack>

            {result.airline.map((items, i) => (
              <div
                onClick={() => console.log("DivOnClickIsWorking")}
                className="text-center  shadow-md bg-white p-3 rounded-md ml-10"
              >
                <div className="justify-center inline-flex flex-wrap">
                  <img
                    src={`https://www.travomint.com/resources/images/airline-logo/${items.code}.png`}
                    className="w-11 rounded-3xl"
                  />
                  <br />
                </div>
                <Typography> {items.name}</Typography>
                {/* {result.flightResult.map((Faremt) => (
                      <div className="text-center amount text-dark mt-0 font-bold">
                        ₹ {Faremt.fare.grandTotal}
                      </div>
                    ))} */}
              </div>
            ))}
          </Card>
          <div>
            <div className="grid grid-cols-5 bg-white-200">
              <div className="mt-4 ">
                <div className="px-2 py-2 mt-1 mr-4 border rounded-2xl">
                  <Transition appear show={isOpen} as={Fragment}>
                    <Dialog
                      as="div"
                      className="fixed bg-slate-200 inset-0 z-10 overflow-y-auto"
                      onClose={closeModal}
                    >
                      <div className="min-h-screen px-4 text-center">
                        <Transition.Child
                          as={Fragment}
                          enter="ease-out duration-300"
                          enterFrom="opacity-0"
                          enterTo="opacity-100"
                          leave="ease-in duration-200"
                          leaveFrom="opacity-100"
                          leaveTo="opacity-0"
                        >
                          <Dialog.Overlay className="fixed inset-0" />
                        </Transition.Child>

                        {/* This element is to trick the browser into centering the modal contents. */}
                        <span
                          className="inline-block h-screen align-middle"
                          aria-hidden="true"
                        >
                          &#8203;
                        </span>
                        <Transition.Child
                          as={Fragment}
                          enter="ease-out duration-300"
                          enterFrom="opacity-0 scale-95"
                          enterTo="opacity-100 scale-100"
                          leave="ease-in duration-200"
                          leaveFrom="opacity-100 scale-100"
                          leaveTo="opacity-0 scale-95"
                        >
                          <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                            <Dialog.Title
                              as="h3"
                              className="text-lg font-medium leading-6 text-gray-900"
                            >
                              <i class="fas fa-user border-4 bg-blue-500 text-white border-white drop-shadow-xl outline-2 outline-gray-500 p-2 rounded-3xl"></i>
                              <span className="ml-2">Sign In</span>
                            </Dialog.Title>
                            <div className="mt-6"></div>
                          </div>
                        </Transition.Child>
                      </div>
                    </Dialog>
                  </Transition>

                  <Modal
                    show={show}
                    size="xl"
                    className="mt-20"
                    onHide={handleClose}
                  >
                    <DIV>
                      <Modal.Header
                        // className="bg-gradient-to-r from-blue-500 to-blue-900 text-white"
                        closeButton
                        closeVariant="black"
                      >
                        <Modal.Title>Flight Details</Modal.Title>
                      </Modal.Header>
                    </DIV>

                    <Modal.Body>
                      <p className="px-20">
                        The baggage information is just for reference. Please
                        Check with airline before check-in. For more
                        information, visit the airline's official website.
                      </p>
                      {modal === undefined ? null : (
                        <>
                          {modal.outBound.map((item) => {
                            return <div></div>;
                          })}
                        </>
                      )}
                    </Modal.Body>

                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleClose}>
                        Close
                      </Button>
                    </Modal.Footer>
                  </Modal>
                  <div className="up rounded-2xl mt-4 pb-2">
                    <div className="px-2 grid grid-cols-1 ">
                      <div className="grid grid-cols-2 px-2 py-2 text-left">
                        <p className="mb-0 text-black font-bold font-sans">
                          Filter
                        </p>
                        <p className="text-right mb-0 font-sans">Reset All</p>
                      </div>
                    </div>

                    <div className="px-2 pb-2 grid grid-cols-1 ">
                      <div className="grid grid-cols-1 text-left">
                        <Disclosure defaultOpen="true">
                          {({ open }) => (
                            <>
                              <Disclosure.Button className="flex justify-between up mb-2  w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                                <span className="font-sans">Choose Stop</span>
                                <ChevronUpIcon
                                  className={`${
                                    open ? "transform rotate-180" : ""
                                  } w-5 h-5 text-gray-900`}
                                />
                              </Disclosure.Button>
                              <Disclosure.Panel className="px-2 pt-1   rounded-2xl  pb-2 text-sm text-gray-900">
                                <input type="radio" />
                                <span className=" text-xs font-bold font-sans">
                                  {" "}
                                  Non-Stop
                                </span>{" "}
                                <span className="float-right font-sans">
                                  <i class="fas fa-rupee-sign"></i> 8313.00
                                </span>
                                <br />
                              </Disclosure.Panel>
                            </>
                          )}
                        </Disclosure>
                      </div>
                    </div>
                  </div>
                  <div className="px-2 py-2 grid grid-cols-1 up rounded-2xl">
                    <div className="grid grid-cols-1 text-left">
                      <Disclosure defaultOpen="true">
                        {({ open }) => (
                          <>
                            <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                              <Slider
                                getAriaLabel={() => "Temperature range"}
                                value={priceRange}
                                onChange={handleChange}
                                valueLabelDisplay="auto"
                                // getAriaValueText={valuetext}
                              />
                              <ChevronUpIcon
                                className={`${
                                  open ? "transform rotate-180" : ""
                                } w-5 h-5 text-gray-900`}
                              />
                            </Disclosure.Button>

                            {/* <Disclosure.Panel className="px-2 pt-2 pb-2 text-sm text-gray-900">
                              <input
                                type="range"
                                id="vol"
                                name="vol"
                                min="0"
                                max="50"
                              />
                            </Disclosure.Panel> */}
                          </>
                        )}
                      </Disclosure>
                    </div>
                  </div>

                  <div className=" py-2 grid grid-cols-1 up rounded-2xl">
                    <div className="grid grid-cols-1 text-left">
                      <Disclosure defaultOpen="true">
                        {({ open }) => (
                          <>
                            <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                              <span className="text-lg font-bold ml-2 font-sans">
                                Onward Travel
                              </span>
                              <ChevronUpIcon
                                className={`${
                                  open ? "transform rotate-180" : ""
                                } w-5 h-5 text-gray-900`}
                              />
                            </Disclosure.Button>
                            <Disclosure.Panel className="px-2 pt-2 pb-2 text-sm text-gray-900">
                              <div className="mb-4">
                                <div className="text-sm font-bold ml-2 font-sans">
                                  Stop From{" "}
                                  {AirPortData.filter(
                                    (items) =>
                                      items.airportCode === bodyData.departure
                                  ).map((ite) => (
                                    <> {ite.cityName}</>
                                  ))}
                                </div>
                                <div className="ml-2">Non-Stop</div>
                                <div className="ml-2">One-Stop</div>
                                <div className="ml-2">2+stop</div>
                              </div>
                              <div className="mb-4">
                                <span className="text-sm font-bold ml-2 font-sans">
                                  Departure From
                                  {AirPortData.filter(
                                    (items) =>
                                      items.airportCode === bodyData.departure
                                  ).map((ite) => (
                                    <> {ite.cityName}</>
                                  ))}
                                </span>
                                <div className="grid grid-cols-2   ">
                                  <div className=" justify-center d-flex flex-column align-items-center ">
                                    <img src={morning} className="w-1/4 " />
                                    <span className="text-sh font-sans">
                                      5 AM - 12 PM
                                    </span>
                                  </div>
                                  <div className="justify-center d-flex flex-column align-items-center ">
                                    <img src={noon} className="w-1/4 " />
                                    <span className="text-sh font-sans">
                                      12 PM - 6 PM
                                    </span>
                                  </div>
                                </div>
                                <div className="grid grid-cols-2  mt-4">
                                  <div className="justify-center d-flex flex-column align-items-center">
                                    <img src={evening} className="w-1/4" />
                                    <span className="text-sh font-sans">
                                      6 PM - 12 AM
                                    </span>
                                  </div>
                                  <div className="justify-center d-flex flex-column align-items-center">
                                    <img src={night} className="w-1/4" />
                                    <span className="text-sh font-sans">
                                      12 AM - 5 AM
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div>
                                <span className="text-sm font-bold ml-2 font-sans">
                                  Arrival at{" "}
                                  {AirPortData.filter(
                                    (items) =>
                                      items.airportCode === bodyData.arrival
                                  ).map((ite) => (
                                    <> {ite.cityName}</>
                                  ))}
                                </span>
                                <div className="grid grid-cols-2   ">
                                  <div className=" justify-center d-flex flex-column align-items-center ">
                                    <img src={morning} className="w-1/4 " />
                                    <span className="text-sh font-sans">
                                      5 AM - 12 PM
                                    </span>
                                  </div>
                                  <div className="justify-center d-flex flex-column align-items-center ">
                                    <img src={noon} className="w-1/4 " />
                                    <span className="text-sh font-sans">
                                      12 PM - 6 PM
                                    </span>
                                  </div>
                                </div>
                                <div className="grid grid-cols-2 mt-4">
                                  <div className="justify-center d-flex flex-column align-items-center">
                                    <img src={evening} className="w-1/4" />
                                    <span className="text-sh font-sans">
                                      6 PM - 12 PM
                                    </span>
                                  </div>
                                  <div className="justify-center d-flex flex-column align-items-center">
                                    <img src={night} className="w-1/4" />
                                    <span className="text-sh font-sans ">
                                      12 AM - 5 AM
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </Disclosure.Panel>
                          </>
                        )}
                      </Disclosure>
                    </div>
                  </div>

                  <div className=" py-2 grid grid-cols-1 up rounded-2xl ">
                    <div className="grid grid-cols-1 text-left">
                      <Disclosure defaultOpen="true">
                        {({ open }) => (
                          <>
                            <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                              <span className="text-lg font-bold ml-2 font-sans">
                                Return Travel
                              </span>
                              <ChevronUpIcon
                                className={`${
                                  open ? "transform rotate-180" : ""
                                } w-5 h-5 text-gray-900`}
                              />
                            </Disclosure.Button>
                            <Disclosure.Panel className="px-2 pt-2 pb-2 text-sm text-gray-900">
                              <div className="mb-4">
                                <div className="text-sm font-bold ml-2 font-sans">
                                  Stop From{" "}
                                  {AirPortData.filter(
                                    (items) =>
                                      items.airportCode === bodyData.departure
                                  ).map((ite) => (
                                    <> {ite.cityName}</>
                                  ))}
                                </div>
                                <div className="ml-2">Non-Stop</div>
                                <div className="ml-2">One-Stop</div>
                                <div className="ml-2">2+stop</div>
                              </div>
                              <div className="mb-4">
                                <span className="text-sm font-bold ml-2 font-sans">
                                  Departure From
                                  {AirPortData.filter(
                                    (items) =>
                                      items.airportCode === bodyData.departure
                                  ).map((ite) => (
                                    <> {ite.cityName}</>
                                  ))}
                                </span>
                                <div className="grid grid-cols-2   ">
                                  <div className=" justify-center d-flex flex-column align-items-center ">
                                    <img src={morning} className="w-1/4 " />
                                    <span className="text-sh font-sans">
                                      5 AM - 12 PM
                                    </span>
                                  </div>
                                  <div className="justify-center d-flex flex-column align-items-center ">
                                    <img src={noon} className="w-1/4 " />
                                    <span className="text-sh font-sans">
                                      12 PM - 6 PM
                                    </span>
                                  </div>
                                </div>
                                <div className="grid grid-cols-2  mt-4">
                                  <div className="justify-center d-flex flex-column align-items-center">
                                    <img src={evening} className="w-1/4" />
                                    <span className="text-sh font-sans">
                                      6 PM - 12 AM
                                    </span>
                                  </div>
                                  <div className="justify-center d-flex flex-column align-items-center">
                                    <img src={night} className="w-1/4" />
                                    <span className="text-sh font-sans">
                                      12 AM - 5 AM
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div>
                                <span className="text-sm font-bold ml-2 font-sans">
                                  Arrival at{" "}
                                  {AirPortData.filter(
                                    (items) =>
                                      items.airportCode === bodyData.arrival
                                  ).map((ite) => (
                                    <> {ite.cityName}</>
                                  ))}
                                </span>
                                <div className="grid grid-cols-2   ">
                                  <div className=" justify-center d-flex flex-column align-items-center ">
                                    <img src={morning} className="w-1/4 " />
                                    <span className="text-sh font-sans">
                                      5 AM - 12 PM
                                    </span>
                                  </div>
                                  <div className="justify-center d-flex flex-column align-items-center ">
                                    <img src={noon} className="w-1/4 " />
                                    <span className="text-sh font-sans">
                                      12 PM - 6 PM
                                    </span>
                                  </div>
                                </div>
                                <div className="grid grid-cols-2 mt-4">
                                  <div className="justify-center d-flex flex-column align-items-center">
                                    <img src={evening} className="w-1/4" />
                                    <span className="text-sh font-sans">
                                      6 PM - 12 PM
                                    </span>
                                  </div>
                                  <div className="justify-center d-flex flex-column align-items-center">
                                    <img src={night} className="w-1/4" />
                                    <span className="text-sh font-sans ">
                                      12 AM - 5 AM
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </Disclosure.Panel>
                          </>
                        )}
                      </Disclosure>
                    </div>
                  </div>

                  <div className="px-2 py-2 grid grid-cols-1 up rounded-2xl ">
                    <div className="grid grid-cols-1 text-left">
                      <Disclosure defaultOpen="true">
                        {({ open }) => (
                          <>
                            <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                              <span className="text-lg font-bold font-sans">
                                Airline
                              </span>
                              <ChevronUpIcon
                                className={`${
                                  open ? "transform rotate-180" : ""
                                } w-5 h-5 text-gray-900`}
                              />
                            </Disclosure.Button>

                            <Disclosure.Panel className="px-2 pt-1  mt-1  rounded-2xl  pb-2 text-sm text-gray-900">
                              {result.airline.map((item, i) => {
                                const filterflightResult =
                                  result.flightResult.filter(
                                    (item) =>
                                      item.valCarrier == result.airline[i].code
                                  );
                                const minAirlineValue = Math.min(
                                  ...filterflightResult.map(
                                    (item) => item.fare.grandTotal
                                  )
                                );

                                return (
                                  <div
                                    className="d-flex align-items-center mb-2"
                                    key={i}
                                  >
                                    <div className="d-flex align-items-center ">
                                      <input
                                        type="checkbox"
                                        id="name"
                                        className="mr-2 height-mint"
                                        style={{
                                          height: "15px",
                                          width: "15px",
                                        }}
                                        name="name"
                                      />
                                    </div>
                                    <div className="d-flex align-items-center justify-content-between w-full">
                                      <div>
                                        <span className="text-xs font-bold font-sans">
                                          {item.name} &nbsp;(
                                          {filterflightResult.length})
                                        </span>
                                      </div>
                                      <div>
                                        <span className="text-xs font-bold font-sans">
                                          {" "}
                                          <i class="fas fa-rupee-sign fa-sm "></i>
                                          &nbsp; {minAirlineValue}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                );
                              })}
                            </Disclosure.Panel>
                          </>
                        )}
                      </Disclosure>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-span-4 ">
                {/* 1st */}
                {/* <div className="grid grid-cols-2 mb-4 my-3">
                  <div>
                    {result.flightResult
                      .filter((items) => (items.outBound ? items.outBound : ""))
                      .map((items, i) => (
                        <div value={items.resultID}>
                          <div className="grid grid-cols-11 px-3 py-3  my-3 pb-2 bg-slate-100 up rounded-2xl mr-3">
                            <div className="col-span-3">
                              <div className="">
                                <div className="">
                                  <img
                                    src={`https://www.travomint.com/resources/images/airline-logo/${items.outBound[0].airline}.png`}
                                    height="50px"
                                    width={50}
                                  />
                                </div>
                                <div className="pl-2">
                                  <div className="mt-2">
                                    <span className="text-xs font-bold font-sans flex flex-row">
                                      {items.outBound[0].airline}-
                                      {items.outBound[0].flightNo}
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <button
                                onClick={() => handleShow(items.resultID)}
                                className="down  border-2 font-bold text-sm font-sans pt-1 pb-1 px-3  mt-3 rounded-lg"
                                style={{
                                  border: "1px solid ",
                                  borderColor: theme.palette.primary.light,
                                }}
                              >
                                <i class="far fa-hand-point-right"></i> Flight
                                Details
                              </button>
                            </div>
                            <div className="col-span-5 ">
                              <div className="grid grid-cols-1 text-center">
                                <span className="text-xs  top-3 relative font-sans">
                                  <ConvertMinsToTime
                                    data={
                                      items.outBound[0].eft +
                                      items.outBound[0].layOverTime
                                    }
                                  />
                                </span>
                                <br />
                              </div>
                              <div className="grid grid-cols-4">
                                <div className="text-center py-3 flex flex-column">
                                  <span className="text-xs text-black font-sans font-bold">
                                    {items.outBound[0].depDate
                                      .split("T")[1]
                                      .substring(0, 5)}{" "}
                                  </span>
                                  <span className="text-xs text-black font-sans font-bold">
                                    {items.outBound[0].fromAirport}
                                  </span>
                                </div>
                                <div className="col-span-2">
                                  <div className="grid grid-cols-9 mt-3 ">
                                    <div className="col-span-3 pl-2">
                                      <hr className="bg-black opacity-100" />
                                    </div>
                                    <div className="col-span-3 text-center relative top-1 ">
                                      <i class="fas fa-plane f text-blue-500"></i>{" "}
                                      <br />
                                    </div>
                                    <div className="col-span-3 pr-2">
                                      <hr className="bg-black opacity-100" />
                                    </div>
                                  </div>
                                </div>
                                <div className="text-center py-3 flex flex-column">
                                  <span className="text-xs text-black font-sans font-bold">
                                    {items.outBound[0].reachDate
                                      .split("T")[1]
                                      .substring(0, 5)}{" "}
                                  </span>
                                  <span className="text-xs text-black font-sans font-bold">
                                    {items.outBound.length == 2 ? (
                                      <span>{items.outBound[1].toAirport}</span>
                                    ) : (
                                      <span>{items.outBound[0].toAirport}</span>
                                    )}
                                  </span>
                                </div>
                              </div>
                              <div className="grid grid-cols-1 text-center">
                                <span className="text-xs mt-1 font-sans relative">
                                  {items.outBound.length === 1 && "Non-Stop"}
                                </span>
                              </div>
                            </div>
                            <div className="col-span-3 text-right">
                              <div className="pt-3 text-center">
                                <div className="px-2 pt-0">
                                  <span className=" text-xs font-sans font-bold">
                                    From
                                  </span>
                                </div>
                                <div className="pl-4 col-span-5">
                                  <span className="text-sm font-sans font-bold">
                                    <i class="fas fa-rupee-sign fa-sm"></i>{" "}
                                    {items.fare.grandTotal.toFixed(0, 2)}
                                  </span>
                                  <br />
                                </div>
                              </div>
                              <div style={{ marginTop: "20px" }}>
                                <Button
                                  variant="contained"
                                  onClick={() => setInBound(items)}
                                >
                                  Select Now
                                </Button>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))}
                  </div>

                  <div>
                    {result.flightResult
                      .filter((items) => (items.inBound ? items.inBound : ""))
                      .map((items, i) => (
                        <>
                          <div className="grid grid-cols-11 px-3 py-3  my-3 pb-2 bg-slate-100 up rounded-2xl ml-2">
                            <div className="col-span-3">
                              <div className="">
                                <div className="pl-2">
                                  <img
                                    src={`https://www.travomint.com/resources/images/airline-logo/${items.inBound[0].airline}.png`}
                                    height="50px"
                                    width={50}
                                  />
                                </div>
                                <div className="pl-2">
                                  <div className="mt-2">
                                    <span className="text-xs font-bold font-sans flex flex-row">
                                      {items.inBound[0].airline}-
                                      {items.inBound[0].flightNo}
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <button
                                style={{
                                  border: "1px solid ",
                                  borderColor: theme.palette.primary.light,
                                }}
                                onClick={() => handleShow(items.resultID)}
                                className="down border-2 font-bold text-sm font-sans pt-1 pb-1 px-3  mt-3 rounded-lg"
                              >
                                <i class="far fa-hand-point-right"></i> Flight
                                Details
                              </button>
                            </div>
                            <div className="col-span-5 ">
                              <div className="grid grid-cols-1 text-center">
                                <span className="text-xs  top-3 relative font-sans">
                                  <ConvertMinsToTime
                                    data={
                                      items.inBound[0].eft +
                                      items.inBound[0].layOverTime
                                    }
                                  />
                                </span>
                                <br />
                              </div>
                              <div className="grid grid-cols-4">
                                <div className="text-center py-3 flex flex-column">
                                  <span className="text-xs text-black font-sans font-bold">
                                    {items.inBound[0].depDate
                                      .split("T")[1]
                                      .substring(0, 5)}
                                  </span>
                                  <span className="text-xs text-black font-sans font-bold">
                                    {items.inBound[0].fromAirport}
                                  </span>
                                </div>
                                <div className="col-span-2">
                                  <div className="grid grid-cols-9 mt-3 ">
                                    <div className="col-span-3 pl-2">
                                      <hr className="bg-black opacity-100" />
                                    </div>
                                    <div className="col-span-3 text-center relative top-1 ">
                                      <i class="fas fa-plane f text-blue-500"></i>{" "}
                                      <br />
                                    </div>
                                    <div className="col-span-3 pr-2">
                                      <hr className="bg-black opacity-100" />
                                    </div>
                                  </div>
                                </div>
                                <div className="text-center py-3 flex flex-column">
                                  <span className="text-xs text-black font-sans font-bold">
                                    {items.inBound[0].reachDate
                                      .split("T")[1]
                                      .substring(0, 5)}{" "}
                                  </span>
                                  <span className="text-xs text-black font-sans font-bold">
                                    {items.inBound.length == 2 ? (
                                      <span>{items.inBound[1].toAirport}</span>
                                    ) : (
                                      <span>{items.inBound[0].toAirport}</span>
                                    )}
                                  </span>
                                </div>
                              </div>
                              <div className="grid grid-cols-1 text-center">
                                <span className="text-xs mt-1 font-sans relative">
                                  Non-Stop
                                </span>
                              </div>
                            </div>
                            <div className="col-span-3 text-right">
                              <div className="pt-3 text-center">
                                <div className="px-2 pt-0">
                                  <span className=" text-xs font-sans font-bold">
                                    From
                                  </span>
                                </div>
                                <div className="pl-4 col-span-5">
                                  <span className="text-sm font-sans font-bold">
                                    <i class="fas fa-rupee-sign fa-sm"></i>{" "}
                                    {items.fare.grandTotal.toFixed(0, 2)}
                                  </span>
                                  <br />
                                </div>
                              </div>
                              <div style={{ marginTop: "20px" }}>
                                <Button
                                  onClick={() => setOutBound(items)}
                                  variant="contained"
                                >
                                  Select Now
                                </Button>
                              </div>
                            </div>
                          </div>
                        </>
                      ))}
                  </div>
                </div> */}
                {result.flightResult.map((item) => {
                  const totalineft = item.inBound
                    .map((ite) => {
                      return ite.eft;
                    })
                    .reduce((total, currentValue) => {
                      return total + currentValue;
                    }, 0);
                  const totalouteft = item.outBound
                    .map((items) => {
                      return items.eft;
                    })
                    .reduce((total, currentValue) => {
                      return total + currentValue;
                    }, 0);

                  return (
                    <Grid
                      container
                      spacing={2}
                      className="px-6 py-3 ms-3 me-3 mt-3 shadow rounded-2xl"
                    >
                      <Grid item xs={12} xl={10} md={6}>
                        <div className="d-flex align-items-center justify-content-between">
                          <div className="w-15">
                            <img
                              src={`https://www.travomint.com/resources/images/airline-logo/${item.outBound[0].airline}.png`}
                              height="50px"
                              width={50}
                            />
                            <span className="fs-6 text-gray-500 font-sans font-bold ">
                              {" "}
                              {item.outBound[0].airline}-
                              {item.outBound[0].flightNo}
                            </span>
                          </div>
                          <div className="text-center py-3">
                            <span className="text-black font-bold">
                              <div className="text-gray-600">
                                {item.outBound[0].fromAirport}
                              </div>
                              <div>
                                {convertFrom24To12Format(
                                  item.outBound[0].depDate
                                )}
                              </div>
                              <div className="text-gray-600">
                                {moment(item.outBound[0].depDate).format(
                                  "MMM DD, YYYY"
                                )}
                              </div>
                            </span>
                          </div>
                          <div className="col-span-2 text-center py-3 w-25">
                            <div className="progress">
                              <div
                                className="progress-bar progress-bar-striped progress-bar-animated"
                                role="progressbar"
                                aria-valuenow="75"
                                aria-valuemin="0"
                                aria-valuemax="100"
                                style={{ width: "100%" }}
                              >
                                <span>
                                  <FontAwesomeIcon
                                    icon={faPlane}
                                    className="text-gray-500"
                                  />
                                </span>
                              </div>
                            </div>
                            <div>
                              <div className=" text-black font-bold">
                                <ConvertMinsToTime data={totalouteft} />
                              </div>
                              <div>
                                {item.outBound.length === 1
                                  ? "Non-Stop"
                                  : item.outBound.length === 2
                                  ? "One-Stop"
                                  : item.outBound.length === 3
                                  ? "Two-Stop"
                                  : item.outBound.length === 4
                                  ? "Three - Stop"
                                  : null}
                              </div>
                            </div>
                          </div>
                          <div className="text-center py-3 w-25">
                            <span className="text-black font-bold">
                              <div className="text-gray-600">
                                {
                                  item.outBound[item.outBound.length - 1]
                                    .toAirport
                                }
                              </div>
                              <div>
                                {convertFrom24To12Format(
                                  item.outBound[item.outBound.length - 1]
                                    .reachDate
                                )}
                              </div>
                              <div className="text-gray-600">
                                {moment(
                                  item.outBound[item.outBound.length - 1]
                                    .reachDate
                                ).format("MMM DD, YYYY")}
                              </div>
                            </span>
                          </div>
                        </div>
                        <Divider style={{ marginRight: "20px" }} />

                        <div className="d-flex align-items-center justify-content-between">
                          <div>
                            <img
                              src={`https://www.travomint.com/resources/images/airline-logo/${item.inBound[0].airline}.png`}
                              height="50px"
                              width={50}
                            />
                            <span className="fs-6 text-gray-500 font-sans font-bold ">
                              {" "}
                              {item.inBound[0].airline}-
                              {item.inBound[0].flightNo}
                            </span>
                          </div>

                          <div className="text-center py-3 ">
                            <span className="text-black font-bold">
                              <div className="text-gray-600">
                                {item.inBound[0].fromAirport}
                              </div>
                              <div>
                                {convertFrom24To12Format(
                                  item.inBound[0].depDate
                                )}
                              </div>
                              <div className="text-gray-600">
                                {moment(item.inBound[0].depDate).format(
                                  "MMM DD, YYYY"
                                )}
                              </div>
                            </span>
                          </div>
                          <div className="col-span-2 text-center py-3 w-25">
                            <div className="progress">
                              <div
                                className="progress-bar progress-bar-striped progress-bar-animated"
                                role="progressbar"
                                aria-valuenow="75"
                                aria-valuemin="0"
                                aria-valuemax="100"
                                style={{ width: "100%" }}
                              >
                                <span>
                                  <FontAwesomeIcon
                                    icon={faPlane}
                                    className="text-gray-500"
                                  />
                                </span>
                              </div>
                            </div>
                            <div>
                              <div className=" text-black font-bold">
                                <ConvertMinsToTime data={totalineft} />
                              </div>
                              <div>
                                {item.inBound.length === 1
                                  ? "Non-Stop"
                                  : item.inBound.length === 2
                                  ? "One-Stop"
                                  : item.inBound.length === 3
                                  ? "Two-Stop"
                                  : item.inBound.length === 4
                                  ? "Three - Stop"
                                  : null}
                              </div>
                            </div>
                          </div>
                          <div className="text-center py-3 w-25">
                            <span className="text-black font-bold">
                              <div className="text-gray-600">
                                {
                                  item.inBound[item.inBound.length - 1]
                                    .toAirport
                                }
                              </div>
                              <div>
                                {convertFrom24To12Format(
                                  item.inBound[item.inBound.length - 1]
                                    .reachDate
                                )}
                              </div>
                              <div className="text-gray-600">
                                {moment(
                                  item.inBound[item.inBound.length - 1]
                                    .reachDate
                                ).format("MMM DD, YYYY")}
                              </div>
                            </span>
                          </div>
                        </div>
                      </Grid>
                      <Grid item xs={12} xl={2} md={6} className="border-start">
                        <div className="d-flex flex-column align-items-center ">
                          <button
                            onClick={() => handleShow(item)}
                            className="down border-gray-500 border-2 font-bold pt-1 pb-1 pl-3 text-sm pr-3 text-gray-600 mt-4 rounded-lg"
                          >
                            <i className="far fa-hand-point-right"></i> Flight
                            Details
                          </button>
                          <span className=" font-bold  font-sans text-gray-600 mt-3">
                            From
                          </span>
                          <div className="text-black font-bold ">
                            {item.fare.grandTotal.toFixed(1)}
                          </div>
                          <Link
                            to={"/Flight/travellersinfo"}
                            state={{ data: item }}
                            style={{ textDecoration: "none" }}
                          >
                            <Button variant="contained" className="mt-4">
                              Book Now
                            </Button>
                          </Link>
                        </div>
                      </Grid>
                    </Grid>
                  );
                })}
              </div>
            </div>
          </div>
          {/* <TwoFooter first={inBound} second={outBound} /> */}
        </Box>
      )}
    </>
  );
};

export default SearchEngine;
