import React from "react";
import PropTypes from "prop-types";
import { useEffect, useRef, useState } from "react";
import { format } from "date-fns";
import { sentenceCase } from "change-case";
import { Icon } from "@iconify/react";
import { Link as RouterLink } from "react-router-dom";
import shareFill from "@iconify/icons-eva/share-fill";
import printerFill from "@iconify/icons-eva/printer-fill";
import downloadFill from "@iconify/icons-eva/download-fill";
import trash2Outline from "@iconify/icons-eva/trash-2-outline";
import moreVerticalFill from "@iconify/icons-eva/more-vertical-fill";
import arrowIosForwardFill from "@iconify/icons-eva/arrow-ios-forward-fill";
// material
import { useTheme } from "@material-ui/core/styles";
import {
  Box,
  Card,
  Menu,
  Stack,
  Table,
  Avatar,
  Button,
  Divider,
  MenuItem,
  TableRow,
  TableBody,
  TableCell,
  TableHead,
  CardHeader,
  Typography,
  TableContainer,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Dialog,
  Slide,
} from "@material-ui/core";
// utils
// import mockData from "../../../utils/mock-data";
//
import Label from "../../components/Label";
import Scrollbar from "../../components/Scrollbar";
import { MIconButton } from "../../components/@material-extend";
import { hostname } from "src/HostName";
import useAuth from "src/hooks/useAuth";
import arrowIosForwardOutline from "@iconify/icons-eva/arrow-ios-forward-outline";
import { moment } from "moment";

// ----------------------------------------------------------------------

// const MOCK_BOOKINGS = [...Array(5)].map((_, index) => ({
//   id: window.userid,
//   CreatedAt: window.createdAt,
//   UpdatedAt: window.updatedAt,
//   Type: window.type,
//   amount: window.amount,
// }));

// ----------------------------------------------------------------------

MoreMenuButton.propTypes = {
  onDelete: PropTypes.func,
  onDownload: PropTypes.func,
  onPrint: PropTypes.func,
  onShare: PropTypes.func,
};
UserBookings.propTypes = {
  walletdata: PropTypes.string,
};

function MoreMenuButton({ onDownload, onPrint, onShare, onDelete }) {
  const menuRef = useRef(null);
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <>
        <MIconButton ref={menuRef} size="large" onClick={handleOpen}>
          <Icon icon={moreVerticalFill} width={20} height={20} />
        </MIconButton>
      </>

      <Menu
        open={open}
        anchorEl={menuRef.current}
        onClose={handleClose}
        PaperProps={{
          sx: { width: 200, maxWidth: "100%" },
        }}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
      >
        <MenuItem onClick={onDownload}>
          <Icon icon={downloadFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Full Details
          </Typography>
        </MenuItem>
        <MenuItem onClick={onPrint}>
          <Icon icon={printerFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Print
          </Typography>
        </MenuItem>
        <MenuItem onClick={onShare}>
          <Icon icon={shareFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Share
          </Typography>
        </MenuItem>
        <Divider />
        <MenuItem onClick={onDelete} sx={{ color: "error.main" }}>
          <Icon icon={trash2Outline} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Delete
          </Typography>
        </MenuItem>
      </Menu>
    </>
  );
}
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const padding = 5;
export default function UserBookings({ walletdata }) {
  const theme = useTheme();
  const isLight = theme.palette.mode === "light";
  // const { user } = useAuth()
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  const [bookingHistory, setBookingHistory] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [searchId, setSearchId] = useState(0);
  const handleClickDownload = (id) => {
    setOpen(true);
    setSearchId(id);
  };
  const handleClickPrint = () => {};
  const handleClickShare = () => {};
  const handleClickDelete = () => {};

  const BookingHistory = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      userid: user.userid,
    });
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    const GettingHistory = await fetch(
      `${hostname}get/booking/history`,
      requestOptions
    );
    const allHistoryData = await GettingHistory.json();

    setBookingHistory(allHistoryData);
  };

  useEffect(() => BookingHistory(), []);

  const handleClose = () => {
    setOpen(false);
  };
  const ConvertMinsToTime = ({ data }) => {
    let hours = Math.floor(data / 60);
    let minutes = data % 60;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    return `${hours}h:${minutes}m`;
  };

  return (
    <>
      <Card>
        <CardHeader title="Your Booking" sx={{ mb: 3 }} />
        <Scrollbar>
          <TableContainer sx={{ minWidth: 720 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell sx={{ minWidth: 240 }}>Booking ID</TableCell>
                  <TableCell sx={{ minWidth: 160 }}>PNR</TableCell>
                  <TableCell sx={{ minWidth: 120 }}>Booking Status</TableCell>
                  <TableCell sx={{ minWidth: 200 }}>Amount </TableCell>
                  {/* <TableCell sx={{ minWidth: 120 }}>Room Type</TableCell> */}
                  <TableCell />
                </TableRow>
              </TableHead>
              {bookingHistory.status === true ? (
                <TableBody>
                  {bookingHistory.bookingDetails.map((row) => (
                    <TableRow key={row.id}>
                      <TableCell>
                        <Label>{row.bookingID}</Label>
                      </TableCell>
                      <TableCell>
                        <Label>{row.pNR}</Label>
                      </TableCell>
                      <TableCell>
                        <Label
                          sx={{
                            backgroundColor:
                              row.bookingStatus === "Confirm"
                                ? "#90EE90"
                                : "#ffcccb ",
                            color:
                              row.bookingStatus === "Confirm" ? "green" : "red",
                          }}
                        >
                          {row.bookingStatus}
                        </Label>
                      </TableCell>
                      <TableCell>
                        <Label>Rs.{row.grandTotal}</Label>
                      </TableCell>
                      <TableCell>
                        <MoreMenuButton
                          onDownload={() => handleClickDownload(row.searchID)}
                          onPrint={handleClickPrint}
                          onShare={handleClickShare}
                          onDelete={handleClickDelete}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              ) : (
                <Typography as="h3" sx={{ padding: 5 }}>
                  {bookingHistory.message}
                </Typography>
              )}
            </Table>
          </TableContainer>
        </Scrollbar>

        <Divider />
        {searchId === 0 ? null : (
          <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle>{"Flight Details"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-slide-description">
                {bookingHistory.bookingDetails
                  .filter((item) => searchId === item.searchID)
                  .map((item) =>
                    item.flightInfo.map((item) => (
                      <Stack>
                        <Stack
                          direction={"row"}
                          justifyContent={"space-between"}
                          alignItems={"center"}
                        >
                          <Stack alignItems={"center"}>
                            <img
                              src={`https://www.travomint.com/resources/images/airline-logo/${item.airlineCode}.png`}
                              className="w-12 down rounded-2xl float-right"
                            />
                            <Typography variant="subtitle2">
                              {item.airlineName} - {item.flightNo}
                            </Typography>
                          </Stack>
                          <Stack sx={{ padding: padding }}>
                            <Typography variant="h6">
                              {item.fromAirport}
                            </Typography>
                            <Typography>
                              {item.depDate.split("T")[1].substring(0, 5)}
                            </Typography>
                          </Stack>
                          <Stack>
                            <Icon
                              icon={arrowIosForwardOutline}
                              width={20}
                              height={20}
                            />
                            <ConvertMinsToTime data={item.estimateTime} />
                          </Stack>
                          <Stack sx={{ padding: padding }}>
                            <Typography variant="h6">
                              {item.toAirport}
                            </Typography>
                            <Typography>
                              {item.reachDate.split("T")[1].substring(0, 5)}
                            </Typography>
                          </Stack>
                        </Stack>
                        <Stack
                          direction="row"
                          justifyContent={"space-between"}
                          sx={{ mx: 2 }}
                        >
                          <Typography>From date</Typography>
                          <Typography>To date</Typography>
                        </Stack>
                        <Stack
                          direction="row"
                          sx={{ m: 1 }}
                          justifyContent={"space-between"}
                        >
                          <Typography>
                            {/* {moment(item.depDate.split("T")[0]).format(
                              "DD/MM/YYYY"
                            )} */}
                            {format(
                              new Date(item.depDate.split("T")[0]),
                              "dd/MM/yyyy"
                            )}
                          </Typography>
                          <Typography>
                            {format(
                              new Date(item.reachDate.split("T")[0]),
                              "dd/MM/yyyy"
                            )}
                          </Typography>
                        </Stack>
                        <Stack
                          direction="row"
                          justifyContent={"space-between"}
                          sx={{ mx: 1 }}
                        >
                          <Typography>From Terminal</Typography>
                          <Typography>{item.fromTerminal} Terminal</Typography>
                        </Stack>
                        <Stack
                          direction="row"
                          justifyContent={"space-between"}
                          sx={{ mx: 1 }}
                        >
                          <Typography>To Terminal</Typography>
                          <Typography>{item.toTerminal} Terminal</Typography>
                        </Stack>
                      </Stack>
                    ))
                  )}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>close</Button>
            </DialogActions>
          </Dialog>
        )}

        <Box sx={{ p: 2, textAlign: "right" }}>
          <Button
            to="#"
            size="small"
            color="inherit"
            component={RouterLink}
            endIcon={<Icon icon={arrowIosForwardFill} />}
          >
            View All
          </Button>
        </Box>
      </Card>
    </>
  );
}
