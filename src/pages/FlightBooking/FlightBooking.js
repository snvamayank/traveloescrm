import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import {
  BookingFlightList,
  ShowTicket,
  ticketVerfiyurl,
} from "src/Api/Flight/FlightApiUrl";
import {
  useGetFlightBoooking,
  useTicketVerfiy,
} from "src/Api/Flight/useFlightCustomApi";
import ErrorPages from "src/components/Error_Pages/ErrorPages";
import LoadingScreen from "src/components/LoadingScreen";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import axios from "axios";
const list = [
  { id: 2, name: "OrderNo" },
  { id: 3, name: "PnrCode" },
  { id: 4, name: "BookingDate" },
  { id: 5, name: "JourneyDate" },
  { id: 6, name: "TripType" },
  { id: 7, name: "From" },
  { id: 8, name: "To" },
  { id: 9, name: "Cus /Supp Amount" },
  { id: 10, name: "Source" },
  { id: 11, name: "Stops" },
  { id: 12, name: "Airline" },
  { id: 13, name: "Currency" },
  { id: 14, name: "Action" },
];
const FlightBooking = () => {
  const flightData = useGetFlightBoooking(BookingFlightList);
  const result = useTicketVerfiy(ticketVerfiyurl);
  const [open, setopen] = useState(false);
  const [data, setData] = useState();
  const [page, setPage] = useState(1);
  const [selectpage, setselectPage] = useState(15);

  const showDetail = (item) => {
    setopen(true);
    const data = {
      pnrCode: item.pnrCode,
      supp: 52,
    };
    axios
      .post(`${ShowTicket}`, data)
      .then((res) => {
        console.log("res", res.data);
        setData(res.data);
      })
      .catch((err) => console.log("error", err.message));
    console.log("item", item);
  };
  const handleClose = () => {
    setopen(false);
  };
  if (flightData.loading) {
    return <LoadingScreen />;
  }
  if (flightData.error) {
    return <ErrorPages title={flightData.error} />;
  }
  return (
    <div>
      <div className="text-center">
        <h2>Flight Booking History</h2>
      </div>
      <TableContainer className="tableairp-responsiveBooking">
        <Table className="table-airp">
          <TableHead>
            <TableRow>
              {list.map((item, index) => (
                <TableCell key={index}>{item.name}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {flightData.data.data.map((item, i) => {
              return (
                <TableRow key={i}>
                  <TableCell>{item.orderNo}</TableCell>
                  <TableCell>{item.pnrCode}</TableCell>
                  <TableCell>{item.bookingDate}</TableCell>
                  <TableCell>{item.journeyDate}</TableCell>
                  <TableCell>{item.tripType}</TableCell>
                  <TableCell>{item.from}</TableCell>
                  <TableCell>{item.to}</TableCell>
                  <TableCell>
                    {item.cusAmount} , {item.suppAmount}
                  </TableCell>
                  <TableCell>{item.source}</TableCell>
                  <TableCell>{item.stops}</TableCell>
                  <TableCell>{item.airline}</TableCell>
                  <TableCell>{item.currency}</TableCell>
                  <TableCell
                    onClick={
                      item.ticketIssue === false
                        ? () => {
                            result.verfiy(item);
                          }
                        : null
                    }
                  >
                    {item.ticketIssue === false ? (
                      "Issue Ticket"
                    ) : (
                      <>
                        <div
                          onClick={() => showDetail(item)}
                          style={{ color: "green" }}
                        >
                          Show TicketInfo
                        </div>
                      </>
                    )}
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
        {flightData.data.data.length > 12 ? (
          <div className="d-flex flex-row-reverse m-3">
            <ChevronRightIcon
              onClick={() =>
                page < flightData.data.data.length / selectpage
                  ? setPage((pre) => pre + 1)
                  : null
              }
              className="cursor-pointer"
            />
            {page} -&nbsp;
            {Math.ceil(flightData.data.data.length / selectpage)} Page
            <KeyboardArrowLeftIcon
              onClick={() => (page > 1 ? setPage((pre) => pre - 1) : null)}
              className="cursor-pointer"
            />
            <select
              name="selectpage"
              id="selectpage"
              value={selectpage}
              onChange={(e) => setselectPage(e.target.value)}
              className="border border-2 rounded ml-2"
            >
              <option value="15">15</option>
              <option value="30">30</option>
              <option value="45">45</option>
              <option value="60">60</option>
            </select>
            Row Per Page
          </div>
        ) : null}
      </TableContainer>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle className="d-flex justify-content-between align-items-center pb-4">
          <h3>Booking TicketInfo</h3>
          <div>
            <CloseIcon onClick={() => handleClose()} />
          </div>
        </DialogTitle>
        <DialogContent>
          <pre>{data && JSON.stringify(data, null, 2)}</pre>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default FlightBooking;
