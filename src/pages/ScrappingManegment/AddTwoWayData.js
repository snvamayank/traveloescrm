import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components

import axios from "axios";
import {
  Card,
  CardHeader,
  Stack,
  Select,
  MenuItem,
  Button,
  Input,
  Grid,
  TextField,
  CardContent,
  Paper,
  CardActions,
  Divider,
  Box,
  Typography,
  FormControl,
  InputLabel,
} from "@material-ui/core";
import Page from "src/components/Page";
import { scrapping_Host } from "src/HostName";
import { styled } from "@mui/material/styles";
import { Icon } from "@iconify/react";
import logOutOutline from "@iconify/icons-eva/log-out-outline";
import plusCircleOutline from "@iconify/icons-eva/plus-circle-outline";
import ReturnFlight from "src/components/_dashboard/ScrappingManagment/ReturnFlight";
import DepartureFlight from "src/components/_dashboard/ScrappingManagment/DepartureFlight";
import moment from "moment";
import AirPortData from "src/Api/SampleData";
import useAuth from "src/hooks/useAuth";
import { travelClass, TripSelect } from "src/utils/stop";
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));
export default function AddTwoWayData() {
  // const { user } = useAuth();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  const [show, setShow] = useState(false);
  const [validate, setValidate] = useState(false);
  const [filteredDataSource, setFilteredDataSource] = useState(AirPortData);
  const [masterDataSource, setMasterDataSource] = useState(AirPortData);
  const [originAirport, setOriginAirport] = useState("");
  const [destinationAirport, setDestinationAirport] = useState("");
  const [departureAirportCode, setDepartureAirportCode] = useState("");
  const [airportCodeAndName, setAirportCodeAndName] = useState({
    name: "",
    code: "",
  });
  const [arrivalAirportCode, setArrivalAirportCode] = useState("");
  const [returnAirportCode, setReturnAirportCode] = useState("");
  const [arrivalReturnAirportCode, setArrivalReturnAirportCode] = useState("");
  const [airportName, setAiportName] = useState("");
  const [showAirport, setShowAirport] = useState(0);
  const [search, setSearch] = useState("");

  const [mainDetails, setMainDetails] = useState({
    Trip: 0,
    TripType: 2,
    Meta: "",
    Origin: "",
    Destination: "",
    DepartureDate: "",
    ReturnDate: "",
    Adults: 1,
    Childs: 0,
    Infants: 0,
    TravelClass: "None",
    Currency: "",
    DepartureFlight: [],
    upload_id: 999,
  });
  console.log("mainDetails", mainDetails);
  const [departDetails, setDepartDetails] = useState([
    {
      DepartureFlightDate: "",
      DepartureFlightTime: "",
      ArrivalFlightDate: "",
      ArrivalFlightTime: "",
      DepartureAirportCode: "",
      DepartureAirportName: "",
      ArrivalAirportCode: "",
      ArrivalAirportName: "",
      AirlineCode: "",
      AirlineName: "",
      FlightNo: "",
      DepartureTerminal: "",
      ArrivalTerminal: "",
      Baggage: "",
      CabinBaggage: "",
      CheckIn: "",
      Duration: "",
    },
  ]);
  const [departFare, setDepartFare] = useState({
    TotalFare: "",
    BaseFare: "",
    Surcharge: "",
  });
  const [returnDetails, setReturnDetails] = useState([
    {
      ReturnFlightDate: "",
      ReturnFlightTime: "",
      ArrivalFlightDate: "",
      ArrivalFlightTime: "",
      ReturnDepAirportCode: "",
      DepartureAirportName: "",
      ArrivalAirportName: "",
      ReturnArrivalAirportCode: "",
      AirlineCode: "",
      AirlineName: "",
      FlightNo: "",
      ReturnTerminal: "",
      ArrivalTerminal: "",
      Baggage: "",
      CabinBaggage: "",
      CheckIn: "",
      Duration: "",
    },
  ]);
  console.log("mainDetails", mainDetails);
  const [returnFare, setReturnFare] = useState({
    TotalFare: 0,
    BaseFare: 0,
    Surcharge: 0,
  });

  const ChangeFormValues = (e, name) => {
    if (name === "Origin" || name === "Destination") {
      searchFilterFunction(e.target.value, name);
      setMainDetails({ ...mainDetails, [name]: e.target.value });
    } else if (name == "DepartureDate" || name === "ReturnDate") {
      setMainDetails({
        ...mainDetails,
        [name]: moment(e.target.value).format("MM/DD/YYYY"),
      });
    } else {
      setMainDetails({ ...mainDetails, [name]: e.target.value });
    }
  };

  const ClickAirport = (e, index) => {
    const list = [...departDetails];
    list[index][e.name] = e.value;
    setDepartDetails(list);
  };
  const ClickReturnAirport = (e, index) => {
    const list = [...returnDetails];
    list[index][e.name] = e.value;
    setReturnDetails(list);
  };

  const ChangeDepartDetails = (e, index) => {
    const { name, value } = e.target;
    const list = [...departDetails];
    if (name === "DepartureAirportCode" || name === "ArrivalAirportCode") {
      searchFilterFunction(e.target.value, name);
      list[index][name] = value;
      setDepartDetails(list);
    } else if (name === "DepartureFlightDate" || name === "ArrivalFlightDate") {
      const dtFormat = moment(value).format("MM/DD/YYYY");
      list[index][name] = dtFormat;
      setDepartDetails(list);
    } else {
      list[index][name] = value;
      setDepartDetails(list);
    }
  };

  const ChangeReturnDetails = (e, index) => {
    const { name, value } = e.target;
    const list = [...returnDetails];
    if (
      name === "ReturnDepAirportCode" ||
      name === "ReturnArrivalAirportCode"
    ) {
      searchFilterFunction(e.target.value, name);
      list[index][name] = value;
      setReturnDetails(list);
    } else if (
      name === "ReturnFlightDate" ||
      (name && e.target.id === "ArrivalFlightDate")
    ) {
      const dtFormat = moment(value).format("MM/DD/YYYY");
      list[index][name] = dtFormat;
      setReturnDetails(list);
    } else {
      list[index][name] = value;
      setReturnDetails(list);
    }
  };

  const ChangeDepartFare = (e, name) => {
    setDepartFare({ ...departFare, [name]: Number(e.target.value) });
  };
  const ChangeReturnFare = (e, name) => {
    setReturnFare({ ...returnFare, [name]: Number(e.target.value) });
  };

  const AddMoreDepartFlightDetails = () => {
    setDepartDetails([
      ...departDetails,
      {
        DepartureFlightDate: "",
        DepartureFlightTime: "",
        ArrivalFlightDate: "",
        ArrivalFlightTime: "",
        DepartureAirportCode: "",
        DepartureAirportName: "",
        ArrivalAirportCode: "",
        ArrivalAirportName: "",
        AirlineCode: "",
        AirlineName: "",
        FlightNo: "",
        DepartureTerminal: "",
        ArrivalTerminal: "",
        Baggage: "",
        CabinBaggage: "",
        CheckIn: "",
        Duration: "",
      },
    ]);
  };
  const AddMoreReturnFlightDetails = () => {
    setReturnDetails([
      ...returnDetails,
      {
        ReturnFlightDate: "",
        ReturnFlightTime: "",
        ArrivalFlightDate: "",
        ArrivalFlightTime: "",
        ReturnDepAirportCode: "",
        DepartureAirportName: "",
        ArrivalAirportName: "",
        ReturnArrivalAirportCode: "",
        AirlineCode: "",
        AirlineName: "",
        FlightNo: "",
        ReturnTerminal: "",
        ArrivalTerminal: "",
        Baggage: "",
        CabinBaggage: "",
        CheckIn: "",
        Duration: "",
      },
    ]);
  };

  const mergingObj = {
    ...mainDetails,
    DepartureFlight: departDetails,
    DepartureFare: departFare,
    ReturnFlight: returnDetails,
    ReturnFare: returnFare,
  };

  const AddRecord = () => {
    setValidate(false);
    var config = {
      method: "post",
      url: `${scrapping_Host}post/metadata`,
      headers: {
        "Content-Type": "application/json",
      },
      data: mergingObj,
    };
    axios(config)
      .then(function (response) {
        if (response.data.status === "success") {
          alert("Your data has been Succesfully Updated");
          setMainDetails({
            Trip: 2,
            TripType: 2,
            Meta: "",
            Origin: "",
            Destination: "",
            DepartureDate: "",
            ReturnDate: "",
            Adults: 1,
            Childs: 0,
            Infants: 0,
            TravelClass: "ECONOMY",
            Currency: "",
            DepartureFlight: [],
            upload_id: 999,
          });
          setDepartDetails([
            {
              DepartureFlightDate: "",
              DepartureFlightTime: "",
              ArrivalFlightDate: "",
              ArrivalFlightTime: "",
              DepartureAirportCode: "",
              DepartureAirportName: "",
              ArrivalAirportCode: "",
              ArrivalAirportName: "",
              AirlineCode: "",
              AirlineName: "",
              FlightNo: "",
              DepartureTerminal: "",
              ArrivalTerminal: "",
              Baggage: "",
              CabinBaggage: "",
              CheckIn: "",
              Duration: "",
            },
          ]);
          setDepartFare({
            TotalFare: "",
            BaseFare: "",
            Surcharge: "",
          });
          setReturnDetails([
            {
              ReturnFlightDate: "",
              ReturnFlightTime: "",
              ArrivalFlightDate: "",
              ArrivalFlightTime: "",
              ReturnDepAirportCode: "",
              DepartureAirportName: "",
              ArrivalAirportName: "",
              ReturnArrivalAirportCode: "",
              AirlineCode: "",
              AirlineName: "",
              FlightNo: "",
              ReturnTerminal: "",
              ArrivalTerminal: "",
              Baggage: "",
              CabinBaggage: "",
              CheckIn: "",
              Duration: "",
            },
          ]);
          setReturnFare({
            TotalFare: 0,
            BaseFare: 0,
            Surcharge: 0,
          });
        } else {
          alert("There is Something wrong");
        }
      })
      .catch(function (error) {
        console.log("error");
      });
  };

  const searchFilterFunction = (text, name) => {
    if (text) {
      const newData = masterDataSource.filter(function (item) {
        const itemData = item.airportCode ? item.airportCode : "".toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
      if (name === "Origin") {
        setOriginAirport(text);
      } else if (name === "Destination") {
        setDestinationAirport(text);
      } else if (name === "DepartureAirportCode") {
        setDepartureAirportCode(text);
      } else if (name === "ArrivalAirportCode") {
        setArrivalAirportCode(text);
      } else if (name === "ReturnDepAirportCode") {
        setReturnAirportCode(text);
      } else if (name === "ReturnArrivalAirportCode") {
        setArrivalReturnAirportCode(text);
      } else {
      }
    } else {
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };
  return (
    <Card>
      <Box sx={{ flexGrow: 1 }}>
        <Typography variant="h4" sx={{ marginLeft: 5, marginTop: 5 }}>
          Add two way itinerary
        </Typography>
        <Grid container columns={{ xs: 4, sm: 8, md: 12 }}>
          <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            <Item>
              <Stack xs={12} sm={12} md={3}>
                <Select
                  value={mainDetails.Trip}
                  label="Select Trip"
                  // defaultValue={"International"}
                  placeholder="International"
                  onChange={(e) => ChangeFormValues(e, "Trip")}
                  sx={{ width: "20rem", margin: 2 }}
                >
                  {TripSelect.map((item) => (
                    <MenuItem value={item.id}>{item.name}</MenuItem>
                  ))}
                </Select>
                {validate === true && (
                  <Typography variant="subtitle2" sx={{ color: " red" }}>
                    Please Select Trip
                  </Typography>
                )}
              </Stack>
              <Stack xs={12} sm={12} md={3}>
                <FormControl>
                  <InputLabel id="Select meta" className="ml-5  mt-3">
                    Select meta
                  </InputLabel>
                  <Select
                    value={mainDetails.Meta}
                    label="Select meta"
                    onChange={(e) => ChangeFormValues(e, "Meta")}
                    sx={{ width: "20rem", margin: 2 }}
                  >
                    {" "}
                    {user.metaPermission.map((item) => (
                      <MenuItem value={item}>{item}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Stack>
              {validate === true && (
                <Typography variant="subtitle2" sx={{ color: " red" }}>
                  Please Select The Meta
                </Typography>
              )}
            </Item>
          </Grid>

          <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            <Item>
              <Stack xs={12} sm={12} md={3} className="position-relative">
                <TextField
                  variant={"outlined"}
                  label="Origin (LAX)"
                  id="Origin"
                  name="Origin"
                  onClick={() => setShowAirport(1)}
                  value={mainDetails.Origin}
                  onChange={(e) => ChangeFormValues(e, "Origin")}
                  formControlProps={{
                    fullWidth: true,
                  }}
                  sx={{ width: "20rem", margin: 2 }}
                />
                <ul
                  style={{
                    position: "absolute",
                    top: "74px",
                    maxHeight: "400px",
                    overflowY: "scroll",
                    width: "100%",
                  }}
                  className="menuflitem-5  pl-0  pr-0  z-50
                bg-white shadow rounded "
                >
                  {showAirport === 1 && (
                    <Stack
                      sx={{
                        justifyContent: "center",
                        overflow: "hidden",
                      }}
                    >
                      {filteredDataSource.map((item) => (
                        <Stack
                          sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}
                          onClick={() => {
                            setMainDetails({
                              ...mainDetails,
                              Origin: item.airportCode,
                            });
                            setOriginAirport(item.airportCode);
                          }}
                        >
                          <Typography
                            onClick={() => setShowAirport(0)}
                            className="pl-2 pt-2"
                          >
                            {item.airportName}
                          </Typography>
                          <Typography
                            onClick={() => setShowAirport(0)}
                            className="pe-1 pt-2"
                          >
                            {item.airportCode}
                          </Typography>
                        </Stack>
                      ))}
                    </Stack>
                  )}
                </ul>

                {validate === true && (
                  <>
                    {mainDetails.Origin.length > 3 ? (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        You have exceed the limits of entry
                      </Typography>
                    ) : (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter origin{" "}
                      </Typography>
                    )}
                  </>
                )}
              </Stack>

              <Stack xs={12} sm={12} md={3} className="position-relative">
                <TextField
                  sx={{ width: "20rem", margin: 2 }}
                  variant={"outlined"}
                  label="Destination (MIA) "
                  id="Destination"
                  name="Destination"
                  onClick={() => setShowAirport(2)}
                  value={mainDetails.Destination}
                  onChange={(e) => ChangeFormValues(e, "Destination")}
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
                <ul
                  style={{
                    position: "absolute",
                    top: "74px",
                    maxHeight: "400px",
                    overflowY: "scroll",
                    width: "100%",
                  }}
                  className="menuflitem-5  pl-0  pr-0  z-50
                bg-white shadow rounded "
                >
                  {showAirport === 2 && (
                    <Stack
                      sx={{
                        justifyContent: "center",
                        overflow: "hidden",
                      }}
                    >
                      {filteredDataSource.map((item) => (
                        <Stack
                          sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}
                          onClick={() => {
                            {
                              setMainDetails({
                                ...mainDetails,
                                Destination: item.airportCode,
                              });
                              setDestinationAirport(item.airportCode);
                            }
                          }}
                        >
                          <Typography
                            onClick={() => setShowAirport(0)}
                            className="pl-2 pt-2"
                          >
                            {item.airportName}
                          </Typography>
                          <Typography
                            onClick={() => setShowAirport(0)}
                            className="pe-1 pt-2"
                          >
                            {item.airportCode}
                          </Typography>
                        </Stack>
                      ))}
                    </Stack>
                  )}
                </ul>

                {validate === true && (
                  <>
                    {mainDetails.Destination.length > 3 ? (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        You have exceed the limits of entry
                      </Typography>
                    ) : (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please select destination{" "}
                      </Typography>
                    )}
                  </>
                )}
              </Stack>
            </Item>
          </Grid>
          <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            <Item>
              <Stack
                xs={12}
                sm={12}
                md={3}
                direction="column"
                sx={{ justifyContent: "space-between" }}
              >
                <Stack>
                  <TextField
                    sx={{ width: "20rem", margin: 2 }}
                    variant={"outlined"}
                    type="date"
                    id="DepartureDate "
                    name="DepartureDate"
                    // value={mainDetails.DepartureDate}
                    onChange={(e) => ChangeFormValues(e, "DepartureDate")}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />

                  {validate === true && (
                    <Typography variant="subtitle2" sx={{ color: " red" }}>
                      Please select the departure date{" "}
                    </Typography>
                  )}
                </Stack>
              </Stack>
              <Stack
                xs={12}
                sm={12}
                md={3}
                direction="column"
                sx={{ justifyContent: "space-between" }}
              >
                <Stack>
                  <TextField
                    sx={{ width: "20rem", margin: 2 }}
                    variant={"outlined"}
                    type="date"
                    id="ReturnDate"
                    name="ReturnDate"
                    // value={mainDetails.Return}
                    onChange={(e) => ChangeFormValues(e, "ReturnDate")}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                  {validate === true && (
                    <Typography variant="subtitle2" sx={{ color: " red" }}>
                      Please Select return date{" "}
                    </Typography>
                  )}
                </Stack>
              </Stack>
            </Item>
          </Grid>

          <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            <Item>
              <Stack xs={12} sm={12} md={3}>
                <Select
                  value={mainDetails.TravelClass}
                  label="Select TravelClass"
                  defaultValue={"Select class"}
                  onChange={(e) => ChangeFormValues(e, "TravelClass")}
                  sx={{ width: "20rem", margin: 2 }}
                >
                  {travelClass.map((item) => (
                    <MenuItem value={item.classname}>{item.name}</MenuItem>
                  ))}
                </Select>
                {validate === true && (
                  <Typography variant="subtitle2" sx={{ color: " red" }}>
                    Please select the class{" "}
                  </Typography>
                )}
                {/* {returnDetails.length === 2 ? null : (
                  <Grid
                    spacing={{ xs: 2, md: 3 }}
                    columns={{ xs: 4, sm: 8, md: 12 }}
                  >
                    <Item>
                      <Box sx={{ p: 2, textAlign: "left" }}>
                        <Button
                          variant="contained"
                          onClick={() => setShow(true)}
                          endIcon={<Icon icon={plusCircleOutline} />}
                        >
                          Add return Details
                        </Button>
                      </Box>
                    </Item>
                  </Grid>
                )} */}
              </Stack>
            </Item>
          </Grid>
          <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            <Item>
              <Stack xs={12} sm={12} md={3}>
                <FormControl>
                  <InputLabel id="Select meta" className="ml-5  mt-3">
                    Select Currency
                  </InputLabel>
                  <Select
                    value={mainDetails.Currency}
                    label="Select Currency"
                    onChange={(e) => ChangeFormValues(e, "Currency")}
                    sx={{ width: "20rem", margin: 2 }}
                  >
                    {user.currencyPermission.map((item) => (
                      <MenuItem value={item}>{item}</MenuItem>
                    ))}
                  </Select>
                </FormControl>

                {validate === true && (
                  <>
                    {mainDetails.Currency.length > 3 ? (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        You have exceed the limits of entry
                      </Typography>
                    ) : (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please add currency{" "}
                      </Typography>
                    )}
                  </>
                )}
              </Stack>
            </Item>
          </Grid>
          <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            <Item>
              <Stack xs={12} sm={12} md={3}>
                {returnDetails.length === 2 ? null : (
                  <Grid
                    spacing={{ xs: 2, md: 3 }}
                    columns={{ xs: 4, sm: 8, md: 12 }}
                  >
                    <Item>
                      <Box sx={{ p: 2, textAlign: "left" }}>
                        <Button
                          variant="contained"
                          onClick={() => setShow(true)}
                          endIcon={<Icon icon={plusCircleOutline} />}
                        >
                          Add return Details
                        </Button>
                      </Box>
                    </Item>
                  </Grid>
                )}
              </Stack>
            </Item>
          </Grid>
        </Grid>
        {show === true ? (
          <>
            <Divider />

            <DepartureFlight
              item={departDetails}
              ChangeDepartDetails={ChangeDepartDetails}
              AddMoreDepartFlightDetails={AddMoreDepartFlightDetails}
              departFare={departFare}
              ChangeDepartFare={ChangeDepartFare}
              validate={validate}
              filteredDataSource={filteredDataSource}
              originAirport={originAirport}
              destinationAirport={destinationAirport}
              setOriginAirport={setOriginAirport}
              setDestinationAirport={setDestinationAirport}
              departureAirportCode={departureAirportCode}
              setDepartureAirportCode={setDepartureAirportCode}
              arrivalAirportCode={arrivalAirportCode}
              setArrivalAirportCode={setArrivalAirportCode}
              setShowAirport={setShowAirport}
              showAirport={showAirport}
              setAiportName={setAiportName}
              airportName={airportName}
              airportCodeAndName={airportCodeAndName}
              setAirportCodeAndName={setAirportCodeAndName}
              ClickAirport={ClickAirport}
            />
            <Divider />
            <ReturnFlight
              returnDetails={returnDetails}
              ChangeReturnFare={ChangeReturnFare}
              returnFare={returnFare}
              ChangeReturnDetails={ChangeReturnDetails}
              AddMoreReturnFlightDetails={AddMoreReturnFlightDetails}
              validate={validate}
              filteredDataSource={filteredDataSource}
              originAirport={originAirport}
              destinationAirport={destinationAirport}
              setOriginAirport={setOriginAirport}
              setDestinationAirport={setDestinationAirport}
              departureAirportCode={departureAirportCode}
              setDepartureAirportCode={setDepartureAirportCode}
              arrivalAirportCode={arrivalAirportCode}
              setArrivalAirportCode={setArrivalAirportCode}
              setShowAirport={setShowAirport}
              showAirport={showAirport}
              setReturnAirportCode={setReturnAirportCode}
              returnAirportCode={returnAirportCode}
              setArrivalReturnAirportCode={setArrivalReturnAirportCode}
              arrivalReturnAirportCode={arrivalReturnAirportCode}
              ClickReturnAirport={ClickReturnAirport}
            />
          </>
        ) : null}
      </Box>
      <Divider />
      {mainDetails.Origin &&
      mainDetails.Destination &&
      mainDetails.DepartureDate &&
      mainDetails.Meta ? (
        <Box sx={{ p: 2, textAlign: "left" }}>
          <Button
            to="#"
            variant="contained"
            onClick={() => AddRecord(mergingObj)}
            endIcon={<Icon icon={logOutOutline} />}
          >
            Submit
          </Button>
        </Box>
      ) : (
        <Box sx={{ p: 2, textAlign: "left" }}>
          <Button
            to="#"
            variant="contained"
            onClick={() => setValidate(true)}
            endIcon={<Icon icon={logOutOutline} />}
          >
            Submit
          </Button>
        </Box>
      )}
    </Card>
  );
}
