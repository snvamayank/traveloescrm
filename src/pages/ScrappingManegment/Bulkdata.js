import { Input, TextField } from "@material-ui/core";
import React, { Component, useState } from "react";
import Label from "src/components/Label";
import * as xlsx from "xlsx";
import { Button } from "@material-ui/core";
import { scrapping_Host } from "src/HostName";
import LoadingScreen from "src/components/LoadingScreen";

const ExcelReader = () => {
  const [excelJson, setExcelJson] = useState([]);
  const [loading, setloading] = useState(false);
  const [fileList, setFileList] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const readUploadFile = (e) => {
    setloading(true);
    e.preventDefault();
    setFileList(e.target.files);
    // if (e.target.files) {
    //   const reader = new FileReader();
    //   reader.onload = (e) => {
    //     const data = e.target.result;
    //     console.log("data", data);
    //     const workbook = xlsx.read(data, { type: "array" });
    //     const sheetName = workbook.SheetNames[0];
    //     const worksheet = workbook.Sheets[sheetName];
    //     const json = xlsx.utils.sheet_to_json(worksheet);
    //     setExcelJson(json);
    //   };
    //   reader.readAsArrayBuffer(e.target.files[0]);
    // }
  };

  const fileUpload = () => {
    setIsLoading(true);

    var formdata = new FormData();

    formdata.append(`uploadfile`, fileList[0], fileList[0].name);

    var requestOptions = {
      method: "POST",
      body: formdata,
      redirect: "follow",
    };
    fetch(`${scrapping_Host}add/bulkdata`, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let resultParseData = JSON.parse(result);
        console.log("result", result);
        if (resultParseData.status == 200) {
          setIsLoading(false);
          alert("File has been uploaded");
        } else {
          setIsLoading(false);
          alert("File has not been uploaded");
        }
      })
      .catch((error) => {
        setIsLoading(false);
        alert("Server error");
        console.log("error123", error.message);
      });
  };

  return (
    <form>
      {isLoading === false ? (
        <>
          <Input
            variant="Outlined"
            type="file"
            name="upload"
            id="upload"
            onChange={readUploadFile}
            className="me-5"
          />
          <Button
            variant="contained"
            onClick={fileUpload}
            disabled={loading != false ? false : true}
          >
            Upload files
          </Button>
        </>
      ) : (
        // <LoadingScreen />
        "please wait it takes some time.."
      )}
    </form>
  );
};

export default ExcelReader;
