import React, { useEffect, useState } from "react";
import Page from "src/components/Page";
import {
  Alert,
  Box,
  Button,
  Card,
  CardHeader,
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  MenuItem,
  Paper,
  Select,
  Snackbar,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@material-ui/core";
import DeleteIcon from "@mui/icons-material/Delete";
import CloseIcon from "@mui/icons-material/Close";
import EditIcon from "@mui/icons-material/Edit";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import DatePicker from "@mui/lab/DatePicker";
import axios from "axios";
import { scrapping_Host } from "src/HostName";
import DepartureForm from "./EditData/DepartureForm.";
import ReturnForm from "./EditData/ReturnForm";
import { styled } from "@material-ui/styles";
import moment from "moment";
import { replicateCallApi } from "src/Api/CallApi/ReplicateApi";
import LoadingScreen from "src/components/LoadingScreen";
import { IconButton } from "@mui/material";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const ViewData = () => {
  const [routeData, setRouteData] = useState("");
  const [refresh, setRefresh] = useState(false);
  const [objectID, setObjectID] = useState(0);
  const [open, setOpen] = useState(false);
  const [open1, setOpen1] = useState(false);
  const [departDetails, setDepartDetails] = useState([]);
  const [returnDetails, setReturnDetails] = useState([]);
  const [horizontal, setHorizontal] = useState("center");
  const [vertical, setVertical] = useState("top");
  const [departFare, setDepartFare] = useState({});
  const [returnFare, setReturnFare] = useState({});
  const [mainDetails, setMainDetails] = useState({});
  const [validate, setValidate] = useState(false);
  const [fromDate, setFromDate] = useState(moment().format("YYYY-MM-DD"));
  const [toDate, setToDate] = useState(moment().format("YYYY-MM-DD"));
  const [replicateId, setReplicateId] = useState("");
  const [deleteId, setDeleteId] = useState("");
  const [upload_Id, setUpload_Id] = useState("");
  const [showToast, setShowToast] = useState(false);

  const handleChange = (newValue) => {
    const convertDateyear = moment(newValue).format("YYYY-MM-DD");
    setFromDate(convertDateyear);
  };

  const handleDate = (val) => {
    const convertDateyear = moment(val).format("YYYY-MM-DD");
    setToDate(convertDateyear);
  };

  const handleClose = () => setOpen(false);
  const handleClose1 = () => setOpen1(false);
  const handleClick = (id) => {
    setDeleteId(id);
    setShowToast(true);
  };

  const ChangeFormValues = (e, name) => {
    if (name === "DepartureDate" || name === "ReturnDate") {
      setMainDetails({
        ...mainDetails,
        [name]: moment(e.target.value).format("MM/DD/YYYY"),
      });
    } else {
      setMainDetails({ ...mainDetails, [name]: e.target.value });
    }
  };

  const GetAllResult = () => {
    const options = {
      method: "GET",
      url: `${scrapping_Host}get_upload_id`,
    };

    axios
      .request(options)
      .then(function (response) {
        // console.log("response", response);
        setRouteData(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  };
  const replicateRecord = () => {
    replicateCallApi(
      fromDate,
      toDate,
      replicateId,
      handleClose1,
      setRefresh,
      refresh
    );
  };

  const UpdateData = (data) => {
    const options = {
      method: "PATCH",
      url: `${scrapping_Host}update/metadata`,
      headers: { "Content-Type": "application/json" },
      data: data,
    };

    axios
      .request(options)
      .then(function (response) {
        if (response.data.status === true) {
          // console.log("response", response.data);
          alert(" data Updated");
          handleClose();
          setRefresh(!refresh);
        }
      })
      .catch(function (error) {
        console.error(error);
      });
  };
  const DeleteRecord = (objID) => {
    handleClose2();
    const options = {
      method: "DELETE",
      url: `${scrapping_Host}deleteDataById`,
      headers: { "Content-Type": "application/json" },
      data: { id: objID },
    };

    axios
      .request(options)
      .then(function (response) {
        alert("Data has been Deleted ");
        setRefresh(!refresh);
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  const hnadleByUpload_Id = () => {
    var options = {
      method: "DELETE",
      url: `${scrapping_Host}delete_upload_id_record/${upload_Id}`,
    };
    axios
      .request(options)
      .then(function (response) {
        if (response.data.status === true) {
          alert(` ${response.data.deletedCount} Data has been Deleted`);
          setUpload_Id("");
          setRefresh(!refresh);
        }
      })

      .catch(function (error) {
        console.error(error);
        alert(` This upload Id ${upload_Id} has not been Exist`);
        setUpload_Id("");
      });
  };

  const handleOpen = (id) => {
    setObjectID(id);
    setOpen(true);
    routeData.Routeinfo.filter((item) => item._id === id).map(
      (item) => (
        setDepartFare(item.DepartureFare),
        setMainDetails(item),
        setReturnFare(item.ReturnFare)
      )
    );
  };

  const handleReplicate = (id, date, returnDate) => {
    const convertDateyear = moment(date).format("YYYY-MM-DD");
    const convertreturnDate = moment(returnDate).format("YYYY-MM-DD");
    setFromDate(convertDateyear);
    setToDate(
      convertreturnDate === "Invalid date" ? convertDateyear : convertreturnDate
    );
    setReplicateId(id);
    setOpen1(true);
  };

  const handleClose2 = () => {
    setShowToast(false);
  };
  useEffect(() => GetAllResult(), [refresh]);

  const mergobj = {
    ...mainDetails,
    DepartureFare: departFare,
    ReturnFare: returnFare,
  };

  const ActionStyle = {
    color: "#00ab55",
  };

  if (routeData.length === 0) {
    // return "Please While we fetch your data ";
    return <LoadingScreen />;
  }

  return (
    <Page>
      <Card>
        <div className="d-flex align-items-center ">
          <div className="w-25">
            <CardHeader title="All-Scrap-Data" />
          </div>
          <div className="d-flex align-items-center  justify-content-between w-full ">
            <Typography className="pt-4">
              {routeData.Routeinfo === undefined
                ? ""
                : `Number Of Result ${routeData.Routeinfo.length}`}
            </Typography>
            <div className="d-flex align-items-center">
              <TextField
                sx={{ width: "10rem", margin: 2 }}
                variant={"outlined"}
                placeholder="Enter ID"
                type="text"
                id="Upload_Id "
                name="Upload_Id"
                value={upload_Id}
                onChange={(e) => {
                  setUpload_Id(e.target.value);
                }}
              />

              <Button
                variant="contained"
                color="error"
                className="me-3"
                onClick={() => hnadleByUpload_Id()}
              >
                DeleteById
              </Button>
            </div>
          </div>
        </div>

        <TableContainer sx={{ minWidth: 720 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>Trip</TableCell>
                <TableCell>Meta</TableCell>
                <TableCell>From</TableCell>
                <TableCell>To</TableCell>
                <TableCell>DepartDate</TableCell>
                <TableCell>ReturnDate</TableCell>
                {/* <TableCell>Travellers</TableCell> */}
                <TableCell>Class</TableCell>
                <TableCell>Curr.</TableCell>
                {/* <TableCell>PriceOneWay</TableCell> */}
                <TableCell>TotalFare</TableCell>
                {/* <TableCell>Replicate </TableCell>
                <TableCell>Update</TableCell>
                <TableCell>Delete </TableCell> */}
                <TableCell className="text-center">Action</TableCell>
              </TableRow>
            </TableHead>

            {routeData.Routeinfo === undefined ? (
              <div className="d-flex align-items-center ">NO Data </div>
            ) : (
              <TableBody>
                <>
                  {routeData.Routeinfo.map((item, i) => (
                    <TableRow key={i}>
                      <TableCell>{item.upload_id}</TableCell>
                      <TableCell>{item.Trip === 1 ? "Dom " : "Int"}</TableCell>
                      <TableCell>{item.Meta}</TableCell>
                      <TableCell>{item.Origin}</TableCell>
                      <TableCell>{item.Destination}</TableCell>
                      <TableCell>{item.DepartureDate}</TableCell>
                      <TableCell>{item.ReturnDate}</TableCell>
                      {/* <TableCell>
                        {item.Adults + item.Childs + item.Infants}
                      </TableCell> */}
                      <TableCell>{item.TravelClass.slice(0, 3)}</TableCell>
                      <TableCell>{item.Currency}</TableCell>
                      {/* <TableCell>
                        {item.TripType === 2 && item.DepartureFare.TotalFare}{" "}
                      </TableCell> */}

                      <TableCell>
                        {item.DepartureFare
                          ? item.DepartureFare.TotalFare
                          : null}
                      </TableCell>
                      <TableCell>
                        <div className="d-flex justify-content-between">
                          <div className="border border-success rounded p-1 ">
                            <button
                              onClick={() =>
                                handleReplicate(
                                  item._id,
                                  item.DepartureDate,
                                  item.ReturnDate
                                )
                              }
                            >
                              <ContentCopyIcon style={ActionStyle} />
                            </button>
                          </div>
                          <div className="border border-success rounded p-1">
                            <button onClick={() => handleOpen(item._id)}>
                              <EditIcon style={ActionStyle} />
                            </button>
                          </div>

                          <div className="border border-danger rounded p-1">
                            <button onClick={() => handleClick(item._id)}>
                              <DeleteIcon style={{ color: "red" }} />
                            </button>
                          </div>
                        </div>
                      </TableCell>
                    </TableRow>
                  ))}
                </>
              </TableBody>
            )}
          </Table>
        </TableContainer>
      </Card>
      <Snackbar
        open={showToast}
        onClose={handleClose2}
        key={horizontal + vertical}
        anchorOrigin={{ vertical, horizontal }}
      >
        <Alert severity="warning" sx={{ width: "100%", marginTop: "10rem" }}>
          Are you sure your want to delete this itinerary
          <Stack
            sx={{
              display: "flex",
              flexDirection: "row-reverse",
              justifyContent: "space-around",
              padding: 1,
            }}
          >
            <Button
              size="small"
              onClick={() => handleClose2()}
              color="error"
              variant="contained"
            >
              Cancel
            </Button>
            <Button
              size="small"
              onClick={() => DeleteRecord(deleteId)}
              variant="contained"
            >
              Confirm
            </Button>
          </Stack>
        </Alert>
      </Snackbar>
      <Dialog open={open1} onClose={handleClose1} fullWidth={true}>
        <DialogTitle>Replicate</DialogTitle>
        <DialogContent>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              label="fromDate"
              inputFormat="dd/MM/yyyy"
              value={fromDate}
              onChange={handleChange}
              renderInput={(params) => <TextField {...params} />}
            />
          </LocalizationProvider>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              label="toDate"
              inputFormat="dd/MM/yyyy"
              value={toDate}
              onChange={handleDate}
              renderInput={(params) => <TextField {...params} />}
            />
          </LocalizationProvider>
          <Button onClick={() => replicateRecord()}>Sumbit</Button>
        </DialogContent>
      </Dialog>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth={true}
        maxWidth="xl"
      >
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <DialogTitle id="alert-dialog-title">Edit data</DialogTitle>
          <IconButton
            aria-label="delete"
            onClick={handleClose}
            style={{
              cursor: "pointer",
              marginRight: "50px",
              marginTop: "14px",
            }}
          >
            <CloseIcon />
          </IconButton>
        </div>
        <DialogContent>
          <Grid container columns={{ xs: 4, sm: 8, md: 12 }}>
            <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
              <Item>
                <Stack xs={12} sm={12} md={3}>
                  <Select
                    value={mainDetails.Trip}
                    label="Select Trip"
                    defaultValue={"International"}
                    placeholder="International"
                    disabled={true}
                    // onChange={(e) => ChangeFormValues(e, "Trip")}
                    sx={{ width: "20rem", margin: 2 }}
                  >
                    <MenuItem value={2}>International</MenuItem>
                    <MenuItem value={1}>Domestic</MenuItem>
                  </Select>
                </Stack>
                <Stack xs={12} sm={12} md={3}>
                  <Select
                    disabled={true}
                    value={mainDetails.Meta}
                    label="Select meta"
                    // onChange={(e) => ChangeFormValues(e, "Meta")}
                    sx={{ width: "20rem", margin: 2 }}
                  >
                    <MenuItem value="None">Select Meta</MenuItem>
                    <MenuItem value="jtrdr">Jet Radar</MenuItem>
                    <MenuItem value="online">Online</MenuItem>
                    <MenuItem value="skyscanner">SkyScanner</MenuItem>
                    <MenuItem value="jtrdr_ca">Jet RadarCA</MenuItem>
                    <MenuItem value="jtrdr_us">Jet RadarUS</MenuItem>
                    <MenuItem value="jtrdr_uk">Jet RadarUK</MenuItem>
                    <MenuItem value="jtrdr_in">Jet RadarIN</MenuItem>
                  </Select>
                </Stack>
                {validate === true && (
                  <Typography variant="subtitle2" sx={{ color: " red" }}>
                    Please Select The Meta
                  </Typography>
                )}
              </Item>
            </Grid>
            <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
              <Item>
                <Stack xs={12} sm={12} md={3}>
                  <TextField
                    variant={"outlined"}
                    label="Origin (LAX)"
                    id="Origin"
                    name="Origin"
                    disabled={true}
                    value={mainDetails.Origin}
                    // onChange={(e) => ChangeFormValues(e, "Origin")}
                    formControlProps={{
                      fullWidth: true,
                    }}
                    sx={{ width: "20rem", margin: 2 }}
                  />
                  {validate === true && (
                    <>
                      {mainDetails.Origin.length > 3 ? (
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          You have exceed the limits of entry
                        </Typography>
                      ) : (
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter origin{" "}
                        </Typography>
                      )}
                    </>
                  )}
                </Stack>

                <Stack xs={12} sm={12} md={3}>
                  <TextField
                    sx={{ width: "20rem", margin: 2 }}
                    variant={"outlined"}
                    label="Destination (MIA) "
                    id="Destination"
                    name="Destination"
                    disabled={true}
                    value={mainDetails.Destination}
                    // onChange={(e) => ChangeFormValues(e, "Destination")}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                  {validate === true && (
                    <>
                      {mainDetails.Destination.length > 3 ? (
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          You have exceed the limits of entry
                        </Typography>
                      ) : (
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please select destination{" "}
                        </Typography>
                      )}
                    </>
                  )}
                </Stack>
              </Item>
            </Grid>

            {mainDetails.TripType === 2 ? (
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack
                    xs={12}
                    sm={12}
                    md={3}
                    direction="column"
                    sx={{ justifyContent: "space-between" }}
                  >
                    <Stack>
                      <TextField
                        sx={{ width: "20rem", margin: 2 }}
                        variant={"outlined"}
                        type="date"
                        id="DepartureDate "
                        name="DepartureDate"
                        value={mainDetails.DepartureDate}
                        onChange={(e) => ChangeFormValues(e, "DepartureDate")}
                        formControlProps={{
                          fullWidth: true,
                        }}
                      />

                      {validate === true && (
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please select the departure date{" "}
                        </Typography>
                      )}
                    </Stack>
                  </Stack>
                  <Stack
                    xs={12}
                    sm={12}
                    md={3}
                    direction="column"
                    sx={{ justifyContent: "space-between" }}
                  >
                    <Stack>
                      <TextField
                        sx={{ width: "20rem", margin: 2 }}
                        variant={"outlined"}
                        type="date"
                        id="ReturnDate"
                        name="ReturnDate"
                        value={mainDetails.ReturnDate}
                        onChange={(e) => ChangeFormValues(e, "ReturnDate")}
                        formControlProps={{
                          fullWidth: true,
                        }}
                      />
                      {validate === true && (
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please Select return date{" "}
                        </Typography>
                      )}
                    </Stack>
                  </Stack>
                </Item>
              </Grid>
            ) : (
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack
                    xs={12}
                    sm={12}
                    md={3}
                    direction="column"
                    sx={{ justifyContent: "space-between" }}
                  >
                    <Stack>
                      <TextField
                        sx={{ width: "20rem", margin: 2 }}
                        variant={"outlined"}
                        type="date"
                        id="DepartureDate "
                        name="DepartureDate"
                        value={mainDetails.DepartureDate}
                        onChange={(e) => ChangeFormValues(e, "DepartureDate")}
                        formControlProps={{
                          fullWidth: true,
                        }}
                      />

                      {validate === true && (
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please select the departure date{" "}
                        </Typography>
                      )}
                    </Stack>
                  </Stack>
                </Item>
              </Grid>
            )}

            <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
              <Item>
                <Stack xs={12} sm={12} md={3}>
                  <Select
                    value={mainDetails.TravelClass}
                    label="Select TravelClass"
                    defaultValue={"Select class"}
                    onChange={(e) => ChangeFormValues(e, "TravelClass")}
                    sx={{ width: "20rem", margin: 2 }}
                  >
                    <MenuItem value="None">Select Class</MenuItem>
                    <MenuItem value="ECONOMY">Economy</MenuItem>
                    <MenuItem value="BUSINESS">business</MenuItem>
                    <MenuItem value="PREMIUM ECONOMY">Premium Economy</MenuItem>
                    <MenuItem value="FIRST CLASS">First Class</MenuItem>
                  </Select>
                  {validate === true && (
                    <Typography variant="subtitle2" sx={{ color: " red" }}>
                      Please select the class{" "}
                    </Typography>
                  )}
                  {/* {returnDetails.length === 2 ? null : (
                        <Grid
                          spacing={{ xs: 2, md: 3 }}
                          columns={{ xs: 4, sm: 8, md: 12 }}
                        >
                          <Item>
                            <Box sx={{ p: 2, textAlign: "left" }}>
                              <Button
                                variant="contained"
                                onClick={() => setShow(true)}
                                endIcon={<Icon icon={plusCircleOutline} />}
                              >
                                Add return Details
                              </Button>
                            </Box>
                          </Item>
                        </Grid>
                      )} */}
                </Stack>
              </Item>
            </Grid>
            <Grid spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
              <Item>
                <Stack xs={12} sm={12} md={3}>
                  <TextField
                    sx={{ width: "20rem", margin: 2 }}
                    variant={"outlined"}
                    label="Currency (USD)"
                    id="Currency"
                    disabled={true}
                    name="Currency"
                    value={mainDetails.Currency}
                    onChange={(e) => ChangeFormValues(e, "Currency")}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                  {validate === true && (
                    <>
                      {mainDetails.Currency.length > 3 ? (
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          You have exceed the limits of entry
                        </Typography>
                      ) : (
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please add currency{" "}
                        </Typography>
                      )}
                    </>
                  )}
                </Stack>
              </Item>
            </Grid>

            {mainDetails.TripType === 1 ? (
              <DepartureForm
                departDetails={mainDetails.DepartureFlight}
                departFare={departFare}
                validate={validate}
                setDepartDetails={setDepartDetails}
                setDepartFare={setDepartFare}
              />
            ) : (
              <>
                <Divider />

                <DepartureForm
                  departDetails={mainDetails.DepartureFlight}
                  departFare={departFare}
                  validate={validate}
                  setDepartDetails={setDepartDetails}
                  setDepartFare={setDepartFare}
                />
                <Divider />
                <ReturnForm
                  returnDetails={mainDetails.ReturnFlight}
                  returnFare={returnFare}
                  setReturnDetails={setReturnDetails}
                  setReturnFare={setReturnFare}
                />
              </>
            )}

            <Box sx={{ p: 2, flexGrow: 1, textAlign: "right" }}>
              <Button variant="contained" onClick={() => UpdateData(mergobj)}>
                Submit
              </Button>
            </Box>
          </Grid>
        </DialogContent>
      </Dialog>
    </Page>
  );
};

export default ViewData;
