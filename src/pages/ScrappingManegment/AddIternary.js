import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components

import axios from "axios";
import { Stack, Paper, Box, Typography } from "@material-ui/core";
import { styled } from "@mui/material/styles";
import AddOneWayData from "./AddOneWayData";
import AddTwoWayData from "./AddTwoWayData";

export default function SelectTripWay() {
  const [tripType, setTripType] = useState(1);
  return (
    <>
      <Box
        sx={{
          flexGrow: 1,
          backgroundColor: (theme) => theme.palette.primary.lighter,
          display: "flex",
          width: "15rem",
          justifyContent: "space-around",
          alignItems: "center",
          padding: 1,
          borderRadius: 1,
        }}
      >
        <Stack
          onClick={() => setTripType(1)}
          sx={{
            backgroundColor: tripType === 1 && "#eee",
            padding: 2,
            borderRadius: 2,
          }}
          className="cursor-pointer"
        >
          <Typography variant="h6">One Way</Typography>
        </Stack>

        <Stack
          onClick={() => setTripType(2)}
          sx={{
            backgroundColor: tripType === 2 && "#eee",
            padding: 2,
            borderRadius: 2,
          }}
          className="cursor-pointer"
        >
          <Typography variant="h6">Two Way</Typography>
        </Stack>
      </Box>
      {tripType === 1 ? <AddOneWayData /> : <AddTwoWayData />}
    </>
  );
}
