import React, { useEffect, useState } from "react";
import axios from "axios";
import useSettings from "src/hooks/useSettings";
import { useTheme, styled } from "@material-ui/core/styles";
import {
  Button,
  Card,
  CardHeader,
  Stack,
  Table,
  TableContainer,
  Checkbox,
  Paper,
  TableCell,
  TableHead,
  TableRow,
  TableBody,
  Snackbar,
  Alert,
} from "@material-ui/core";
import { Icon } from "@iconify/react";
import { Link } from "react-router-dom";
import arrowheadrightfill from "@iconify/icons-eva/arrowhead-right-fill";
import Page from "src/components/Page";
import { scrapping_Host } from "src/HostName";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  display: "flex",
  color: theme.palette.text.secondary,
  borderWidth: 1,
  borderRadius: 10,
  alignItems: "center",
}));

const ViewScappyUser = () => {
  const { themeStretch } = useSettings();
  const [agents, setAgents] = useState([]);
  const [isLoading, setIsloading] = useState(true);

  const [showToast, setShowToast] = useState(false);

  const GetScrapAgent = () => {
    const options = {
      method: "GET",
      url: `${scrapping_Host}getScrappingAgents`,
    };

    axios
      .request(options)
      .then(function (response) {
        if (response.data) {
          setAgents(response.data);
          setIsloading(false);
        }
      })
      .catch(function (error) {
        console.error(error);
      });
  };
  useEffect(() => GetScrapAgent(), []);

  const handleClose2 = () => {
    setShowToast(false);
  };
  return (
    <Page>
      <Card>
        <CardHeader title="All-Scrap-Data" />
        <TableContainer sx={{ minWidth: 720 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>TripType</TableCell>
                <TableCell>Meta</TableCell>
                <TableCell>From</TableCell>
                <TableCell>To</TableCell>
                <TableCell>DepartureDate</TableCell>
                <TableCell>ReturnDate</TableCell>
                <TableCell>No Of Travellers</TableCell>
                <TableCell>TravelClass</TableCell>
                <TableCell>Currency</TableCell>
                <TableCell>PriceOneWay</TableCell>
                <TableCell>PriceTwoWay</TableCell>
                <TableCell>Update Data</TableCell>
                <TableCell>Delete Data</TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {agents.map((item, i) => (
                <TableRow key={i}>
                  <TableCell>
                    {item.Trip === 1 ? "Domestic " : "International"}
                  </TableCell>
                  <TableCell>{item.userName}</TableCell>
                  <TableCell>
                    {item.metaPermission.map((item) => item + "      ")}
                  </TableCell>
                  <TableCell>
                    {item.currencyPermission.map((item) => item + "      ")}
                  </TableCell>
                  <TableCell>
                    <Button
                      variant="contained"
                      onClick={() => console.log("item._id")}
                    >
                      Edit
                    </Button>
                  </TableCell>
                  <TableCell>
                    <Button
                      variant="contained"
                      color="error"
                      onClick={() => alert("working")}
                    >
                      Delete
                    </Button>
                  </TableCell>

                  <Snackbar
                    open={showToast}
                    onClose={handleClose2}
                    // key={horizontal + vertical}
                    // anchorOrigin={{ vertical, horizontal }}
                  >
                    <Alert
                      severity="warning"
                      sx={{ width: "100%", marginTop: "10rem" }}
                    >
                      Are you sure your want to delete this itinerary
                      <Stack
                        sx={{
                          display: "flex",
                          flexDirection: "row-reverse",
                          padding: 1,
                        }}
                      >
                        <Button
                          size="small"
                          onClick={() => handleClose2()}
                          color="error"
                          variant="contained"
                        >
                          Cancel
                        </Button>
                        <Button
                          size="small"
                          onClick={() => alert("item._id")}
                          variant="contained"
                        >
                          Confirm
                        </Button>
                      </Stack>
                    </Alert>
                  </Snackbar>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Card>
    </Page>
  );
};

export default ViewScappyUser;
