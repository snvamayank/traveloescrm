import {
  Button,
  Card,
  CardHeader,
  Divider,
  Stack,
  Typography,
} from "@material-ui/core";
import React from "react";
import Lottie from "react-lottie";
import bookingSuccess from "../../LottieView/failure.json";
import { useNavigate } from "react-router";

const Success = () => {
  const navigate = useNavigate();
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: bookingSuccess,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <>
      <Card sx={{ margin: 10, padding: 5 }}>
        <Stack justifyContent={"center"} alignItems="center">
          <Stack>
            <Lottie options={defaultOptions} height={200} width={200} />
          </Stack>
          <Stack>
            <Typography variant="h6">
              Your Booking has not been Confirmed
            </Typography>
          </Stack>
          <Stack>Will get in touch with you soon</Stack>
          <Stack>
            <Button
              variant="contained"
              sx={{ margin: 2 }}
              onClick={() => navigate("/dashboard/app")}
            >
              Go to dashboard
            </Button>
          </Stack>
        </Stack>
      </Card>
    </>
  );
};

export default Success;
