import {
  Button,
  Card,
  CardHeader,
  Divider,
  Stack,
  Typography,
} from "@material-ui/core";
import React from "react";
import Lottie from "react-lottie";
import bookingSuccess from "../../LottieView/successful.json";
import { useNavigate } from "react-router";
// import BOOKINGRESP from "../../utils/DummyBookingResponse";
import moment from "moment";

const Success = ({ flight, BOOKINGRESP }) => {
  const navigate = useNavigate();
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: bookingSuccess,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  // const ConvertMinsToTime = ({ data }) => {
  //   let hours = Math.floor(data / 60)
  //   let minutes = data % 60
  //   minutes = minutes < 10 ? '0' + minutes : minutes
  //   return `${hours}h:${minutes}m`
  // }
  // const fromDate = flight.FlightResult_.outBound[0].depDate
  //   .split('T')[1]
  //   .substring(5, 0)
  // const toDate = flight.FlightResult_.outBound[0].reachDate
  //   .split('T')[1]
  //   .substring(5, 0)
  // const depDate = flight.FlightResult_.outBound[0].depDate.split('T')[0]
  // const reachDate = flight.FlightResult_.outBound[0].reachDate.split('T')[0]
  // const formattedDepDate = moment(depDate).format('DD/MM/YYYY')
  // const formattedReachDate = moment(reachDate).format('DD/MM/YYYY')
  return (
    <>
      <Card sx={{ margin: 10, padding: 5 }}>
        <Stack justifyContent={"center"} alignItems="center">
          <Stack>
            <Lottie options={defaultOptions} height={200} width={200} />
          </Stack>
          <Stack>
            <Typography variant="h6">
              Your Booking has been Confirmed
            </Typography>
          </Stack>
          <Stack sx={{ margin: 2 }}>Your flight details are shown below</Stack>
          <Stack>
            <Button
              variant="contained"
              onClick={() => navigate("/dashboard/app")}
            >
              Go to dashboard
            </Button>
          </Stack>
        </Stack>
      </Card>
      <Card
        sx={{
          margin: 10,
        }}
      >
        <CardHeader title="Ticket Details" sx={{ margin: 1 }} />
        <Divider />

        <Stack
          direction={"row"}
          justifyContent="space-around"
          alignItems={"center"}
          sx={{ margin: 1 }}
        >
          <Stack direction={"row"} alignItems="center">
            <Stack>
              <Typography variant="subtitle1">Your Booking ID :</Typography>
              {/* <Typography variant="h6">{BOOKINGRESP.bookingID}</Typography> */}
            </Stack>
          </Stack>
          <Stack sx={{ m: 2 }}>
            <Typography variant="subtitle1"> Your PNR : </Typography>
            {/* <Typography variant="h6"> {BOOKINGRESP.pNR}</Typography> */}
          </Stack>

          {/* <Stack>
            <Typography variant="subtitle1">Booking status</Typography>
            <Typography
              sx={{
                color:
                  BOOKINGRESP.bookingStatus === 'Confirm'
                    ? '#00CC00'
                    : '#FFCCCB',
              }}
              variant="h6"
            >
              {BOOKINGRESP.bookingStatus}
            </Typography>
          </Stack> */}
        </Stack>
      </Card>
      {/* <Card
        sx={{
          margin: 10,
        }}
      >
        <CardHeader title="Flight Details" sx={{ margin: 1 }} />
        <Divider />

        <Stack
          direction={'row'}
          justifyContent="space-around"
          alignItems={'center'}
          sx={{ margin: 1 }}
        >
          <Stack direction={'row'} alignItems="center">
            <img
              src={`https://www.travomint.com/resources/images/airline-logo/${flight.FlightResult_.airline}.png`}
              className="w-20 down rounded-xl inline float-right"
            />
            <Stack sx={{ m: 2 }}>
              <Typography variant="h5">
                {flight.FlightResult_.outBound[0].airlineName}
              </Typography>
              <Typography>
                {flight.FlightResult_.outBound[0].flightNo} -
                {flight.FlightResult_.outBound[0].airline}
              </Typography>
              <Typography>
                {flight.FlightResult_.outBound[0].cabinClass === 1
                  ? 'Economy'
                  : flight.FlightResult_.outBound[0].cabinClass === 2
                  ? 'Premium Economy'
                  : flight.FlightResult_.outBound[0].cabinClass === 3
                  ? 'Busniess Class'
                  : flight.FlightResult_.outBound[0].cabinClass === 4
                  ? 'First Class'
                  : null}{' '}
              </Typography>
            </Stack>
          </Stack>
          <Stack>
            <Typography variant="h5">
              {flight.FlightResult_.outBound[0].fromAirport}
            </Typography>
            <Typography variant="subtitle1">{fromDate}</Typography>
            <Typography variant="subtitle1">{formattedDepDate}</Typography>
          </Stack>
          <Stack>
            <Typography variant="h5">
              <Typography variant="subtitle1">
                <ConvertMinsToTime
                  data={
                    flight.FlightResult_.outBound[0].eft +
                    flight.FlightResult_.outBound[0].layOverTime
                  }
                />
              </Typography>
            </Typography>
          </Stack>

          <Stack>
            <Typography variant="h5">
              {flight.FlightResult_.outBound[0].toAirport}
            </Typography>
            <Typography variant="subtitle1">{toDate}</Typography>
            {formattedReachDate}
          </Stack>
        </Stack>
        <Divider />
      </Card> */}
    </>
  );
};

export default Success;
