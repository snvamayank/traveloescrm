import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  Card,
  Divider,
  Grid,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { CardHeader } from "@mui/material";
import Scrollbar from "src/components/Scrollbar";
import Label from "src/components/Label";
import { scrapping_Host } from "src/HostName";

const Metamange = () => {
  const [meta, setMeta] = useState([]);
  const [value, setValue] = useState(false);
  /* --------------------------------------FUNCTIONS------------------------------------ */

  const changeState = (e, id) => {
    setValue(!value);
    let StateObj;
    meta
      .filter((item) => item._id == id)
      .map(
        (item) =>
          (StateObj = {
            gds: item.gds,
            source: item.source,
            status: Boolean(value),
          })
      );
    UpdateMeta(StateObj);
  };

  /* --------------------------------------FUNCTIONS------------------------------------ */
  /* ----------------------------------API CALLS--------------------------------------- */
  const getMeta = () => {
    const options = {
      method: "GET",
      url: `${scrapping_Host}getallgdsHandle/mystifly`,
    };

    axios
      .request(options)
      .then(function (response) {
        setMeta(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  };
  const UpdateMeta = (StateObj) => {
    const options = {
      method: "PATCH",
      url: `${scrapping_Host}updateStatus`,
      headers: { "Content-Type": "application/json" },
      data: StateObj,
    };

    axios
      .request(options)
      .then(function (response) {
        if (response.data.status == true) {
          getMeta();
          console.log("ApiHite");
        }
      })
      .catch(function (error) {
        console.error(error);
      });
  };
  useEffect(() => getMeta(), []);
  /* ----------------------------------API CALLS--------------------------------------- */

  return (
    <Grid>
      <Card>
        <CardHeader title="All Details" sx={{ mb: 3 }} />
        <Scrollbar>
          <TableContainer sx={{ minWidth: 720 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Source</TableCell>
                  <TableCell>Status</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {meta.map((row) => (
                  <TableRow>
                    <TableCell>
                      <Typography variant="h5" sx={{ fontWeight: "normal" }}>
                        {row.source}
                      </Typography>
                    </TableCell>

                    <TableCell>
                      <Switch
                        checked={row.status}
                        onChange={(e) => changeState(e, row._id)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <Divider />
      </Card>
    </Grid>
  );
};
export default Metamange;
