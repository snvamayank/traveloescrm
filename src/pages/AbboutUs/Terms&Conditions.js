import { Box, Card, Container, Button } from "@material-ui/core";
import React, { useCallback, useState } from "react";
import HeaderBreadcrumbs from "src/components/HeaderBreadcrumbs";
import Page from "src/components/Page";
import { PATH_DASHBOARD } from "src/routes/paths";
import { Link as RouterLink } from "react-router-dom";
import { Icon } from "@iconify/react";
import plusFill from "@iconify/icons-eva/plus-fill";
import useSettings from "src/hooks/useSettings";
import { useDispatch, useSelector } from "src/redux/store";
import { getMorePosts, getPostsInitial } from "src/redux/slices/blog";

const TermsAndConditons = () => {
  const { themeStretch } = useSettings();
  const dispatch = useDispatch();

  return (
    <Page title="Terms And Conditions | Traveloes">
      <Container maxWidth={themeStretch ? false : "lg"}>
        <HeaderBreadcrumbs
          heading="Terms and Conditions"
          links={[
            { name: "Dashboard", href: PATH_DASHBOARD.root },
            {
              name: "Terms And Conditions",
              href: PATH_DASHBOARD.aboutus.terms,
            },
          ]}
          action={
            <Button
              variant="contained"
              component={RouterLink}
              to={PATH_DASHBOARD.blog.newPost}
              startIcon={<Icon icon={plusFill} />}
            >
              New Post
            </Button>
          }
        />

        <Card>
          <Box sx={{ p: { xs: 3, md: 5 } }}>
            <div>
              <div>
                <h2>Traveloes- Terms and Conditions</h2>
                <p>
                  Please read out the Terms and Conditions carefully before
                  using the website and its content. Your use of this website is
                  completely based on your consent in regard to the Terms and
                  conditions provided here. Also, by using our website you agree
                  with the conditions. In case, if you do not agree with any
                  section of the conditions, you should not make use of this
                  website. Traveloes also reserves all the right to make changes
                  in the terms at its own will from time to time.{" "}
                </p>
                <h4>Basic Terms and Conditions</h4>
                <p>
                  <strong>1. Use of the website</strong>
                </p>
                <p>By using Traveloes website, you permit that:</p>
                <ul>
                  <li>
                    You have a legal authority to create a legal obligation and
                    enter the Terms of use.{" "}
                  </li>
                  <li>
                    You will use the website in accordance with the Terms of
                    use.{" "}
                  </li>
                  <li>
                    You will use this website for booking legit flight tickets
                    for you or for any other person on whose behalf you can act
                    legally.
                  </li>
                  <li>
                    You will inform the other person about the Terms of use that
                    apply to the reservations including the rules and
                    restrictions.{" "}
                  </li>
                  <li>
                    You will provide the correct Name, email Id, address, and
                    phone number. Also, you will be completely responsible for
                    the information provided. In case of mistake and error,
                    Traveloes will not take any liability for the information
                    provided.
                  </li>
                </ul>
                <p>
                  Besides, Traveloes retains the right of sole discretion as per
                  which one can be denied access to the website and services
                  offered without any prior notice and for any reasons including
                  or limited to the violations of the Terms and Conditions.{" "}
                </p>
                <p>
                  <strong>2. The site and its content </strong>
                </p>
                <p>
                  This site is only meant for personal use. You are not
                  permitted to copy, adapt, exchange, modify, sell or transmit
                  any of the content published on the website.
                </p>
                <p />
                <p>
                  Traveloes offers comprehensive, non-transferable and a limited
                  power to view, use and make purchases on this site. Also, you
                  agree that you will not interrupt or make any effort to
                  interrupt the operations of the site.{" "}
                </p>
                <p />
                <p>
                  You recognize that excluding information,services,and travel
                  products being provided under the name of Traveloes, we do not
                  control, operate or endorse any of the information, service,
                  and product on the internet. You agree that Tavomint does not
                  guarantee or warrant that the files available for downloading
                  through the site are free from worms, viruses or code that can
                  be damaging. Also, you are responsible for executing the
                  processes to fulfill a certain requirement and for the
                  efficiency of the data input and output.{" "}
                </p>
                <p />
                <p>
                  Traveloes has the right to modify, change, add, remove or
                  suspend any of the content or the services displayed on the
                  website that includes the specifications and products
                  described on the site on a temporary or permanent basis, any
                  time without any prior notice or liability.{" "}
                </p>
                <p>
                  <strong>3. Payment options</strong>
                </p>
                <p />
                <p>
                  <strong>A) Online card payment </strong>
                </p>
                <p>
                  Master, American Express, Master Card payments are processed
                  using the online payment gateway system. You do not need to
                  worry about the card information details provided as the bank
                  will directly authorize the card transactions without any
                  information accessible to us. And within 25-20 seconds the
                  bank generates a code that authorizes the payment and
                  completion of the transaction.
                </p>
                <p />
                <p>
                  As Traveloes uses the latest encryption technology, one can
                  easily book their product using SSL encryption. Further,
                  making transactions online using a credit card is way safer as
                  we do not retain any credit card information. You can be
                  guaranteed that Traveloes offers the premium security services
                  available on the internet. In case, if the credit card
                  transaction is declined, an alternative payment option will be
                  offered to you 72 hours prior to the departure or the booking
                  will be cancelled.{" "}
                </p>
                <p />
                <p>
                  Further, Traveloes charges a service fee for all the domestic
                  reservations. And in case of cancellation, this fee might not
                  be refunded.{" "}
                </p>
                <p>
                  <strong>B) Internet Banking</strong>
                </p>
                <p>
                  Traveloes accepts payment net banking payments for the orders
                  booked on the site. The amount for the booking will be debited
                  automatically and all the payments will be processed through
                  an online gateway to ensure safe transactions.
                </p>
                <p>
                  <strong> 4.Confirmation of transaction</strong>
                </p>
                <p>
                  You should avoid taking any actions based on the information
                  displayed on the website until you receive a confirmation of
                  the transaction. In case of confirmation via email, if you
                  fail to receive an email confirming your purchase or
                  transaction within a certain time, initially look for the
                  confirmation email in the Junk folder or else feel free to
                  contact our call center.{" "}
                </p>
                <p>
                  <strong>
                    5.Delivery of the service or product purchased
                  </strong>
                </p>
                <p />
                <p>
                  <strong>A) What is an electronic ticket? </strong>
                </p>
                <p />
                <p>
                  An e-ticket is a paperless or electronic form of the ticket
                  with a unique confirmation code provided to the passengers.
                  Further, the passenger needs to provide that unique code at
                  the airport to claim their ticket
                </p>
                <p />
                <p>
                  <strong> B) How to get details of the e-ticket? </strong>
                </p>
                <p />
                <p>
                  The details about the e-ticket will be provided to the
                  passengers via email at the time of confirming the
                  reservations. If the passenger fails to receive any details of
                  the e-ticket within 8-hours of the booking then, one can
                  contact the customer care for the required assistance
                </p>
                <p />
                <p>
                  <strong>C) Is it important to carry an e-ticket?</strong>{" "}
                </p>
                <p>
                  Yes, it is essential to carry a copy of the e-ticket. In case,
                  if the passenger fails to present a copy of e-ticket,
                  Traveloes will not be liable, if the airline does not issue
                  boarding pass or issues with traveling.
                </p>
                <p>
                  <strong>
                    D) How to get a boarding pass for the e-ticket?{" "}
                  </strong>
                </p>
                <p>
                  To get a boarding pass, you need to present your the e-ticket
                  with the confirmation mail along with a photo identity proof
                  at the check-in counter. Once the documents are verified, the
                  airline representative will issue you a boarding pass.
                </p>
                <p />
                <p>
                  <strong>6.Cancellation and Amendment policy</strong>
                </p>
                <p>
                  One can easily cancel their domestic flight tickets online.
                  For cancelling a booking, log in to My Bookings and select a
                  booking and click on the Cancel button. Further, for online
                  cancellation, there might be some cancellation charges.
                </p>
                <p />
                <p>
                  Further, to cancel and amend booking offline, one can contact
                  Traveloes customer service. Further, for offline cancellation,
                  some charges might be applied for both domestic and
                  international travel that can be confirmed by contacting the
                  customer support.{" "}
                </p>
                <p />
                <p>
                  Also, there are charges for rescheduling the flight or in the
                  case of no-show for domestic flights. Besides, it is
                  recommended to refer to the airline amendment and cancellation
                  policy before purchasing services at Traveloes.
                </p>
                <p>
                  <strong>
                    7.How to get a refund for a cancelled ticket or reservation?{" "}
                  </strong>
                </p>
                <p>
                  For unused and no-show bookings, you are required to make a
                  request to claim a valid refund as defined in the airline
                  policies within 90 days from the departure of the flight.
                  Further, no refund will be offered if the refund request is
                  filed after the expiry of 90 days and all the unclaimed amount
                  will be presumed to be forfeited.
                </p>
                <p />
                <p>
                  The refund should be processed within 15-20 days from the date
                  of the cancellation request. And if the payment is provided
                  using a valid credit card then, the amount will be refunded to
                  the credit card. Besides, the payment made for the
                  reservations using another mode of payment, then the refund
                  will be provided in the form of a cheque within 30 days of the
                  refund request.
                </p>
                <p>
                  <strong>8.Details on promotion codes</strong>
                </p>
                <p>
                  Traveloes generates promotional codes timely that one can
                  avail on the site as a coupon. Also, Traveloes holds all the
                  rights to modify, add, withdraw any terms and conditions in
                  parts or completely without giving any prior notice.
                </p>
                <p>
                  When you visit the Traveloes website and sign up for some
                  information regarding travel offers, our representative or
                  partners can contact you on a frequent basis to offer
                  information about the products and services that we think
                  might benefit you.
                </p>
                <p />
                <p>
                  <strong>9.Passport details</strong>
                </p>
                <p>
                  The passport details are important for the issuance of
                  e-ticket to the USA, Europe, and Canada. Further, few airlines
                  who are traveling to these destinations might need the details
                  of the passport for issuing an e-ticket.
                </p>
                <p>
                  <strong>10.Guidelines for Visa</strong>
                </p>
                <p>
                  It is recommended that you carry a valid visa for the country
                  to which you are traveling to or through. Also, check all the
                  visa requirements of the airline and the embassy to avoid any
                  issues related to the visa requirements.
                </p>
                <p>
                  <strong>11.JURISDICTION</strong>
                </p>
                <p>
                  This Agreement is subject to interpretation as per the laws of
                  India, and the parties shall refer any unresolved disputes to
                  the exclusive jurisdiction of courts in Noida.
                </p>
                <div className="col-xs-12 col-md-4">
                  <div className="back-clr col-xs-12">
                    <p style={{ textAlign: "left" }} />
                  </div>
                  <div className="col-xs-12" style={{ padding: 0 }}>
                    <ul className="right-contact"></ul>
                  </div>
                </div>
              </div>
              {/*col-xs-12 col-sm-12 col-md-12 col-lg-12*/}
            </div>
          </Box>
        </Card>
      </Container>
    </Page>
  );
};

export default TermsAndConditons;
