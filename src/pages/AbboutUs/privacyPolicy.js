import React, { useCallback, useEffect, useState } from "react";
import {
  Box,
  Container,
  Grid,
  Skeleton,
  Stack,
  Button,
  Card,
  Typography,
  Divider,
} from "@material-ui/core";
import Page from "src/components/Page";
import InfiniteScroll from "react-infinite-scroll-component";
import {
  BlogPostCard,
  BlogPostsSearch,
  BlogPostsSort,
} from "src/components/_dashboard/blog";
import { orderBy } from "lodash";
import useSettings from "src/hooks/useSettings";
import { useDispatch, useSelector } from "react-redux";
import { getMorePosts, getPostsInitial } from "src/redux/slices/blog";
import HeaderBreadcrumbs from "src/components/HeaderBreadcrumbs";
import { PATH_DASHBOARD } from "src/routes/paths";
import { Link as RouterLink } from "react-router-dom";
import { Icon } from "@iconify/react";
import plusFill from "@iconify/icons-eva/plus-fill";
const SORT_OPTIONS = [
  { value: "latest", label: "Latest" },
  { value: "popular", label: "Popular" },
  { value: "oldest", label: "Oldest" },
];

// ----------------------------------------------------------------------

const applySort = (posts, sortBy) => {
  if (sortBy === "latest") {
    return orderBy(posts, ["createdAt"], ["desc"]);
  }
  if (sortBy === "oldest") {
    return orderBy(posts, ["createdAt"], ["asc"]);
  }
  if (sortBy === "popular") {
    return orderBy(posts, ["view"], ["desc"]);
  }
  return posts;
};

const SkeletonLoad = (
  <Grid container spacing={3} sx={{ mt: 2 }}>
    {[...Array(4)].map((_, index) => (
      <Grid item xs={12} md={3} key={index}>
        <Skeleton
          variant="rectangular"
          width="100%"
          sx={{ height: 200, borderRadius: 2 }}
        />
        <Box sx={{ display: "flex", mt: 1.5 }}>
          <Skeleton variant="circular" sx={{ width: 40, height: 40 }} />
          <Skeleton variant="text" sx={{ mx: 1, flexGrow: 1 }} />
        </Box>
      </Grid>
    ))}
  </Grid>
);

const PrivacyPolicy = () => {
  const { themeStretch } = useSettings();
  const dispatch = useDispatch();
  const [filters, setFilters] = useState("latest");
  const { posts, hasMore, index, step } = useSelector((state) => state.blog);
  const sortedPosts = applySort(posts, filters);
  const onScroll = useCallback(() => dispatch(getMorePosts()), [dispatch]);

  useEffect(() => {
    dispatch(getPostsInitial(index, step));
  }, [dispatch, index, step]);

  const handleChangeSort = (event) => {
    setFilters(event.target.value);
  };
  return (
    <Page title="Privacy Policy | Traveloes">
      <Container maxWidth={themeStretch ? false : "lg"}>
        <HeaderBreadcrumbs
          heading="Privacy Policy"
          links={[
            { name: "Dashboard", href: PATH_DASHBOARD.root },
            { name: "Privacy Policy", href: PATH_DASHBOARD.aboutus.privacy },
          ]}
          action={
            <Button
              variant="contained"
              component={RouterLink}
              to={PATH_DASHBOARD.blog.newPost}
              startIcon={<Icon icon={plusFill} />}
            >
              New Post
            </Button>
          }
        />

        <Card>
          <Box sx={{ p: { xs: 3, md: 5 } }}>
            <Typography variant="h3" sx={{ mb: 5 }}>
              Privacy Policy General Information
            </Typography>

            <Box sx={{ my: 5 }}>
              <Divider />
              <Typography variant="subtitle1" sx={{ margin: 2 }}>
                Traveloes respects their customers and value their privacy and
                relationship with the business partners, clients and third-party
                who are in a contractual relationship with Traveloes or any
                party who purchase, inquire about purchase or intend to purchase
                any product or service offered by Traveloes or any of the
                interface channels like mobile, website, mobile app or via
                offline modes like call centers or offices. We are dedicated to
                protecting your personally identifiable information by which one
                can be identified referred to as personal information, personal
                data or information or any special category of data by handling
                it the data with responsibility and by using it with the
                technical and organizational standard. We assure that we at
                Traveloes follow relevant standards when it comes to protecting
                your data privacy. This policy defines the type of personal
                information collected by the website, how it is processed and
                protected, how users can use their rights while accessing the
                website. By accessing and using the Traveloes website, the user
                agrees with the terms of the privacy and the contents mentioned.
                Further, this policy does not apply to third parties even if the
                parties and websites are linked to our website.
              </Typography>
              <Divider />
            </Box>

            <Box sx={{ display: "flex", mb: 2 }}>
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="why_choose_Traveloes">
                  <h2>
                    <p>1.Personal information collected by Traveloes</p>
                  </h2>
                </div>
                <p>
                  <strong>
                    A. We collect the following information while purchasing
                    service through our website:
                  </strong>
                </p>
                <ul>
                  <li>Name, date of birth and gender</li>
                  <li>Contact details like the address and phone number</li>
                  <li>Country of the residence</li>
                  <li>Details of the credit card</li>
                  <li>Passport information and frequent flyer details</li>
                  <li>
                    Family details like marital status or number of dependents{" "}
                  </li>
                  <li>Educational and employment details</li>
                  <li>Tax and financial related info</li>
                  <li>Browser, IP address and access timing details</li>
                  <li>Information about complaints made by you</li>
                  <li>Information on how you use the services and products</li>
                </ul>
                <p>
                  <strong>
                    B.Personal data collected can also include sensitive or
                    special categories of information that include:
                  </strong>
                </p>
                <ul>
                  <li>Dietary requirements of the passengers</li>
                  <li>Special health requirements</li>
                </ul>
                <p>
                  <strong>
                    C.Personal details of the customers, travelers, and other
                    users
                  </strong>
                </p>
                <p>
                  If you provide personal information about the individuals
                  other than you then, you agree:
                </p>
                <ul>
                  <li>
                    To inform the particular individual about the privacy policy
                  </li>
                  <li>
                    And to obtain legal consent from the individual for the
                    collection, use, and transfer of the data collected
                  </li>
                </ul>
              </div>
            </Box>

            <Box sx={{ display: "flex", mb: 2 }}>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="why_choose_Traveloes">
                  <h2>
                    <p> 2. How do we collect personal data for use?</p>
                  </h2>
                </div>

                <p>
                  <strong>A.Directly</strong>
                </p>

                <p>
                  We can obtain the personal data directly from the users
                  through various modes but not limited to the business cards
                  offered by the user, who have completed our online form for
                  query or any records related to their previous requests. We
                  can also collect information directly while we are developing
                  a business relation or at the time of fulfilling professional
                  service.
                </p>

                <p>
                  <strong>B.Indirectly</strong>
                </p>

                <p>
                  We can also collect data about individuals indirectly via
                  various sources that include travel agents, social media,
                  travel websites, or through an authorized representative. We
                  may also attach the data with our relationship management
                  files to understand you better and offer you the best
                  services.
                </p>
              </div>
            </Box>
            <Box sx={{ display: "flex", mb: 2 }}>
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="why_choose_Traveloes">
                  <h2>
                    <p>3. How will we process or use your collected data?</p>
                  </h2>
                </div>
                <p>
                  The data we collect from the users are not meant for trade or
                  sale until the prior consent of the user. Further, the data
                  collected is processed in the following ways:{" "}
                </p>
                <ul>
                  <li>
                    To book reservations for you and for the individual whose
                    information is provided.
                  </li>
                  <li>
                    Net banking and credit card details are directly collected
                    by the banks and payment gateways
                  </li>
                  <li>
                    The details collected might be processed by the
                    third-parties for the process of discounts and cashback
                  </li>
                  <li>
                    To respond and manage the request made through our site
                  </li>
                  <li>To fulfill legal requirements</li>
                  <li>To promote our services and products</li>
                </ul>
                <p>
                  Besides, the data can also be collected from Cookies and app
                  permissions enabled by the user while accessing our website
                </p>
              </div>
            </Box>
            <Box sx={{ display: "flex", mb: 2 }}>
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="why_choose_Traveloes">
                  <h2>
                    <p>4. With whom we share the collected data? </p>
                  </h2>
                </div>
                <p>
                  Concerning the details mentioned on how we will process and
                  use your data, we may reveal your personal information to:
                </p>
                <ul>
                  <li>Qualified Authorities</li>
                  <li>
                    Other company to whom we need to transfer our rights and
                    obligations
                  </li>
                  <li>
                    Other company and person after the restructure and
                    acquisition of the company
                  </li>
                  <li>
                    The credit reference agencies or organizations that help in
                    reducing the risk of fraud
                  </li>
                </ul>
              </div>
            </Box>
            <Box sx={{ display: "flex", mb: 2 }}>
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="why_choose_Traveloes">
                  <h2>
                    <p>5. Where can we transfer your collected data? </p>
                  </h2>
                </div>
                <p>
                  Information that we collect or hold can be transferred to the
                  countries:
                </p>
                <ul>
                  <li>Where we have business</li>
                  <li>Where our third-party partners conduct activities</li>
                  <li>Which are linked with Traveloes</li>
                  <li>From where you regularly transmit or receive the data</li>
                </ul>
              </div>
            </Box>
            <Box sx={{ display: "flex", mb: 2 }}>
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="why_choose_Traveloes">
                  <h2>
                    <p>
                      {" "}
                      6. How do we ensure the security of the collected data?{" "}
                    </p>
                  </h2>
                </div>
                <p>
                  We have executed some technical and organizational measures to
                  secure the data collected and prevent illegal access to the
                  data collected and ensure data security. The safeguard
                  measures to ensure the privacy and sensitivity of the
                  information collected and processed. Traveloes follows the
                  industry standards to protect the personal data collected at
                  the time of transmission and once the information is
                  collected.{" "}
                </p>
                <p>
                  We take no liability and responsibility for the disclosure of
                  the information because of any error or transmission or
                  illegal access of the third-parties or cause that are not in
                  our control.{" "}
                </p>
              </div>
            </Box>
            <Box sx={{ display: "flex", mb: 2 }}>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="why_choose_Traveloes">
                  <h2>
                    <p> 2. How do we collect personal data for use?</p>
                  </h2>
                </div>

                <p>
                  <strong>A.Directly</strong>
                </p>

                <p>
                  We can obtain the personal data directly from the users
                  through various modes but not limited to the business cards
                  offered by the user, who have completed our online form for
                  query or any records related to their previous requests. We
                  can also collect information directly while we are developing
                  a business relation or at the time of fulfilling professional
                  service.
                </p>

                <p>
                  <strong>B.Indirectly</strong>
                </p>

                <p>
                  We can also collect data about individuals indirectly via
                  various sources that include travel agents, social media,
                  travel websites, or through an authorized representative. We
                  may also attach the data with our relationship management
                  files to understand you better and offer you the best
                  services.
                </p>
              </div>
            </Box>
            <Box sx={{ display: "flex", mb: 2 }}>
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="why_choose_Traveloes">
                  <h2>
                    <p> 7. How long will the personal data be stored? </p>
                  </h2>{" "}
                </div>
                <p>We keep the stored personal data for the longest:</p>
                <ul>
                  <li>
                    To the period that is necessary for keeping the data stored
                  </li>
                  <li>Retention period stated by the law</li>
                  <li>
                    For the period during which investigation or legal action
                    might arise regarding any service
                  </li>
                </ul>
              </div>
            </Box>
            <Box sx={{ display: "flex", mb: 2 }}>
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="why_choose_Traveloes">
                  <h2>
                    <p> 8. Modifications in the privacy policy</p>
                  </h2>{" "}
                </div>
                <p>
                  When we decide to make any modification in the privacy policy,
                  we will publish it online without prior notification. In case,
                  if you don't agree with the policy, it is recommended that you
                  discontinue the use of our website.{" "}
                </p>
                <div className="col-xs-12 col-md-4">
                  <div className="row"></div>
                  {/*row*/}
                </div>
              </div>
            </Box>
          </Box>
        </Card>
      </Container>
    </Page>
  );
};
export default PrivacyPolicy;
