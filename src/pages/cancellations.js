import React from 'react'
import PropTypes from 'prop-types'
import { useEffect, useRef, useState } from 'react'
import { format } from 'date-fns'
import { sentenceCase } from 'change-case'
import { Icon } from '@iconify/react'
import { Link as RouterLink } from 'react-router-dom'
import shareFill from '@iconify/icons-eva/share-fill'
import printerFill from '@iconify/icons-eva/printer-fill'
import downloadFill from '@iconify/icons-eva/download-fill'
import trash2Outline from '@iconify/icons-eva/trash-2-outline'
import moreVerticalFill from '@iconify/icons-eva/more-vertical-fill'
import arrowIosForwardFill from '@iconify/icons-eva/arrow-ios-forward-fill'
// material
import { useTheme } from '@material-ui/core/styles'
import {
  Box,
  Card,
  Menu,
  Stack,
  Table,
  Avatar,
  Button,
  Divider,
  MenuItem,
  TableRow,
  TableBody,
  TableCell,
  TableHead,
  CardHeader,
  Typography,
  TableContainer,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Dialog,
  Slide,
} from '@material-ui/core'
// utils
// import mockData from "../../../utils/mock-data";
//
import Label from '../components/Label'
import Scrollbar from '../components/Scrollbar'
import { MIconButton } from '../components/@material-extend'
import { hostname } from 'src/HostName'
import useAuth from 'src/hooks/useAuth'
import arrowIosForwardOutline from '@iconify/icons-eva/arrow-ios-forward-outline'
import { moment } from 'moment'

// ----------------------------------------------------------------------

// const MOCK_BOOKINGS = [...Array(5)].map((_, index) => ({
//   id: window.userid,
//   CreatedAt: window.createdAt,
//   UpdatedAt: window.updatedAt,
//   Type: window.type,
//   amount: window.amount,
// }));

// ----------------------------------------------------------------------

export default function Cancellations({ walletdata }) {
  const theme = useTheme()

  return (
    <>
      <Card>
        <CardHeader title="Your Booking" sx={{ mb: 3 }} />
        <Scrollbar>
          <TableContainer sx={{ minWidth: 720 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell sx={{ minWidth: 240 }}>Booking ID</TableCell>
                  <TableCell sx={{ minWidth: 160 }}>PNR</TableCell>
                  <TableCell sx={{ minWidth: 120 }}>Booking Status</TableCell>
                  <TableCell sx={{ minWidth: 200 }}>Amount </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableCell>
                  <Typography sx={{ margin: 10 }}>No record found</Typography>
                </TableCell>
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>

        <Divider />

        <Box sx={{ p: 2, textAlign: 'right' }}>
          <Button
            to="#"
            size="small"
            color="inherit"
            component={RouterLink}
            endIcon={<Icon icon={arrowIosForwardFill} />}
          >
            View All
          </Button>
        </Box>
      </Card>
    </>
  )
}
