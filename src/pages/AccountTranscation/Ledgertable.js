import React from 'react'
import { filter } from 'lodash'
import { Icon } from '@iconify/react'
import { sentenceCase } from 'change-case'
import { useState, useEffect } from 'react'
import plusFill from '@iconify/icons-eva/arrow-downward-outline'
import { Link as RouterLink } from 'react-router-dom'
// material
import { useTheme } from '@material-ui/core/styles'
import {
  Card,
  Table,
  Stack,
  Avatar,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Tooltip,
  IconButton,
  TextField,
  CardHeader,
  Divider,
} from '@material-ui/core'
// redux
import { useDispatch, useSelector } from '../../redux/store'
import { getUserList, deleteUser } from '../../redux/slices/user'
// routes
import { PATH_DASHBOARD } from '../../routes/paths'
// hooks
import useSettings from '../../hooks/useSettings'
// components
import Page from '../../components/Page'
import Label from '../../components/Label'
import Scrollbar from '../../components/Scrollbar'
import SearchNotFound from '../../components/SearchNotFound'
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs'
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user/list'
import moment from 'moment'
import { DatePicker, LocalizationProvider } from '@material-ui/lab'
import AdapterDateFns from '@mui/lab/AdapterDateFns'
import { useLocation } from 'react-router'
import { hostname } from 'src/HostName'
import useAuth from 'src/hooks/useAuth'
import ReactHTMLTableToExcel from 'react-html-table-to-excel'
// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'Created On', label: 'Created On', alignRight: false },
  { id: 'Payment Source', label: 'Payment Source', alignRight: false },
  { id: 'Credit', label: 'Credit', alignRight: false },
  { id: 'Debit', label: 'Debit', alignRight: false },
  { id: 'Amount(Rs)', label: 'Amount(Rs)', alignRight: false },
  { id: 'Current Balance', label: 'Current Balance', alignRight: false },
]

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

export default function LedgerTable(props) {
  const { themeStretch } = useSettings()
  const theme = useTheme()
  const dispatch = useDispatch()
  const { userList } = useSelector((state) => state.user)
  const [page, setPage] = useState(0)
  const [order, setOrder] = useState('asc')
  const [selected, setSelected] = useState([])
  const [orderBy, setOrderBy] = useState('name')
  const [filterName, setFilterName] = useState('')
  const [filteredName, setFilteredName] = useState('')
  const [rowsPerPage, setRowsPerPage] = useState(5)
  const [ruleData, setRuleData] = useState([])
  const [isloading, setIsloading] = useState(true)
  const [value, setValue] = React.useState(null)
  const [secondValue, setSecondValue] = React.useState(null)
  useEffect(() => {
    dispatch(getUserList())
  }, [dispatch])

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = ruleData.data.map((n) => n.currentBalance)
      setSelected(newSelecteds)
      return
    }
    setSelected([])
  }

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name)
    let newSelected = []
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      )
    }
    setSelected(newSelected)
  }
  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const handleFilterByName = (event) => {
    setFilterName(event.target.value)
  }

  const handleDeleteUser = (userId) => {
    dispatch(deleteUser(userId))
  }

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - ruleData.length) : 0

  const isUserNotFound = ruleData.length === 0
  const dateFormat = moment(props.FirstDate).format('YYYY/MM/DD')
  const Seconddate = moment(props.SecondDate).format('YYYY/MM/DD')
  const first = moment(value).format('YYYY-MM-DD')
  const second = moment(secondValue).format('YYYY-MM-DD')
  const { user } = useAuth()
  const { state } = useLocation()
  const GetAllTransaction = () => {
    var myHeaders = new Headers()
    myHeaders.append('Content-Type', 'application/json')
    var raw = JSON.stringify({
      userid: state.userid,
      formDate: first,
      toDate: second,
    })

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    }

    fetch(`${hostname}all/agent/transactions`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result) {
  
          setRuleData(result)
          setIsloading(false)
          setFilteredName(result)
        }
      })
      .catch((error) => console.log('error'))
  }
  // useEffect(() => GetAllRuleMaded(), []);
  const date = Math.floor(Date.now() / 100)

  return (
    <Page title="User List | Traveloes">
      <>
        <HeaderBreadcrumbs
          heading="User List"
          links={[
            { name: 'Dashboard', href: PATH_DASHBOARD.root },
            { name: 'User', href: PATH_DASHBOARD.user.root },
          ]}
          action={
            <Button variant="contained" startIcon={<Icon icon={plusFill} />}>
              <ReactHTMLTableToExcel
                id="test-table-xls-button"
                className="download-table-xls-button"
                table="table-to-xls"
                filename={date}
                sheet="tablexls"
                buttonText="Download as XLS"
              />
            </Button>
          }
        />
        <Card sx={{ p: 2, my: 2 }}>
          <Stack
            direction="row"
            sx={{ alignItems: 'center', justifyContent: 'space-around' }}
          >
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label="From Date"
                value={value}
                onChange={(newValue) => {
                  setValue(newValue)
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>

            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label="To Date"
                value={secondValue}
                onChange={(newSecondValue) => {
                  setSecondValue(newSecondValue)
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
            {secondValue ? (
              <Button
                onClick={() => GetAllTransaction()}
                variant="contained"
                sx={{ width: '10rem', height: '3rem' }}
              >
                Check
              </Button>
            ) : (
              <Button
                onClick={() => alert('Please enter The dates')}
                variant="contained"
                sx={{ width: '10rem', height: '3rem' }}
              >
                Check
              </Button>
            )}
          </Stack>
        </Card>
        {ruleData.status === true ? (
          <Card>
            <CardHeader title="All Transactions" />
            <Divider sx={{ margin: 1, color: 'grey' }} />
            <Scrollbar>
              <TableContainer>
                <Table id="table-to-xls">
                  <UserListHead
                    order={order}
                    orderBy={orderBy}
                    headLabel={TABLE_HEAD}
                    rowCount={ruleData.data.length}
                    onRequestSort={handleRequestSort}
                    onSelectAllClick={handleSelectAllClick}
                  />
                  <TableBody>
                    {ruleData.data
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage,
                      )
                      .map((row, index) => {
                        const {
                          id,
                          name,
                          role,
                          status,
                          company,
                          avatarUrl,
                          isVerified,
                          userid,
                          createdAt,
                          type,
                          currentBalance,
                          amount,
                          paymentSource,
                        } = row
                        const isItemSelected = selected.indexOf(name) !== -1

                        return (
                          <TableRow hover key={index} tabIndex={-1}>
                            <TableCell align="left">{createdAt}</TableCell>
                            <TableCell align="left">{paymentSource}</TableCell>
                            <TableCell align="left">
                              <Label sx={{ textTransform: 'uppercase' }}>
                                {type === 'cr' ? 'cr' : null}
                              </Label>
                            </TableCell>
                            <TableCell align="left">
                              <Label sx={{ textTransform: 'uppercase' }}>
                                {type === 'dr' ? 'dr' : null}
                              </Label>
                            </TableCell>
                            <TableCell align="left">{amount}</TableCell>
                            <TableCell align="left">{currentBalance}</TableCell>
                          </TableRow>
                        )
                      })}
                    {emptyRows > 0 && (
                      <TableRow style={{ height: 53 * emptyRows }}>
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                  </TableBody>
                  {isUserNotFound && (
                    <TableBody>
                      <TableRow>
                        <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                          <SearchNotFound searchQuery={filterName} />
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  )}
                </Table>
              </TableContainer>
            </Scrollbar>
            <Stack direction="row-reverse" sx={{ marginRight: 10 }}></Stack>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={ruleData.data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Card>
        ) : (
          <h6>{ruleData.message}</h6>
        )}
      </>
    </Page>
  )
}
