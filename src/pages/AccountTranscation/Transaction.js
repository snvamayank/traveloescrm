import * as React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import {
  DatePicker,
  LocalizationProvider,
  LoadingButton,
} from "@material-ui/lab";
import { Button, Stack } from "@mui/material/";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import { TextField } from "@material-ui/core";
import LedgerTable from "./Ledgertable";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export default function Transaction() {
  const [value, setValue] = React.useState([null]);
  const [secondValue, setSecondValue] = React.useState([null]);
  return (
    <>
      <LedgerTable FirstDate={value} SecondDate={secondValue} />
    </>
  );
}
