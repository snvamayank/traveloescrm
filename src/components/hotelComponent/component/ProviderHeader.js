import { Button, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/styles";
import React from "react";
import MyAvatar from "src/components/MyAvatar";
import AddIcon from "@mui/icons-material/Add";

const ProviderHeader = ({ data, handleAddRule }) => {
  const theme = useTheme();
  const buttonBorder = {
    borderRadius: 100,
    border: "1px solid ",
    borderColor: theme.palette.primary.light,
  };
  return (
    <div
      className="d-flex align-items-center mb-1 pb-1 justify-content-between"
      style={{
        borderBottom: "1px solid rgb(241 241 241)",
        borderColor: theme.palette.primary.light,
      }}
    >
      <div className="d-flex align-items-center">
        <div className="mr-3 d-flex align-items-center">
          <MyAvatar />
        </div>
        <div className="d-flex  justify-content-center flex-column mx-1">
          <Typography
            sx={{
              color: "primary.main",
              letterSpacing: "1px",
            }}
            className="font-bolder text-uppercase"
            variant="h6"
          >
            {data.name}
          </Typography>
          <Typography variant="body2" className="text-gray-700 font-bold mb-1">
            Provider{" "}
          </Typography>
        </div>
      </div>
      <div
        className="text-left me-5"
        onClick={() => handleAddRule(data.gdsType)}
      >
        <Button style={buttonBorder}>
          Add <AddIcon />
        </Button>
      </div>
    </div>
  );
};

export default ProviderHeader;
