import {
  Dialog,
  DialogContent,
  DialogTitle,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import moment from "moment";
import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import PeopleIcon from "@mui/icons-material/People";
import HotelIcon from "@mui/icons-material/Hotel";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoneyBill } from "@fortawesome/free-solid-svg-icons";

const HotelsDetails = [
  { id: 1, name: "HotelName" },
  { id: 2, name: "Location" },
  { id: 3, name: "CheckIn" },
  { id: 4, name: "CheckOut" },
  { id: 5, name: "Rooms" },
  { id: 6, name: "Nights" },
];

const BookingDetails = [
  { id: 1, name: "Booking Id" },
  { id: 2, name: "Booking Status" },
  { id: 3, name: "Email Id" },
  { id: 4, name: "Phone " },
  { id: 5, name: "Country" },
];

const fareDetails = [
  { id: 1, name: "AverageBaseFare" },
  { id: 2, name: "BaseFare" },
  { id: 3, name: "TotalMarkup" },
  { id: 4, name: "AverageTax" },
  { id: 5, name: "TotalTax" },
  { id: 6, name: "GrandTotal" },
  { id: 7, name: "CurrencyCode" },
];

const PassangerDetails = [
  { id: 1, name: "FirstName" },
  { id: 2, name: "MiddleName" },
  { id: 3, name: "LastName" },
  { id: 4, name: "Gender" },
  { id: 5, name: "DOB" },
];

const SingleBookingDetails = ({ open, handleClose, apiresult }) => {

  return (
    <div>
      <Dialog open={open} onClose={handleClose} fullScreen>
        <DialogTitle className="d-flex justify-content-between align-items-center pb-4">
          <h3>Booking History</h3>
          <div>
            <CloseIcon onClick={() => handleClose()} />
          </div>
        </DialogTitle>
        <DialogContent>
          {apiresult.loading ? (
            "loading"
          ) : (
            <div>
              <div>
                <div className="m-2 d-flex align-items-center">
                  <HotelIcon style={{ color: "326fa8" }} />
                  <h5 className="mb-0 ml-2 text-black"> Booking Details</h5>
                </div>
                <TableContainer className="tableairp-responsive booking">
                  <Table className="table-airp">
                    <TableHead>
                      <TableRow>
                        {BookingDetails.map((item, i) => (
                          <TableCell key={item.id}>{item.name}</TableCell>
                        ))}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell>
                          {apiresult.data?.baseResponse.bookingDetail.bookingId}
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .bookingDetails[0].bookingStatus
                          }
                        </TableCell>
                        <TableCell>
                          {apiresult.data?.baseResponse.bookingDetail.emailId}
                        </TableCell>
                        <TableCell>
                          {apiresult.data?.baseResponse.bookingDetail.mobileNo}
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .hotelDetail.address.Country
                          }
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
              <div>
                <div className="m-2 d-flex align-items-center">
                  <HotelIcon style={{ color: "326fa8" }} />
                  <h5 className="mb-0 ml-2 text-black"> Hotels Details</h5>
                </div>

                <TableContainer className="tableairp-responsive booking">
                  <Table className="table-airp">
                    <TableHead>
                      <TableRow>
                        {HotelsDetails.map((item, i) => (
                          <TableCell key={item.id}>{item.name}</TableCell>
                        ))}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .hotelDetail.hotelName
                          }
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .hotelDetail.address.City
                          }
                        </TableCell>
                        <TableCell>
                          {moment(
                            apiresult.data?.baseResponse.bookingDetail.checkIn
                          ).format("DD MMM YYYY")}
                        </TableCell>
                        <TableCell>
                          {moment(
                            apiresult.data?.baseResponse.bookingDetail.checkOut
                          ).format("DD MMM YYYY")}
                        </TableCell>
                        <TableCell>
                          {apiresult.data?.baseResponse.bookingDetail.rooms}
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .totalNight
                          }
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
              <div>
                <div className="m-2 d-flex align-items-center">
                  <PeopleIcon style={{ color: "326fa8" }} />
                  <h5 className="mb-0 ml-2 text-black"> Passanger Details</h5>
                </div>
                <TableContainer className="tableairp-responsive booking">
                  <Table className="table-airp">
                    <TableHead>
                      <TableRow>
                        {PassangerDetails.map((item, i) => (
                          <TableCell key={item.id}>{item.name}</TableCell>
                        ))}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {apiresult.data?.baseResponse.bookingDetail.paxDetails.map(
                        (item, index) => {
                          return (
                            <TableRow key={index}>
                              <TableCell>{item.firstName}</TableCell>
                              <TableCell>{item.middleName}</TableCell>
                              <TableCell>{item.lastName}</TableCell>
                              <TableCell>{item.gender}</TableCell>
                              <TableCell>
                                {moment(item.dob).format("MMM DD YYYY")}
                              </TableCell>
                            </TableRow>
                          );
                        }
                      )}
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
              <div>
                <div className="m-2 d-flex align-items-center">
                  <FontAwesomeIcon
                    icon={faMoneyBill}
                    style={{ color: "326fa8" }}
                  />
                  <h5 className="mb-0 ml-2 text-black">Fare Details</h5>
                </div>

                <TableContainer className="tableairp-responsive booking">
                  <Table className="table-airp">
                    <TableHead>
                      <TableRow>
                        {fareDetails.map((item, i) => (
                          <TableCell key={item.id}>{item.name}</TableCell>
                        ))}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .bookingAmount.averageBaseFare
                          }
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .bookingAmount.baseFare
                          }
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .bookingAmount.totalMarkup
                          }
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .bookingAmount.averageTax
                          }
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .bookingAmount.totalTax
                          }
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .bookingAmount.grandTotal
                          }
                        </TableCell>
                        <TableCell>
                          {
                            apiresult.data?.baseResponse.bookingDetail
                              .bookingAmount.currencyCode
                          }
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
            </div>
          )}
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default SingleBookingDetails;
