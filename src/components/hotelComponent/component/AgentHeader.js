import React from "react";
import {Card, CardHeader, Switch, styled } from "@material-ui/core";

const DIV = styled(Card)(({ theme }) => ({
  boxShadow: "none",
  textAlign: "center",
  backgroundColor: theme.palette.primary.lighter,
  [theme.breakpoints.up("md")]: {
    textAlign: "left",
    display: "flex",
    alignItems: "center",
    paddingLeft: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },
}));
const AgentHeader = ({state}) => {
  return (
    <DIV
      style={{ marginBottom: 15, overflow: "hidden" }}
      className="d-flex justify-content-between"
    >
      <CardHeader
        title={state}
        sx={{ mb: 3 }}
        titleTypographyProps={{ variant: "h4" }}
      />
      <Switch
        // checked={active}
        // onChange={handledisableprovider}
        inputProps={{ "aria-label": "controlled" }}
        className="me-5"
      />
    </DIV>
  );
};

export default AgentHeader;
