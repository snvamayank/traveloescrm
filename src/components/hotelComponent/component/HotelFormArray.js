export const AddHotelForm = (hotelRule) => [
  {
    name: "ruleName",
    id: "ruleName",
    value: hotelRule.ruleName,
    label: "Rule Name",
    variant: "filled",
    type: "text",
  },
  {
    name: "rooms",
    id: "rooms",
    value: hotelRule.rooms,
    label: "rooms",
    variant: "filled",
    type: "text",
  },
  {
    name: "nights",
    id: "nights",
    value: hotelRule.nights,
    label: "nights",
    variant: "filled",
    type: "text",
  },
  {
    name: "pax",
    id: "pax",
    value: hotelRule.pax,
    label: "pax",
    variant: "filled",
    type: "text",
  },
];
