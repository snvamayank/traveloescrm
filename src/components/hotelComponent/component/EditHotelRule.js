import { Box, Button, Grid, TextField } from "@material-ui/core";
import moment from "moment";
import React from "react";
import { ruleUpdateApi } from "src/Api/Hotel/hotelRuleApi";
import { useSnackbar } from "notistack5";

const EditHotelRule = ({
  updateRule,
  userid,
  gdsType,
  setUpdateRule,
  closeButton,
}) => {
  const { enqueueSnackbar } = useSnackbar();

  const handleEdithotelRule = (e) => {
    const { name, value } = e.target;
    setUpdateRule({ ...updateRule, [name]: value });
  };

  const sumbitUpdate = async (updateRule, userid, gdsType) => {
    const result = await ruleUpdateApi(updateRule, userid, gdsType);
    if (result === "Error") {
      closeButton();
      enqueueSnackbar("Please try again", { variant: "error" });
    }
    if (result?.baseResponse?.status === true) {
      closeButton();
      enqueueSnackbar("Hotel Rule Update Successfully", { variant: "success" });
    }
    if (result?.baseResponse?.status === false) {
      closeButton();
      enqueueSnackbar("Farerule Not Update", { variant: "error" });
    }
  };

  return (
    <div>
      <Box className="mb-3">
        <Grid container spacing={2}>
          <Grid item xs={12} xl={6} md={12}>
            <TextField
              name="ruleName"
              id="ruleName"
              value={updateRule.ruleName}
              type="text"
              label="ruleName"
              InputLabelProps={{ shrink: true }}
              variant="filled"
              disabled="true"
              className="w-full"
              onChange={(e) => handleEdithotelRule(e)}
            />
          </Grid>
          <Grid item xs={12} xl={6} md={12}>
            <TextField
              name="fromDate"
              id="fromDate"
              value={moment(updateRule.fromDate).format("YYYY-MM-DD")}
              type="date"
              label="CheckinDate"
              InputLabelProps={{ shrink: true }}
              variant="filled"
              className="w-full"
              onChange={(e) => handleEdithotelRule(e)}
            />
          </Grid>
        </Grid>
      </Box>
      <Box className="mb-3">
        <Grid container spacing={2}>
          <Grid item xs={12} xl={6} md={12}>
            <TextField
              name="toDate"
              id="toDate"
              value={moment(updateRule.toDate).format("YYYY-MM-DD")}
              type="date"
              label="CheckOutDate"
              InputLabelProps={{ shrink: true }}
              variant="filled"
              className="w-full"
              onChange={(e) => handleEdithotelRule(e)}
            />
          </Grid>
          <Grid item xs={12} xl={6} md={12}>
            <TextField
              name="nights"
              id="nights"
              value={updateRule.nights}
              type="text"
              label="Nights"
              InputLabelProps={{ shrink: true }}
              variant="filled"
              className="w-full"
              onChange={(e) => handleEdithotelRule(e)}
            />
          </Grid>
        </Grid>
      </Box>
      <Box className="mb-3">
        <Grid container spacing={2}>
          <Grid item xs={12} xl={6} md={12}>
            <TextField
              name="pax"
              id="pax"
              value={updateRule.pax}
              type="text"
              label="pax"
              InputLabelProps={{ shrink: true }}
              variant="filled"
              className="w-full"
              onChange={(e) => handleEdithotelRule(e)}
            />
          </Grid>
          <Grid item xs={12} xl={6} md={12}>
            <TextField
              name="rooms"
              id="rooms"
              value={updateRule.rooms}
              type="text"
              label="rooms"
              InputLabelProps={{ shrink: true }}
              variant="filled"
              className="w-full"
              onChange={(e) => handleEdithotelRule(e)}
            />
          </Grid>
        </Grid>
      </Box>
      <Box className="mb-3">
        <Grid container spacing={2}>
          <Grid
            item
            xs={12}
            xl={6}
            md={12}
            className="d-flex align-items-center"
          >
            <div className="d-flex align-items-center">
              <label className="mr-3" style={{ fontSize: "13px" }}>
                Markup Type
              </label>
              <div className="d-flex justify-content-center align-items-center">
                <div className="mr-4">
                  <input
                    name="markupType"
                    type="radio"
                    value={"%"}
                    checked={updateRule.markupType === "%"}
                    id="markupType"
                    onChange={(e) => handleEdithotelRule(e)}
                    className="mr-2 cursor-pointer"
                  />
                  <label>%</label>
                </div>
                <div>
                  <input
                    name="markupType"
                    type="radio"
                    checked={updateRule.markupType === "FLAT"}
                    value={"FLAT"}
                    id="markupType"
                    onChange={(e) => handleEdithotelRule(e)}
                    className="mr-2 cursor-pointer"
                  />
                  <label style={{ fontSize: "13px" }}>FLAT</label>
                </div>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} xl={6} md={12}>
            <TextField
              name="markupAmount"
              id="markupAmount"
              value={updateRule.markupAmount}
              type="text"
              label="markupAmount"
              InputLabelProps={{ shrink: true }}
              variant="filled"
              className="w-full"
              onChange={(e) => handleEdithotelRule(e)}
            />
          </Grid>
        </Grid>
      </Box>

      <div className="text-center">
        <Button
          variant="contained"
          onClick={() => {
            sumbitUpdate(updateRule, userid, gdsType);
          }}
          sx={{
            marginBottom: "20px",
            marginTop: 2,
            height: "45px",
            fontSize: "16px",
            width: "140px",
          }}
        >
          sumbit
        </Button>
      </div>
    </div>
  );
};

export default EditHotelRule;
