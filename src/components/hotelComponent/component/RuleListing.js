import React from "react";
import moment from "moment";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import EditHotelRule from "./EditHotelRule";
import { useState } from "react";
import { ruleDeleteAPi } from "src/Api/Hotel/hotelRuleApi";
import { useSnackbar } from "notistack5";
import CloseIcon from "@mui/icons-material/Close";

const ruleHeader = [
  {
    id: "1",
    name: "RuleName",
  },
  {
    id: "2",
    name: "CheckinDate",
  },
  {
    id: "3",
    name: "CheckoutDate",
  },
  {
    id: "4",
    name: "Rooms",
  },
  {
    id: "5",
    name: "Nights",
  },
  {
    id: "6",
    name: "Pax",
  },
  {
    id: "7",
    name: "MarkupType",
  },
  {
    id: "8",
    name: "MarkupAmount",
  },
  {
    id: "9",
    name: "Action",
  },
];

const ActionStyle = {
  color: "#00ab55",
};
const RuleListing = ({ list, userid, gdsType }) => {
  const { enqueueSnackbar } = useSnackbar();

  const [updateRule, setUpdateRule] = useState({});
  const [open, setOpen] = useState(false);

  const deletehotelMarkup = async (deleteRule) => {
    const result = await ruleDeleteAPi(deleteRule, userid, gdsType);
    if (result === "Error") {
      enqueueSnackbar("Please try again", { variant: "error" });
    }
    if (result?.baseResponse?.status === true) {
      enqueueSnackbar("Hotel Rule Delete Successfully", { variant: "success" });
    }
    if (result?.baseResponse?.status === false) {
      enqueueSnackbar("Farerule Not Delete", { variant: "error" });
    }
  };

  const closeButton = () => {
    setOpen(false);
  };

  const updateHotelMarkup = (updateRule) => {
    setOpen(!open);
    setUpdateRule(updateRule);
  };

  return (
    <div>
      <TableContainer className="tableairp-responsive">
        <Table className="table-airp">
          <TableHead>
            <TableRow>
              {ruleHeader.map((item, index) => (
                <TableCell key={index}>{item.name}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {list.map((rule, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>{rule.ruleName}</TableCell>
                  <TableCell>
                    {moment(rule.fromDate).format("DD MMM, YYYY")}
                  </TableCell>
                  <TableCell>
                    {moment(rule.toDate).format("DD MMM, YYYY")}
                  </TableCell>
                  <TableCell>{rule.rooms}</TableCell>
                  <TableCell>{rule.nights}</TableCell>
                  <TableCell>{rule.pax}</TableCell>
                  <TableCell>{rule.markupType}</TableCell>
                  <TableCell>{rule.markupAmount}</TableCell>
                  <TableCell className="d-flex">
                    <div className="border border-danger rounded p-1 me-2">
                      <button onClick={() => deletehotelMarkup(rule)}>
                        <DeleteIcon style={{ color: "red" }} />
                      </button>
                    </div>

                    <div className="border border-success rounded p-1">
                      <button onClick={() => updateHotelMarkup(rule)}>
                        <EditIcon style={ActionStyle} />
                      </button>
                    </div>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog onClose={() => closeButton()} open={open}>
        <DialogTitle>
          <div className="d-flex align-items-center justify-content-between">
            <h4>Edit Hotel Rule</h4>
            <button onClick={() => closeButton()}>
              <CloseIcon />
            </button>
          </div>
          <Divider />
        </DialogTitle>
        <DialogContent className="mt-3">
          <EditHotelRule
            updateRule={updateRule}
            userid={userid}
            gdsType={gdsType}
            setUpdateRule={setUpdateRule}
            closeButton={closeButton}
          />
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default RuleListing;
