import {
  Box,
  Button,
  Grid,
  Stack,
  TextField,
  Dialog,
  DialogContent,
  DialogTitle,
} from "@material-ui/core";
import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import Divider from "@mui/material/Divider";
import { AddHotelForm } from "./HotelFormArray";
import { addHotelRule } from "src/Api/Hotel/hotelRuleApi";
import { useSnackbar } from "notistack5";    
const AddHotelRule = ({
  handlehotelForm,
  hotelRule,
  userid,
  closeButton,
  gdsType,
  open,
}) => {
  const { enqueueSnackbar } = useSnackbar();
  
  const sumbitRule = async () => {
    if (hotelRule.ruleName.length < 3) {
      return enqueueSnackbar("Please Enter RuleName", { variant: "error" });
    }

    const result = await addHotelRule(hotelRule, userid, gdsType);
    if (result === "Error") {
      closeButton();
      enqueueSnackbar("Please try again", { variant: "error" });
    }
    if (result?.baseResponse?.status === true) {
      closeButton();
      enqueueSnackbar("Hotel Rule Add Successfully", { variant: "success" });
    }
    if (result?.baseResponse?.status === false) {
      closeButton();
      enqueueSnackbar(
        "This Rule Is Already Exist, Kindly Change The Rule And RuleName...",
        { variant: "error" }
      );
    }
  };

  return (
    <Dialog onClose={() => closeButton()} open={open}>
      <DialogTitle>
        <div className="d-flex align-items-center justify-content-between">
          <h4>Add Hotel Rule</h4>
          <button onClick={() => closeButton()}>
            <CloseIcon />
          </button>
        </div>
        <Divider />
      </DialogTitle>
      <DialogContent className="mt-3">
        <Stack>
          <div>
            <Box>
              <Grid container spacing={2}>
                {AddHotelForm(hotelRule).map((item, ind) => {
                  return (
                    <Grid item xs={12} xl={6} md={12}>
                      <TextField
                        name={item.name}
                        id={item.id}
                        value={item.value}
                        type={item.type}
                        label={item.label}
                        InputLabelProps={{ shrink: true }}
                        variant={item.variant}
                        onChange={(e) => handlehotelForm(e)}
                        className="w-full"
                      />
                    </Grid>
                  );
                })}
              </Grid>
            </Box>
            <Box className="my-3">
              <Grid container spacing={2}>
                <Grid
                  item
                  xs={12}
                  xl={6}
                  md={12}
                  className="d-flex align-items-center"
                >
                  <div className="d-flex align-items-center">
                    <label className="mr-3" style={{ fontSize: "13px" }}>
                      Markup Type
                    </label>
                    <div className="d-flex justify-content-center align-items-center">
                      <div className="mr-4">
                        <input
                          name="markupType"
                          type="radio"
                          value={"%"}
                          checked={hotelRule.markupType === "%"}
                          id="markupType"
                          onChange={(e) => handlehotelForm(e)}
                          className="mr-2 cursor-pointer"
                        />
                        <label>%</label>
                      </div>
                      <div>
                        <input
                          name="markupType"
                          type="radio"
                          checked={hotelRule.markupType === "FLAT"}
                          value={"FLAT"}
                          id="markupType"
                          onChange={(e) => handlehotelForm(e)}
                          className="mr-2 cursor-pointer"
                        />
                        <label style={{ fontSize: "13px" }}>FLAT</label>
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={12} xl={6} md={12}>
                  <TextField
                    name="markupAmount"
                    id="markupAmount"
                    value={hotelRule.markupAmount}
                    label="markupAmount"
                    variant="filled"
                    type="text"
                    InputLabelProps={{ shrink: true }}
                    className="w-full"
                    onChange={(e) => handlehotelForm(e)}
                  />
                </Grid>
              </Grid>
            </Box>
            <Box>
              <Grid container spacing={2}>
                <Grid item xs={12} xl={6} md={12}>
                  <TextField
                    name="fromDate"
                    id="fromDate"
                    value={hotelRule.fromDate}
                    type="date"
                    label="CheckinDate"
                    InputLabelProps={{ shrink: true }}
                    variant="filled"
                    className="w-full"
                    onChange={(e) => handlehotelForm(e)}
                  />
                </Grid>
                <Grid item xs={12} xl={6} md={12}>
                  <TextField
                    name="toDate"
                    id="toDate"
                    value={hotelRule.toDate}
                    type="date"
                    label="CheckoutDate"
                    InputLabelProps={{ shrink: true }}
                    variant="filled"
                    className="w-full"
                    onChange={(e) => handlehotelForm(e)}
                  />
                </Grid>
              </Grid>
            </Box>
          </div>
          <div className="text-center">
            <Button
              variant="contained"
              onClick={() => sumbitRule()}
              sx={{
                marginBottom: "20px",
                marginTop: 2,
                height: "45px",
                fontSize: "16px",
                width: "140px",
              }}
            >
              Sumbit
            </Button>
          </div>
        </Stack>
      </DialogContent>
    </Dialog>
  );
};

export default AddHotelRule;
