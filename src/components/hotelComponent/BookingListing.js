import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import moment from "moment";
import React, { useState } from "react";
import SingleBookingDetails from "./component/SingleBookingDetails";
import { userFullBookingHistory } from "src/Api/Hotel/ApiUrl";
import { useFullBookingHistory } from "src/Api/Hotel/usecustomApi";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
const list = [
  { id: 0, name: "BookingId" },
  { id: 1, name: "BookingStatus" },
  { id: 2, name: "GdsType" },
  { id: 4, name: "Provider" },
  { id: 4, name: "BookingAmount" },
  { id: 5, name: "CheckIn" },
  { id: 6, name: "CheckOut" },
  { id: 7, name: "Pax" },
  { id: 8, name: "TotalNight" },
  { id: 9, name: "Room" },
];

const optionList = [15, 30, 45, 60];

const BookingListing = ({ data }) => {
  const { open, setOpen, apiresult, handleOpen } = useFullBookingHistory(
    userFullBookingHistory
  );
  const [page, setPage] = useState(1);
  const [selectpage, setselectPage] = useState(15);
  const handleClose = () => setOpen(false);
  return (
    <div>
      <TableContainer className="tableairp-responsiveBooking ">
        <Table className="table-airp">
          <TableHead>
            <TableRow>
              {list.map((item, index) => (
                <TableCell key={index}>{item.name}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data?.baseResponse?.hotelBooking
              ?.slice(page * selectpage - selectpage, page * selectpage)
              .map((item, index) => {
                return (
                  <TableRow key={index}>
                    <TableCell
                      onClick={() => handleOpen(item.id)}
                      style={{ color: "green", cursor: "pointer" }}
                    >
                      {item.bookingId}
                    </TableCell>
                    <TableCell>{item.bookingStatus}</TableCell>
                    <TableCell>{item.gdsType}</TableCell>
                    <TableCell>{item.key}</TableCell>
                    <TableCell className="d-flex align-items-center">
                      {item.bookingAmount?.grandTotal ? (
                        <>
                          {item.bookingAmount?.currencyCode === "INR"
                            ? "₹"
                            : item.bookingAmount?.currencyCode === "USD"
                            ? "$"
                            : null}
                        </>
                      ) : null}
                      &nbsp;
                      <h6 className="mb-0">{item.bookingAmount?.grandTotal}</h6>
                    </TableCell>
                    <TableCell>
                      {moment(item.checkIn).format("DD MMM , YYYY")}
                    </TableCell>
                    <TableCell>
                      {moment(item.checkOut).format("DD MMM , YYYY")}
                    </TableCell>
                    <TableCell>{item.paxDetails.length}</TableCell>
                    <TableCell>{item.totalNight}</TableCell>
                    <TableCell>{item.rooms}</TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
        {data?.baseResponse?.hotelBooking.length > 15 ? (
          <div className="d-flex justify-content-end align-items-center">
            <div>{`Total Booking ${data?.baseResponse?.hotelBooking.length}`}</div>
            <div className="d-flex flex-row-reverse m-3">
              <ChevronRightIcon
                onClick={() =>
                  page < data?.baseResponse?.hotelBooking.length / selectpage
                    ? setPage((pre) => pre + 1)
                    : null
                }
                className="cursor-pointer"
              />
              {page} -&nbsp;
              {Math.ceil(
                data?.baseResponse?.hotelBooking.length / selectpage
              )}{" "}
              Page
              <KeyboardArrowLeftIcon
                onClick={() => (page > 1 ? setPage((pre) => pre - 1) : null)}
                className="cursor-pointer"
              />
              <select
                name="selectpage"
                id="selectpage"
                value={selectpage}
                onChange={(e) => setselectPage(e.target.value)}
                className="border border-2 rounded ml-2"
              >
                {optionList.map((value, i) => (
                  <option value={value}>{value}</option>
                ))}
              </select>
              Row Per Page
            </div>
          </div>
        ) : null}
      </TableContainer>
      <SingleBookingDetails
        open={open}
        handleClose={handleClose}
        apiresult={apiresult}
      />
    </div>
  );
};

export default BookingListing;
