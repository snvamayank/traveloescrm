import React, { useState } from "react";
import AddHotelRule from "./component/AddHotelRule";
import RuleListing from "./component/RuleListing";
import moment from "moment";
import ProviderHeader from "./component/ProviderHeader";

const HotelAgent = ({ data, userid }) => {
  const [open, setOpen] = useState(false);
  const [gdsType, setGdstype] = useState();
  const [hotelRule, setHotelRule] = useState({
    ruleName: "",
    fromDate: moment().format("YYYY-MM-DD"),
    toDate: moment(Date.now() + 2 * 24 * 60 * 60 * 1000).format("YYYY-MM-DD"),
    rooms: 0,
    nights: 0,
    pax: 0,
    markupType: "FLAT",
    markupAmount: 0,
  });
  const handleAddRule = (gdsType) => {
    setOpen(!open);
    setGdstype(gdsType);
  };
  const closeButton = () => {
    setOpen(false);
  };
  const handlehotelForm = (e) => {
    const { value, name } = e.target;
    setHotelRule({ ...hotelRule, [name]: value });
  };

  return (
    <div>
      {data.map((item, ind) => (
        <div key={ind} className="d-flex flex-column m-4 border p-3 rounded-3">
          <ProviderHeader data={item} handleAddRule={handleAddRule} />
          <RuleListing
            list={item.hotelMarkup}
            userid={userid}
            gdsType={item.gdsType}
          />
        </div>
      ))}
      <AddHotelRule
        handlehotelForm={handlehotelForm}
        hotelRule={hotelRule}
        userid={userid}
        gdsType={gdsType}
        closeButton={closeButton}
        open={open}
      />
    </div>
  );
};

export default HotelAgent;
