import React from "react";
import { Button } from "@material-ui/core";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlane } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

const OneWayFlightCard = ({
  AirResult,
  handleShow,
  convertFrom24To12Format,
  ConvertMinsToTime,
}) => {
  return (
    <div>
      {AirResult.map((item, index) => {
        const outBoundCount = item.outBound.length;

        const fromAirport = item.outBound[0].fromAirport;
        const toAirport = item.outBound[outBoundCount - 1].toAirport;
        const price = item.fare.grandTotal;

        return (
          <div
            className="grid lg:grid-cols-6 grid-cols-1 px-10 py-3 mt-3 shadow rounded-2xl"
            key={index}
            style={{ marginLeft: "60px" }}
          >
            <div>
              <div>
                <div className="pl-4">
                  <img
                    src={`https://www.travomint.com/resources/images/airline-logo/${item.outBound[0].airline}.png`}
                    className=" rounded-3 "
                    height="50px"
                    width="50px"
                    alt="airline"
                  />
                </div>

                <div className="pl-4  col-span-2">
                  <span className="fs-6 text-gray-500 font-sans font-bold ">
                    {" "}
                    {item.outBound[0].airline}-{item.outBound[0].flightNo}
                  </span>
                </div>
              </div>
              <button
                onClick={(e) => handleShow(item)}
                className="down border-gray-500 border-2 font-bold pt-1 pb-1 pl-3 text-sm pr-3 text-gray-600 mt-2 rounded-lg"
              >
                <i className="far fa-hand-point-right"></i> Flight Details
              </button>
            </div>
            <div className="col-span-4 lg:pl-5 lg:pr-5 pl-0 pr-0">
              <div className="grid lg:grid-cols-4 grid-cols-1">
                <div className="text-center py-3">
                  <span className="text-black font-bold">
                    <div className="text-gray-600">{fromAirport}</div>
                    <div>
                      {convertFrom24To12Format(
                        item.outBound[outBoundCount - 1].depDate
                      )}
                    </div>
                    <div className="text-gray-600">
                      {moment(
                        item.outBound[outBoundCount - 1].depDate.split("T")[0]
                      ).format("MMM DD, YYYY")}
                    </div>
                  </span>
                </div>
                <div className="col-span-2 text-center py-3">
                  <div className="progress">
                    <div
                      className="progress-bar progress-bar-striped progress-bar-animated"
                      role="progressbar"
                      aria-valuenow="75"
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: "100%" }}
                    >
                      <span>
                        <FontAwesomeIcon
                          icon={faPlane}
                          className="text-gray-500"
                        />
                      </span>
                    </div>
                  </div>
                  <div>
                    <div className=" text-black font-bold">
                      <ConvertMinsToTime data={item.outBound[0].eft} />
                    </div>
                    <div>
                      {outBoundCount === 1
                        ? "non-stop"
                        : outBoundCount === 2
                        ? "One-Stop"
                        : outBoundCount === 3
                        ? "Two-Stop"
                        : outBoundCount === 4
                        ? "Three-Stop"
                        : null}{" "}
                    </div>
                  </div>
                </div>
                <div className="text-center py-3">
                  <span className="text-black font-bold">
                    <div className="text-gray-600">{toAirport}</div>
                    <div>
                      {convertFrom24To12Format(
                        item.outBound[outBoundCount - 1].reachDate
                      )}
                    </div>
                    <div className="text-gray-600">
                      {moment(
                        item.outBound[outBoundCount - 1].reachDate.split("T")[0]
                      ).format("MMM DD, YYYY")}
                    </div>
                  </span>
                </div>
              </div>
            </div>
            <div className="text-center">
              <div className="mb-2 pt-3">
                <div className=" px-2 pt-0 ">
                  <span className=" font-bold  font-sans text-gray-600">
                    From
                  </span>
                </div>
                <div className="pl-2 col-span-3">
                  <span className="text-lg font-bold  font-sans text-black">
                    <i class="fas fa-rupee-sign fa-sm "></i>{" "}
                    {item.fare.grandTotal.toFixed(0)}
                  </span>
                  <br />
                </div>
              </div>
              <div>
                <Link
                  to={"/Flight/travellersinfo"}
                  state={{ data: item }}
                  style={{ textDecoration: "none" }}
                >
                  <Button variant="contained">Book Now&nbsp;</Button>
                </Link>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default OneWayFlightCard;
