import React from "react";
import Modal from "react-bootstrap/Modal";
import { Typography, Grid, Stack, Card, Button } from "@material-ui/core";
import AirPortData from "../../Api/SampleData";
import moment from "moment";
import { Icon } from "@iconify/react";
import briefcaseoutline from "@iconify/icons-eva/briefcase-outline";
import { styled } from "@material-ui/core/styles";
import { useTheme } from "@material-ui/core/styles";

const DIV = styled(Card)(({ theme }) => ({
  boxShadow: "none",
  textAlign: "center",
  backgroundColor: theme.palette.primary.light,
  [theme.breakpoints.up("md")]: {
    textAlign: "left",
    paddingLeft: 20,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },
}));

const FlightDetailModal = ({
  modal,
  show,
  handleClose,
  convertFrom24To12Format,
}) => {
  const theme = useTheme();
  return (
    <div>
      <Modal
        className="mt-20 overflow-hidden"
        size="xl"
        show={show}
        onHide={handleClose}
      >
        <DIV>
          <Modal.Header
            className="text-black d-flex justify-content-between"
            closeButton
            closeVariant="black"
          >
            <Modal.Title>Flight Details</Modal.Title>
          </Modal.Header>
        </DIV>

        <Modal.Body>
          The baggage information is just for reference. Please Check with
          airline before check-in. For more information, visit the airline's
          official website.
        </Modal.Body>
        <>
          {modal === undefined ? null : (
            <>
              {modal.outBound.map((item, index) => (
                <Stack
                  spacing={{ xs: 2, md: 3 }}
                  columns={{ xs: 4, sm: 8, md: 12 }}
                  key={index}
                >
                  <Stack
                    direction="row"
                    spacing={{ xs: 2, md: 3 }}
                    columns={{ xs: 4, sm: 8, md: 12 }}
                    className="align-items-center justify-content-around"
                  >
                    <Stack>
                      <Stack direction={"row"}>
                        <Stack>
                          <img
                            src={`https://www.travomint.com/resources/images/airline-logo/${item.airline}.png`}
                            className="w-11"
                          />
                        </Stack>
                        <Typography
                          sx={{
                            color: " black",
                            fontWeight: "bold",
                            marginLeft: "5px",
                          }}
                        >
                          {item.airlineName}
                        </Typography>
                        <Typography
                          sx={{
                            color: " black",
                            fontWeight: "bold",
                            marginLeft: "5px",
                          }}
                        >
                          {item.airline}
                        </Typography>
                        <Typography
                          sx={{
                            color: " black",
                            fontWeight: "bold",
                            marginLeft: "5px",
                          }}
                        >
                          {item.flightNo}
                        </Typography>
                      </Stack>
                      <Stack>
                        <Typography>Operated By {item.airlineName}</Typography>
                      </Stack>
                    </Stack>

                    <Stack dircetion="row">
                      <Stack>
                        <Button
                          size="small"
                          sx={{
                            width: "80px",
                            height: "80px",
                            borderRadius: "100px",
                          }}
                        >
                          <Icon
                            icon={briefcaseoutline}
                            width={40}
                            height={40}
                          />
                        </Button>
                        <Typography>Cabin Baggage</Typography>
                      </Stack>
                      <Stack>
                        <Typography>{item.cabinBaggage} Per Person</Typography>
                      </Stack>
                    </Stack>
                    <Stack dircetion="row">
                      <Stack>
                        <Button
                          size="small"
                          sx={{
                            width: "80px",
                            height: "80px",
                            borderRadius: "100px",
                          }}
                        >
                          <Icon
                            icon={briefcaseoutline}
                            width={40}
                            height={40}
                          />
                        </Button>
                        <Typography>Check-In Baggage</Typography>
                      </Stack>
                      <Stack>
                        <Typography>{item.baggage} Per Person</Typography>
                      </Stack>
                    </Stack>
                  </Stack>

                  <Grid
                    spacing={{ xs: 2, md: 3 }}
                    columns={{ xs: 4, sm: 8, md: 12 }}
                    sx={{
                      borderWidth: "1px",
                      borderStyle: "dashed",
                      borderColor: "grey",
                      boxShadow: (theme) => theme.customShadows.z8,
                      borderRadius: "10px",
                    }}
                    className="d-flex justify-content-around align-items-center"
                    style={{ margin: 10 }}
                  >
                    <Stack
                      spacing={{ xs: 2, md: 3 }}
                      columns={{ xs: 4, sm: 8, md: 12 }}
                      className="justify-content-center align-items-center"
                    >
                      <Stack
                        sx={{
                          backgroundColor: "whitesmoke",
                          padding: "10px",
                          borderRadius: "20px",
                          fontSize: 12,
                          marginTop: "10px",
                          width: "60px",
                          marginBottom: "-10px",
                        }}
                        className="justify-content-center align-items-center fw-bold"
                      >
                        <Typography
                          sx={{
                            fontSize: 12,
                            fontWeight: "bold",
                          }}
                        >
                          {item.fromAirport}
                        </Typography>
                      </Stack>
                      <Stack
                        style={{
                          marginBottom: "-20px",
                        }}
                      >
                        <Typography
                          sx={{
                            fontWeight: "bold",
                          }}
                        >
                          {convertFrom24To12Format(item.depDate.split("T")[1])}
                        </Typography>
                      </Stack>
                      <Typography>
                        {AirPortData.filter(
                          (items) => items.airportCode === item.fromAirport
                        ).map((item) => item.cityName)}
                      </Typography>
                      <Typography>
                        {AirPortData.filter(
                          (items) => items.airportCode === item.fromAirport
                        ).map((item) => item.airportName)}
                      </Typography>

                      <Stack sx={{ marginBottom: "10px" }}>
                        <Typography>
                          {moment(item.depDate.split("T")[0]).format(
                            "MMMM Do YYYY"
                          )}
                        </Typography>
                      </Stack>
                    </Stack>
                    <Stack
                      spacing={{ xs: 2, md: 3 }}
                      columns={{ xs: 4, sm: 8, md: 12 }}
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Stack
                        sx={{
                          backgroundColor: "whitesmoke",
                          padding: "10px",
                          borderRadius: "20px",
                          fontSize: 12,
                          marginTop: "10px",
                          width: "60px",
                          marginBottom: "-10px",
                        }}
                        className="justify-content-center align-items-center fw-bold"
                      >
                        <Typography
                          sx={{
                            fontSize: 12,
                            fontWeight: "bold",
                          }}
                        >
                          {item.toAirport}
                        </Typography>
                      </Stack>
                      <Stack
                        style={{
                          marginBottom: "-20px",
                        }}
                      >
                        <Typography
                          sx={{
                            fontWeight: "bold",
                          }}
                        >
                          {convertFrom24To12Format(
                            item.reachDate.split("T")[1]
                          )}
                        </Typography>
                      </Stack>

                      <Typography>
                        {AirPortData.filter(
                          (items) => items.airportCode === item.toAirport
                        ).map((item) => item.cityName)}
                      </Typography>
                      <Typography>
                        {AirPortData.filter(
                          (items) => items.airportCode === item.toAirport
                        ).map((item) => item.airportName)}
                      </Typography>
                      <Stack>
                        <Typography>
                          {moment(item.reachDate.split("T")[0]).format(
                            "MMMM Do YYYY"
                          )}
                        </Typography>
                      </Stack>
                    </Stack>
                  </Grid>
                </Stack>
              ))}
            </>
          )}
        </>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default FlightDetailModal;
