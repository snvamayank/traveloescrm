import React from "react";
import AirPortData from "../../Api/SampleData";
import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";
import { useTheme } from "@material-ui/core/styles";
import { Slider } from "@material-ui/core";

const OneWayFilter = ({ handleFilterAirline, result, bodyData }) => {
  const theme = useTheme();
  return (
    <div
      className="mt-3 ml-3"
      style={{
        border: "1px solid ",
        borderColor: theme.palette.primary.light,
        borderRadius: "10px",
      }}
    >
      <div className="up rounded-2xl mt-4 pb-2">
        <div className="px-2 grid grid-cols-1 ">
          <div className="grid grid-cols-2 px-2 py-2 text-left">
            <p className="mb-0 text-black font-bold font-sans">Filter</p>
            <p className="text-right mb-0 font-sans">Reset All</p>
          </div>
        </div>
      </div>
      <div className=" py-2 grid grid-cols-1 ">
        <div className="grid grid-cols-1 text-left">
          <Disclosure defaultOpen="true">
            {({ open }) => (
              <>
                <Disclosure.Button className="flex up justify-between w-full px-2 py-2  text-sm font-medium text-left text-gray-900 rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                  <span className="text-md font-bold ml-2 font-sans">
                    Stop From{" "}
                    {AirPortData.filter(
                      (items) => items.airportCode === bodyData.departure
                    ).map((ite) => (
                      <> {ite.cityName}</>
                    ))}
                  </span>
                  <ChevronUpIcon
                    className={`${
                      open ? "transform rotate-180" : ""
                    } w-5 h-5 text-gray-900`}
                  />
                </Disclosure.Button>

                <Disclosure.Panel className="px-4 pt-2 pb-2 text-sm text-gray-900">
                  stop
                </Disclosure.Panel>
              </>
            )}
          </Disclosure>
        </div>
      </div>
      <div className=" py-2 grid grid-cols-1 up rounded-2xl ">
        <div className="grid grid-cols-1 text-left">
          <Disclosure defaultOpen="true">
            {({ open }) => (
              <>
                <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                  <span className="text-md ml-2 font-bold font-sans">
                    Price
                  </span>
                  <ChevronUpIcon
                    className={`${
                      open ? "transform rotate-180" : ""
                    } w-5 h-5 text-gray-900`}
                  />
                </Disclosure.Button>

                <Disclosure.Panel className="px-4 pt-2 pb-2 text-sm text-gray-900">
                  {/* <Slider
                                  size="small"
                                  defaultValue={70}
                                  aria-label="Small"
                                  valueLabelDisplay="auto"
                                  marks={marks}
                                /> */}
                  <Slider
                    getAriaLabel={() => "Temperature range"}
                    value={priceRange}
                    onChange={handleChange}
                    valueLabelDisplay="auto"
                    max={MaxFareRange}
                    min={MinfareRange}
                    // getAriaValueText={valuetext}
                  />
                </Disclosure.Panel>
              </>
            )}
          </Disclosure>
        </div>
      </div>

      <div className=" py-2 grid grid-cols-1 up rounded-2xl ">
        <div className="grid grid-cols-1 text-left">
          <Disclosure defaultOpen="true">
            {({ open }) => (
              <>
                <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                  <span className="text-md font-bold ml-2 font-sans">
                    Departure From
                    {AirPortData.filter(
                      (items) => items.airportCode === bodyData.departure
                    ).map((ite) => (
                      <> {ite.cityName}</>
                    ))}
                  </span>
                  <ChevronUpIcon
                    className={`${
                      open ? "transform rotate-180" : ""
                    }w-5 h-5 text-gray-900`}
                  />
                </Disclosure.Button>
                <Disclosure.Panel className="px-2 pt-2 pb-2 text-sm text-gray-900">
                  <div className="grid grid-cols-2   ">
                    <div
                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                      onClick={() => departure("morning")}
                    >
                      <img src={morning} className="w-1/4 " />
                      <span className="text-sh font-sans">5 AM - 12 PM</span>
                    </div>
                    <div
                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                      onClick={() => departure("afternoon")}
                    >
                      <img src={noon} className="w-1/4 " />
                      <span className="text-sh font-sans">12 PM - 6 PM</span>
                    </div>
                  </div>
                  <div className="grid grid-cols-2  mt-4">
                    <div
                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                      onClick={() => departure("evening")}
                    >
                      <img src={evening} className="w-1/4" />
                      <span className="text-sh font-sans">6 PM - 12 AM</span>
                    </div>
                    <div
                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                      onClick={() => departure("night")}
                    >
                      <img src={night} className="w-1/4" />
                      <span className="text-sh font-sans">12 AM - 5 AM</span>
                    </div>
                  </div>
                </Disclosure.Panel>
              </>
            )}
          </Disclosure>
        </div>
      </div>

      <div className=" py-2 grid grid-cols-1 up rounded-2xl">
        <div className="grid grid-cols-1 text-left">
          <Disclosure defaultOpen="true">
            {({ open }) => (
              <>
                <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                  <span className="text-md font-bold ml-2 font-sans">
                    Arrival at{" "}
                    {AirPortData.filter(
                      (items) => items.airportCode === bodyData.arrival
                    ).map((ite) => (
                      <> {ite.cityName}</>
                    ))}
                  </span>
                  <ChevronUpIcon
                    className={`${
                      open ? "transform rotate-180" : ""
                    } w-5 h-5 text-gray-900`}
                  />
                </Disclosure.Button>
                <Disclosure.Panel className="px-2 pt-2 pb-2 text-sm text-gray-900">
                  <div className="grid grid-cols-2   ">
                    <div
                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                      onClick={() => arrival("morning")}
                    >
                      <img src={morning} className="w-1/4 " />
                      <span className="text-sh font-sans">5 AM - 12 PM</span>
                    </div>
                    <div
                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                      onClick={() => arrival("afternoon")}
                    >
                      <img src={noon} className="w-1/4 " />
                      <span className="text-sh font-sans">12 PM - 6 PM</span>
                    </div>
                  </div>
                  <div
                    className="grid grid-cols-2  mt-4"
                    onClick={() => arrival("evening")}
                  >
                    <div className="justify-center d-flex flex-column align-items-center cursor-pointer">
                      <img src={evening} className="w-1/4" />
                      <span className="text-sh font-sans">6 PM - 12 PM</span>
                    </div>
                    <div
                      className="justify-center d-flex flex-column align-items-center cursor-pointer"
                      onClick={() => arrival("night")}
                    >
                      <img src={night} className="w-1/4" />
                      <span className="text-sh font-sans ">12 AM - 5 AM</span>
                    </div>
                  </div>
                </Disclosure.Panel>
              </>
            )}
          </Disclosure>
        </div>
      </div>

      <div className=" py-2 grid grid-cols-1 up rounded-2xl ">
        <div className="grid grid-cols-1 text-left">
          <Disclosure defaultOpen="true">
            {({ open }) => (
              <>
                <Disclosure.Button className="flex justify-between w-full px-2 py-2 text-sm font-medium text-left text-gray-900  rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-white focus-visible:ring-opacity-75">
                  <span className="text-md font-bold font-sans ml-2">
                    Airlines
                  </span>
                  <ChevronUpIcon
                    className={`${
                      open ? "transform rotate-180" : ""
                    } w-5 h-5 text-gray-900`}
                  />
                </Disclosure.Button>
                <Disclosure.Panel className="px-3 pt-1  mt-1  rounded-2xl  pb-2 text-sm text-gray-900">
                  {result.data.airline.map((item, i) => {
                    const filterflightResult = result.data.flightResult.filter(
                      (item) => item.valCarrier == result.data.airline[i].code
                    );
                    const minAirlineValue = Math.min(
                      ...filterflightResult.map((item) => item.fare.grandTotal)
                    );

                    return (
                      <div className="d-flex align-items-center mb-2" key={i}>
                        <div className="d-flex align-items-center ">
                          <input
                            type="checkbox"
                            id="name"
                            className="mr-2 height-mint"
                            style={{
                              height: "15px",
                              width: "15px",
                            }}
                            value={item.code}
                            onClick={(e) => handleFilterAirline(e.target.value)}
                            name="name"
                          />
                        </div>
                        <div className="d-flex align-items-center justify-content-between w-full">
                          <div>
                            <span className="text-xs font-bold font-sans">
                              {item.name} &nbsp;(
                              {filterflightResult.length})
                            </span>
                          </div>
                          <div>
                            <span className="text-xs font-bold font-sans">
                              {" "}
                              <i class="fas fa-rupee-sign fa-sm "></i>
                              &nbsp; {minAirlineValue}
                            </span>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </Disclosure.Panel>
              </>
            )}
          </Disclosure>
        </div>
      </div>
    </div>
  );
};

export default OneWayFilter;
