import { Icon } from "@iconify/react";
import arrowIosDownwardFill from "@iconify/icons-eva/arrow-ios-downward-fill";
// material
import {
  Accordion,
  Typography,
  AccordionSummary,
  AccordionDetails,
} from "@material-ui/core";
// utils
import mockData from "../../../utils/mock-data";
//
import { varFadeIn, MotionInView } from "../../animate";
import { Stack } from "immutable";

// ----------------------------------------------------------------------

const MOCK_FAQS = [...Array(8)].map((_, index) => ({
  id: mockData.id(index),
  value: `panel${index + 1}`,
  heading: `Questions ${index + 1}`,
  detail: mockData.text.description(index),
}));

// ----------------------------------------------------------------------

export default function FaqsList({ agentData, providerData }) {
  return (
    <MotionInView variants={varFadeIn}>
      {agentData.map((accordion) => (
        <Accordion key={accordion.value}>
          <AccordionSummary
            expandIcon={
              <Icon icon={arrowIosDownwardFill} width={20} height={20} />
            }
          >
            <Typography variant="subtitle1">{accordion.username}</Typography>
          </AccordionSummary>
          <AccordionDetails>
            {providerData.data.map((c) => (
              <>
                <Typography>{c.provider}</Typography>
              </>
            ))}
          </AccordionDetails>
        </Accordion>
      ))}
    </MotionInView>
  );
}
