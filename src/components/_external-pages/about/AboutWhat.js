import PropTypes from 'prop-types'
import { Icon } from '@iconify/react'
import roundArrowRightAlt from '@iconify/icons-ic/round-arrow-right-alt'
// material
import { alpha, useTheme, styled } from '@material-ui/core/styles'
import {
  Box,
  Grid,
  Button,
  Container,
  Typography,
  LinearProgress,
} from '@material-ui/core'
// utils
import { fPercent } from '../../../utils/formatNumber'
import mockData from '../../../utils/mock-data'
//
import { MHidden } from '../../@material-extend'
import { varFadeInUp, varFadeInRight, MotionInView } from '../../animate'

// ----------------------------------------------------------------------

const LABEL = ['Development', 'Design', 'Marketing']

const MOCK_SKILLS = [...Array(3)].map((_, index) => ({
  label: LABEL[index],
  value: mockData.number.percent(index),
}))

const RootStyle = styled('div')(({ theme }) => ({
  textAlign: 'center',
  paddingTop: theme.spacing(20),
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('md')]: {
    textAlign: 'left',
  },
}))

// ----------------------------------------------------------------------

ProgressItem.propTypes = {
  progress: PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.number,
  }),
}

function ProgressItem({ progress }) {
  const { label, value } = progress
  return (
    <Box sx={{ mt: 3 }}>
      <Box sx={{ mb: 1.5, display: 'flex', alignItems: 'center' }}>
        <Typography variant="subtitle2">{label}&nbsp;-&nbsp;</Typography>
        <Typography variant="body2" sx={{ color: 'text.secondary' }}>
          {fPercent(value)}
        </Typography>
      </Box>
      <LinearProgress
        variant="determinate"
        value={value}
        sx={{
          '& .MuiLinearProgress-bar': { bgcolor: 'grey.700' },
          '&.MuiLinearProgress-determinate': { bgcolor: 'divider' },
        }}
      />
    </Box>
  )
}

export default function AboutWhat() {
  const theme = useTheme()
  const isLight = theme.palette.mode === 'light'
  const shadow = `-40px 40px 80px ${alpha(
    isLight ? theme.palette.grey[500] : theme.palette.common.black,
    0.48,
  )}`

  return (
    <RootStyle>
      <Container maxWidth="lg">
        <Grid container spacing={3}>
          <Grid item>
            <MotionInView variants={varFadeInRight}>
              <Typography variant="h2" sx={{ mb: 3 }}>
                What is Traveloes?
              </Typography>
            </MotionInView>

            <MotionInView variants={varFadeInRight}>
              <Typography
                sx={{
                  color: (theme) =>
                    theme.palette.mode === 'light'
                      ? 'text.secondary'
                      : 'common.white',
                }}
              >
                Founded in 2017, Traveloes is a pioneer in offering online
                travel services for years with a user base of 5 million.
                Headquarter Based in Waldorf, Maryland (USA) . It operates in
                USA, UK and India currently and It has a dedicated team of
                specialized people who put in tremendous efforts to change the
                scenario of the travel industry. Traveloes offers travelers ease
                of booking travel destinations online at attractive prices.
                While providing constant assistance to the customers.
              </Typography>
              <Typography
                sx={{
                  color: (theme) =>
                    theme.palette.mode === 'light'
                      ? 'text.secondary'
                      : 'common.white',
                }}
              >
                More than 600 notable passenger airlines are operating in the
                world. Some of them operate autonomously, some of them cooperate
                with others, creating alliances to be able to offer more
                destinations. Unfortunately, many low-cost carriers do not have
                cooperation agreements with the big airlines, severely limiting
                the combination options available to travelers.
              </Typography>
              <Typography
                sx={{
                  color: (theme) =>
                    theme.palette.mode === 'light'
                      ? 'text.secondary'
                      : 'common.white',
                }}
              >
                Traveloes removes this limitation by allowing travelers to
                create itineraries from nearly limitless flight combinations.
                They provide services almost 25% cheaper than the competition,
                while in some cases the savings can be as high as 60%.
              </Typography>
              <Typography
                sx={{
                  color: (theme) =>
                    theme.palette.mode === 'light'
                      ? 'text.secondary'
                      : 'common.white',
                }}
              >
                By collecting real-time flight data from countless airlines and
                flight data aggregators and Traveloes stores it in their
                high-end database. For busy consumers, it would take days to
                find the optimal flight combination to match their needs. With
                Traveloes, one can just fill out minor details about travel
                plans - and yes! it is normal if one doesn't know the
                destination yet - the unique and cutting-edge algorithm does all
                the work for you in a matter of seconds, giving out the best
                deals.
              </Typography>
            </MotionInView>
          </Grid>
        </Grid>
      </Container>
    </RootStyle>
  )
}
