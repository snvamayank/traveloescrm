import * as Yup from "yup";
import React, { useState } from "react";
import { Icon } from "@iconify/react";
import { useSnackbar } from "notistack5";
import { useFormik, Form, FormikProvider } from "formik";
import eyeFill from "@iconify/icons-eva/eye-fill";
import closeFill from "@iconify/icons-eva/close-fill";
import eyeOffFill from "@iconify/icons-eva/eye-off-fill";
// material
import {
  Stack,
  TextField,
  IconButton,
  InputAdornment,
  Alert,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core";
import { LoadingButton } from "@material-ui/lab";
// hooks
import useAuth from "../../../hooks/useAuth";
import useIsMountedRef from "../../../hooks/useIsMountedRef";
//
import { MIconButton } from "../../@material-extend";
import { values } from "lodash";
import { useNavigate } from "react-router";

// ----------------------------------------------------------------------

export default function RegisterForm() {
  const navigate = useNavigate();
  const { scrappy_register, getRegisteredData } = useAuth();
  const isMountedRef = useIsMountedRef();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [showPassword, setShowPassword] = useState(false);

  const RegisterSchema = Yup.object().shape({
    currencyPermission: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("First name required"),
    metaPermission: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Last name required"),
    username: Yup.string().required("Email is required"),
    password: Yup.string().required("Password is required"),
  });

  const formik = useFormik({
    initialValues: {
      currencyPermission: "",
      metaPermission: "",
      username: "",
      password: "",
    },
    validationSchema: RegisterSchema,
    onSubmit: async (values, { setErrors, setSubmitting }) => {
      try {
        await scrappy_register(
          values.username,
          values.password,
          values.currencyPermission,
          values.metaPermission
        );
        {
          getRegisteredData.status == false
            ? enqueueSnackbar("Register unsuccessful", {
                variant: "error",
                action: (key) => (
                  <MIconButton size="small" onClick={() => closeSnackbar(key)}>
                    <Icon icon={closeFill} />
                  </MIconButton>
                ),
              })
            : enqueueSnackbar("Register success", {
                variant: "success",
                action: (key) => (
                  <MIconButton
                    size="small"
                    onClick={() => navigate("/dashboard/view-scrappy-user")}
                  >
                    <Icon icon={closeFill} />
                  </MIconButton>
                ),
              });
        }

        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (error) {
        console.error(error);
        if (isMountedRef.current) {
          setErrors({ afterSubmit: error.message });
          setSubmitting(false);
        }
      }
    },
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={3}>
          {errors.afterSubmit && (
            <Alert severity="error">{errors.afterSubmit}</Alert>
          )}

          <Stack direction={{ xs: "column", sm: "row" }} spacing={2}>
            {/* <TextField
              fullWidth
              label="Meta"
              {...getFieldProps("metaPermission")}
              error={Boolean(touched.metaPermission && errors.metaPermission)}
              helperText={touched.metaPermission && errors.metaPermission}
            /> */}
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Currency</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={values.meta}
                label="Meta"
                {...getFieldProps("currencyPermission")}
                error={Boolean(
                  touched.currencyPermission && errors.currencyPermission
                )}
                helperText={
                  touched.currencyPermission && errors.currencyPermission
                }
              >
                <MenuItem value={"online"}>INR</MenuItem>
                <MenuItem value={"skyscanner"}>USD</MenuItem>
                <MenuItem value={"jtrdr"}>CAD</MenuItem>
                <MenuItem value={"jtrdr_in"}>GBP</MenuItem>
              </Select>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Meta</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={values.meta}
                label="Meta"
                {...getFieldProps("metaPermission")}
                error={Boolean(touched.metaPermission && errors.metaPermission)}
                helperText={touched.metaPermission && errors.metaPermission}
              >
                <MenuItem value={"online"}>Online</MenuItem>
                <MenuItem value={"skyscanner"}>Sky Scanner</MenuItem>
                <MenuItem value={"jtrdr"}>Jet Radar</MenuItem>
                <MenuItem value={"jtrdr_in"}>Jet RadarIN</MenuItem>
                <MenuItem value={"jtrdr_ca"}>Jet RadarCA</MenuItem>
                <MenuItem value={"jtrdr_uk"}>Jet RadarUK</MenuItem>
                <MenuItem value={"jtrdr_us"}>Jet RadarUS</MenuItem>
              </Select>
            </FormControl>
          </Stack>

          <TextField
            fullWidth
            autoComplete="username"
            type="username"
            label="username"
            {...getFieldProps("username")}
            error={Boolean(touched.email && errors.email)}
            helperText={touched.email && errors.email}
          />

          <TextField
            fullWidth
            autoComplete="current-password"
            type={showPassword ? "text" : "password"}
            label="Password"
            {...getFieldProps("password")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    edge="end"
                    onClick={() => setShowPassword((prev) => !prev)}
                  >
                    <Icon icon={showPassword ? eyeFill : eyeOffFill} />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            error={Boolean(touched.password && errors.password)}
            helperText={touched.password && errors.password}
          />

          <LoadingButton
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            loading={isSubmitting}
          >
            Add User
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
