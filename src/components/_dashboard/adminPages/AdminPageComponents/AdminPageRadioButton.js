import * as React from "react";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { useDispatch } from "../../../../redux/store";
import { RadioButtonValue } from "../../../../redux/slices/AdmimFieldValues";

const RadioButtonsGroup = () => {
  const [value, setValue] = React.useState("%");
  const dispatch = useDispatch();
  const handleChange = (event) => {
    setValue(event.target.value);
  };
  dispatch(RadioButtonValue(value));
  return (
    <FormControl>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        name="radio-buttons-group"
        row={true}
        value={value}
        onChange={handleChange}
        style={{ marginLeft: 10, marginTop: 2 }}
      >
        <FormControlLabel value="%" control={<Radio />} label="%" />
        <FormControlLabel value="FLAT" control={<Radio />} label="FLAT" />
      </RadioGroup>
    </FormControl>
  );
};
export default RadioButtonsGroup;
