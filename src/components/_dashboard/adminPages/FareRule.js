import React from "react";
import { useSnackbar } from "notistack5";
// material
import { useTheme, styled } from "@material-ui/core/styles";
import {
  Box,
  Grid,
  Container,
  Typography,
  useMediaQuery,
  Button,
  TextField,
  Stack,
  Checkbox,
  Paper,
} from "@material-ui/core";
// utils
import fakeRequest from "../../../utils/fakeRequest";
// components
import Page from "../../../components/Page";
import { makeStyles } from "@material-ui/styles";
import AgentDropDown from "./AdminPageComponents/AgentDropDown";
import ProviderDropDown from "./AdminPageComponents/ProviderDropDown";
import { useSelector, useDispatch } from "../../../redux/store";
import { useEffect, useState } from "react";
import RadioButtonsGroup from "./AdminPageComponents/AdminPageRadioButton";
import useSettings from "../../../hooks/useSettings";
import { datafromTable } from "../../../redux/slices/RemoveManually";
import { PATH_DASHBOARD } from "../../../routes/paths";
import HeaderBreadcrumbs from "../../../components/HeaderBreadcrumbs";
import CartWidget from "../../../components/_dashboard/e-commerce/CartWidget";
import { AppNewInvoice } from "../general-app";
import { hostname } from "src/HostName";
import { DatePicker, LocalizationProvider } from "@material-ui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import useAuth from "src/hooks/useAuth";
import moment from "moment";
import { Icon } from "@iconify/react";
import radioButtonOffOutline from "@iconify/icons-eva/radio-button-off-outline";
import checkmarkCircle2Outline from "@iconify/icons-eva/checkmark-circle-2-outline";
import { FaqsList } from "src/components/_external-pages/faqs";
import { Link } from "react-router-dom";
import arrowheadrightfill from "@iconify/icons-eva/arrowhead-right-fill";
import { useLottie } from "lottie-react";
import Loading from "../../../LottieView/99740-line-loading.json";
import axios from "axios";
// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  minHeight: "100%",
  padding: 40,
  paddingTop: theme.spacing(5),
  paddingBottom: theme.spacing(1),
}));
const useStyles = makeStyles({
  root: {
    background: "linear-gradient(45deg, #3b82f6 20%, #1e3a8a 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "none",
    width: "20rem",
    height: 48,
    marginTop: "20rem",
    padding: "0 32px",
    textDecorationLine: "none",
  },
});
// ----------------------------------------------------------------------

export default function FareRule() {
  const theme = useTheme();
  const { themeStretch } = useSettings();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  const upMd = useMediaQuery(theme.breakpoints.up("xl"));
  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
    ...theme.typography.body2,
    padding: theme.spacing(2),
    display: "flex",
    color: theme.palette.text.secondary,
    borderWidth: 1,
    borderRadius: 10,
    alignItems: "center",
  }));

  /* ---------------------------------------------------------------------------------------------------------------API CALLS------------------------------------------------------------------------------------------------------------------------------------ */

  const [allProviderData, setAllProviderData] = useState([]);
  const [agentData, setAgentData] = useState([]);
  const [isloading, setIsloading] = useState(true);
  const options = {
    animationData: Loading,
    loop: true,
    autoplay: true,
  };
  const { View } = useLottie(options);
  /* +++++++++++++++++++++++++++++++++++++++++++ Get Provider Data ++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  const ApiCall = async () => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    const fetchingData = await fetch(
      `${hostname}get/apiprovider`,
      requestOptions
    );
    // const fetchingData = await fetch(
    //   `${hostname}get/apiprovider`,
    //   requestOptions
    // );
    const ApiResp = await fetchingData.json();

    if (ApiResp) {
      setAllProviderData(ApiResp);
      setIsloading(false);
    }
  };

  const mergeApiCalls = () => {
    ApiCall();
    GetAgentData();
  };

  useEffect(() => mergeApiCalls(), []);
  /* ++++++++++++++++++++++++++++++++++++++++ Get Agent Data +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  const GetAgentData = async () => {
    // var requestOptions = {
    //   method: "GET",
    //   redirect: "follow",
    // };
    // const fetchingData = await fetch(
    //   `${hostname}get/b2bAgents`,
    //   requestOptions
    // );
    // const ApiResp = await fetchingData.json();
    // if (ApiResp) {
    //   setAgentData(ApiResp);
    //   setIsloading(false);
    // }
    const response = await axios.post(`${hostname}get/b2bAgents`, {
      userid: user.userid,
    });
    console.log("response", response);
    setAgentData(response.data.agents);
    setIsloading(false);
  };

  /* ++++++++++++++++++++++++++++++++++++++ Make New rule ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

  /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

  if (isloading) {
    return <Box sx={{ mb: 5 }}>{View}</Box>;
  }

  return (
    <Page title="make new rule | Traveloes">
      <Container maxWidth={themeStretch ? false : "xxxl"} spacing={1}>
        <HeaderBreadcrumbs
          heading="Choose Agent"
          links={[
            { name: "Dashboard", href: PATH_DASHBOARD.root },
            {
              name: "Fare Rule",
              href: PATH_DASHBOARD.farerule.farerule,
            },
          ]}
        />

        <>
          <Box
            sx={{
              height: "5rem",
            }}
          >
            <Grid container spacing={3}>
              {agentData.map((item) => (
                <Grid>
                  <Link
                    to="/dashboard/applyrule"
                    state={{
                      allProviderData,
                      name: item.username,
                      userid: item.userid,
                      agentData: agentData,
                    }}
                    style={{
                      textDecoration: "none",
                      width: 300,
                      margin: 1,
                      backgroundColor: "#eee",
                    }}
                  >
                    <Item
                      sx={{
                        justifyContent: "space-between",
                        alignItems: "center",
                        padding: 1.5,
                        width: 320,
                        margin: 1,
                        height: 100,
                      }}
                    >
                      {" "}
                      <img
                        src={`/static/brand/${item.logo}`}
                        // className="w-40"
                        width="50px"
                      />
                      <Typography variant="subtitle1">
                        {item.username}
                      </Typography>
                      <Icon icon={arrowheadrightfill} width={20} height={20} />
                    </Item>
                  </Link>
                </Grid>
              ))}
            </Grid>
          </Box>
        </>
      </Container>
    </Page>
  );
}
