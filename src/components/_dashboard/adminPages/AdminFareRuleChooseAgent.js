import React from "react";
import { useSnackbar } from "notistack5";
// material
import { useTheme, styled } from "@material-ui/core/styles";
import {
  Box,
  Grid,
  Container,
  Typography,
  useMediaQuery,
  Button,
  TextField,
  Stack,
  Checkbox,
  Paper,
} from "@material-ui/core";
// utils
// components
import Page from "../../../components/Page";
import { makeStyles } from "@material-ui/styles";
import { useEffect, useState } from "react";
import useSettings from "../../../hooks/useSettings";
import { PATH_DASHBOARD } from "../../../routes/paths";
import HeaderBreadcrumbs from "../../../components/HeaderBreadcrumbs";
import { hostname } from "src/HostName";
import { Icon } from "@iconify/react";
import { FaqsList } from "src/components/_external-pages/faqs";
import { Link } from "react-router-dom";
import arrowheadrightfill from "@iconify/icons-eva/arrowhead-right-fill";
import { useLottie } from "lottie-react";
import Loading from "../../../LottieView/99740-line-loading.json";
import axios from "axios";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
import ToggleOffIcon from "@mui/icons-material/ToggleOff";
// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  minHeight: "100%",
  padding: 40,
  paddingTop: theme.spacing(5),
  paddingBottom: theme.spacing(1),
}));
const useStyles = makeStyles({
  root: {
    background: "linear-gradient(45deg, #3b82f6 20%, #1e3a8a 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "none",
    width: "20rem",
    height: 48,
    marginTop: "20rem",
    padding: "0 32px",
    textDecorationLine: "none",
  },
});
// ----------------------------------------------------------------------

export default function FareRule() {
  const theme = useTheme();
  const { themeStretch } = useSettings();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  // console.log("user", user);
  const upMd = useMediaQuery(theme.breakpoints.up("xl"));
  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
    ...theme.typography.body2,
    padding: theme.spacing(2),
    display: "flex",
    color: theme.palette.text.secondary,
    borderWidth: 1,
    borderRadius: 10,
    alignItems: "center",
  }));

  /* ---------------------------------------------------------------------------------------------------------------API CALLS------------------------------------------------------------------------------------------------------------------------------------ */

  const [allProviderData, setAllProviderData] = useState([]);
  const [agentData, setAgentData] = useState([]);
  const [isloading, setIsloading] = useState(true);
  const [farerule, setFarerule] = useState([]);
  const [filterfarerule, setfilterfarerule] = useState([]);
  const options = {
    animationData: Loading,
    loop: true,
    autoplay: true,
  };
  const { View } = useLottie(options);
  /* +++++++++++++++++++++++++++++++++++++++++++ Get Provider Data ++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  const ApiCall = async () => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    const fetchingData = await fetch(
      `${hostname}get/apiprovider`,
      requestOptions
    );
    const ApiResp = await fetchingData.json();
    if (ApiResp) {
      setAllProviderData(ApiResp);
      setIsloading(false);
    }
  };

  const getfareRule = async () => {
    const fetchingRule = await axios.get(`${hostname}get/farerule/info`);
    setFarerule(fetchingRule.data.data);
  };

  const filterRule = (agentData, farerule) => {
    if (agentData !== 0 && farerule !== 0) {
      let filterData = [];
      filterData = farerule.filter((el) => {
        return agentData.find((element) => {
          return element.userid === el.userid;
        });
      });
      setfilterfarerule(filterData);
    }
  };


  const mergeApiCalls = () => {
    ApiCall();
    GetAgentData();
    getfareRule();
  };

  /* ++++++++++++++++++++++++++++++++++++++++ Get Agent Data +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  const GetAgentData = async () => {
    const response = await axios.post(`${hostname}get/b2bAgents`, {
      userid: user.userid,
    });
    setAgentData(response.data.agents);
    setIsloading(false);
  };

  useEffect(() => mergeApiCalls(), []);
  useEffect(
    () => filterRule(agentData, farerule),
    [farerule.length > 0, agentData.length > 0]
  );

  if (isloading) {
    return <Box sx={{ mb: 5 }}>{View}</Box>;
  }

  return (
    <Page title="make new rule | Traveloes">
      <Container maxWidth={themeStretch ? false : "xxxl"} spacing={1}>
        <HeaderBreadcrumbs
          heading="Choose Agent"
          links={[
            { name: "Dashboard", href: PATH_DASHBOARD.root },
            {
              name: "Fare Rule",
              href: PATH_DASHBOARD.farerule.farerule,
            },
          ]}
        />

        <>
          <Box
            sx={{
              height: "5rem",
            }}
          >
            <Grid container spacing={3}>
              {agentData.map((item) => {
                return (
                  <Grid>
                    {filterfarerule
                      .filter((filteuser) => filteuser.userid === item.userid)
                      .map((items) => {
                        return (
                          <Item
                            sx={{
                              padding: 1.5,
                              width: 300,
                              margin: 1,
                            }}
                          >
                            <div className=" w-full">
                              <div className="d-flex align-items-center justify-content-between w-full">
                                <div className="">
                                  <img
                                    src={`/static/brand/${item.logo}`}
                                    width="50px"
                                  />
                                </div>

                                <Typography variant="subtitle1">
                                  {item.username}
                                </Typography>
                                <Icon
                                  icon={arrowheadrightfill}
                                  width={20}
                                  height={20}
                                />
                              </div>
                              <div className="d-flex align-items-center justify-content-between mt-3">
                                <Link
                                  to="/dashboard/AppliedRule"
                                  state={{
                                    allProviderData,
                                    name: item.username,
                                    userid: item.userid,
                                    agentData: agentData,
                                  }}
                                  style={{ textDecoration: "none" }}
                                >
                                  <div className="d-flex align-items-center">
                                    <Button>Flight</Button>
                                    <div className="text-center">
                                      {items.isactive ? (
                                        <ToggleOffIcon
                                          style={{
                                            color: "green",
                                            fontSize: "25px",
                                          }}
                                        />
                                      ) : (
                                        <ToggleOffIcon
                                          style={{
                                            color: "grey",
                                            fontSize: "25px",
                                          }}
                                        />
                                      )}
                                    </div>
                                  </div>
                                </Link>
                                <Link
                                  to="/dashboard/checkAgenthotelRules"
                                  state={{
                                    name: item.username,
                                    userid: item.userid,
                                  }}
                                  style={{ textDecoration: "none" }}
                                >
                                  <Button>Hotels</Button>
                                </Link>
                              </div>
                            </div>
                          </Item>
                        );
                      })}
                  </Grid>
                );
              })}
            </Grid>
          </Box>
        </>
      </Container>
    </Page>
  );
}
