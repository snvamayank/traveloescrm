import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

// core components

import axios from "axios";
import {
  Card,
  CardHeader,
  Stack,
  Select,
  MenuItem,
  Button,
  Input,
  Grid,
  TextField,
  CardContent,
  Paper,
  CardActions,
  Typography,
  Box,
} from "@material-ui/core";
import { Divider } from "@mui/material";
import Page from "src/components/Page";
import { scrapping_Host } from "src/HostName";
import { styled } from "@mui/material/styles";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));
DepartureFlight.propTypes = {
  item: PropTypes.array,
  departFare: PropTypes.array,
  ChangeDepartDetails: PropTypes.func,
  AddMoreDepartFlightDetails: PropTypes.func,
  ChangeDepartFare: PropTypes.func,
  validate: PropTypes.bool,
};
export default function DepartureFlight({
  item,
  ChangeDepartDetails,
  AddMoreDepartFlightDetails,
  departFare,
  ChangeDepartFare,
  validate,
  filteredDataSource,
  showAirport,
  setShowAirport,
  originAirport,
  destinationAirport,
  setOriginAirport,
  setDestinationAirport,
  departureAirportCode,
  arrivalAirportCode,
  setDepartureAirportCode,
  setAirportName,
  setArrivalAirportCode,
  ClickAirport,
}) {
  return (
    <Box>
      {item.map((x, i) => (
        <>
          <Stack
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 5,
            }}
          >
            <Typography variant="h3" sx={{ marginLeft: 5 }}>
              {i + 1}.Departure
            </Typography>
            <Button
              sx={{ marginRight: 5 }}
              variant={"contained"}
              onClick={() => AddMoreDepartFlightDetails()}
            >
              Add another Stop
            </Button>
          </Stack>
          <Box sx={{ flexGrow: 1 }}>
            <Grid
              container
              spacing={{ xs: 1, md: 1 }}
              columns={{ xs: 4, sm: 8, md: 12 }}
            >
              <Grid columns={{ xs: 4, sm: 8, md: 12 }}>
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      variant={"outlined"}
                      id="DepartureFlightDate"
                      name="DepartureFlightDate"
                      type="date"
                      // value={originAirport}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                      sx={{ width: "20rem", margin: 2 }}
                    />
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      variant={"outlined"}
                      label="DepartureFlightTime (16:00)"
                      id="DepartureFlightTime"
                      name="DepartureFlightTime"
                      // value={x.DepartureFlightTime}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                      sx={{ width: "20rem", margin: 2 }}
                    />
                    {validate === true && (
                      <>
                        {x.DepartureFlightTime.length > 5 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter the Time{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      variant={"outlined"}
                      // label="ArrivalFlightDate (MM/DD/YYYY)"
                      id="ArrivalFlightDate"
                      name="ArrivalFlightDate"
                      type="date"
                      // value={x.ArrivalFlightDate}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                      sx={{ width: "20rem", margin: 2 }}
                    />
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ArrivalFlightTime (16:00)"
                      id="ArrivalFlightTime"
                      name="ArrivalFlightTime"
                      value={x.ArrivalFlightTime}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        {x.ArrivalFlightTime.length > 5 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter the Time{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3} className="position-relative">
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="DepartureAirportCode (LAX)"
                      id="DepartureAirportCode"
                      name="DepartureAirportCode"
                      value={x.DepartureAirportCode}
                      onClick={() => setShowAirport(3)}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    <ul
                      style={{
                        position: "absolute",
                        top: "74px",
                        maxHeight: "450px",
                        overflowY: "scroll",
                        width: "100%",
                      }}
                      className="menuflitem-5  pl-0  pr-0  z-50
                bg-white shadow rounded "
                    >
                      {showAirport === 3 && (
                        <Stack
                          sx={{
                            justifyContent: "center",
                            overflow: "hidden",
                          }}
                        >
                          {filteredDataSource.map((item) => (
                            <Stack
                              sx={{
                                // width: "22rem",
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "space-between",
                              }}
                              onClick={(e) => {
                                ClickAirport(
                                  {
                                    value: item.airportCode,
                                    name: "DepartureAirportCode",
                                  },
                                  i
                                );
                                setDepartureAirportCode(item.airportCode);
                              }}
                              // onClick={() =>
                              // }
                            >
                              <Typography
                                onClick={() => setShowAirport(0)}
                                className="pl-2 pt-2"
                              >
                                {item.airportName}
                              </Typography>
                              <Typography
                                onClick={() => setShowAirport(0)}
                                className="pe-1 pt-2"
                              >
                                {item.airportCode}
                              </Typography>
                            </Stack>
                          ))}
                        </Stack>
                      )}
                    </ul>

                    {validate === true && (
                      <>
                        {x.DepartureCityName.length > 3 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter the Departure Code{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack>
                  <Stack xs={12} sm={12} md={3} className="position-relative">
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ArrivalAirportCode (MIA)"
                      id="ArrivalAirportCode2"
                      name="ArrivalAirportCode"
                      value={x.ArrivalAirportCode}
                      onClick={() => setShowAirport(4)}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {
                      <ul
                        style={{
                          position: "absolute",
                          top: "74px",
                          maxHeight: "450px",
                          overflowY: "scroll",
                          width: "100%",
                        }}
                        className="menuflitem-5  pl-0  pr-0  z-50
                bg-white shadow rounded "
                      >
                        {showAirport === 4 && (
                          <Stack
                            sx={{
                              justifyContent: "center",
                              overflow: "hidden",
                              // height: "400px",
                            }}
                          >
                            {filteredDataSource.map((item) => (
                              <Stack
                                sx={{
                                  width: "22rem",
                                  display: "flex",
                                  flexDirection: "row",
                                  justifyContent: "space-between",
                                }}
                                onClick={() => {
                                  {
                                    ClickAirport(
                                      {
                                        value: item.airportCode,
                                        name: "ArrivalAirportCode",
                                      },
                                      i
                                    );
                                    setArrivalAirportCode(item.airportCode);
                                  }
                                }}
                              >
                                <Typography
                                  onClick={() => setShowAirport(0)}
                                  className="pl-2 pt-2"
                                >
                                  {item.airportName}
                                </Typography>
                                <Typography
                                  onClick={() => setShowAirport(0)}
                                  className="pe-4 pt-2"
                                >
                                  {item.airportCode}
                                </Typography>
                              </Stack>
                            ))}
                          </Stack>
                        )}
                      </ul>
                    }

                    {validate === true && (
                      <>
                        {x.ArrivalCityName.length > 3 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter the Arrival Airport Code{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack>
                  {/* <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="DepartureAirportName (San Francisco International Airport)"
                      id="DepartureAirportName"
                      name="DepartureAirportName"
                      value={x.DepartureAirportName}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter airport name{" "}
                        </Typography>
                      </>
                    )}
                  </Stack> */}
                </Item>
              </Grid>
              {/* <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  {" "}
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ArrivalAirportCode (MIA)"
                      id="ArrivalAirportCode2"
                      name="ArrivalAirportCode"
                      value={x.ArrivalAirportCode}
                      onClick={() => setShowAirport(4)}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {
                      <ul
                        style={{
                          position: "absolute",
                          top: "160px",
                        }}
                        className="menuflitem-5  pl-0  pr-0  z-50
                bg-white shadow rounded "
                      >
                        {showAirport === 4 && (
                          <Stack
                            sx={{
                              justifyContent: "center",
                              overflow: "hidden",
                              height: "50px",
                            }}
                          >
                            {filteredDataSource.map((item) => (
                              <Stack
                                sx={{
                                  width: "22rem",
                                  display: "flex",
                                  flexDirection: "row",
                                  justifyContent: "space-between",
                                }}
                                onClick={() =>
                                  setArrivalAirportCode(item.airportCode)
                                }
                              >
                                <Typography onClick={() => setShowAirport(0)}>
                                  {item.airportCode}
                                </Typography>
                                <Typography onClick={() => setShowAirport(0)}>
                                  {item.airportName}
                                </Typography>
                              </Stack>
                            ))}
                          </Stack>
                        )}
                      </ul>
                    }

                    {validate === true && (
                      <>
                        {x.ArrivalCityName.length > 3 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter the Arrival Airport Code{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ArrivalAirportName (Los angeles)"
                      id="ArrivalAirportName"
                      name="ArrivalAirportName"
                      value={x.ArrivalAirportName}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter airport name{" "}
                        </Typography>
                      </>
                    )}
                  </Stack>
                </Item>
              </Grid> */}
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="AirlineCode (6-E)"
                      id="AirlineCode"
                      name="AirlineCode"
                      value={x.AirlineCode}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter airline code{" "}
                        </Typography>
                      </>
                    )}
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="FlightNo (2154)"
                      id="FlightNo"
                      name="FlightNo"
                      value={x.FlightNo}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter Flight no{" "}
                        </Typography>
                      </>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="DepartureTerminal (T-3)"
                      id="DepartureTerminal"
                      name="DepartureTerminal"
                      value={x.DepartureTerminal}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter terminal{" "}
                        </Typography>
                      </>
                    )}
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ArrivalTerminal (T-3)"
                      id="ArrivalTerminal"
                      name="ArrivalTerminal"
                      value={x.ArrivalTerminal}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter Arrival terminal{" "}
                        </Typography>
                      </>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="AirlineName (British Airways)"
                      id="AirlineName"
                      name="AirlineName"
                      value={x.AirlineName}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter airline name{" "}
                        </Typography>
                      </>
                    )}
                  </Stack>
                  {/* <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="CabinBaggage (27kg)"
                      id="CabinBaggage"
                      name="CabinBaggage"
                      value={x.CabinBaggage}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter cabin baggage name{" "}
                        </Typography>
                      </>
                    )}
                  </Stack> */}
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="Duration (400) "
                      id="Duration"
                      name="Duration"
                      value={x.Duration}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="Baggage (7kg)"
                      id="Baggage"
                      name="Baggage"
                      value={x.Baggage}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter baggage{" "}
                        </Typography>
                      </>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="CheckIn (5 Kgs)"
                      id="CheckIn"
                      name="CheckIn"
                      value={x.CheckIn}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  {/* <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="Duration (400) "
                      id="Duration"
                      name="Duration"
                      value={x.Duration}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </Stack> */}
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="CabinBaggage (27kg)"
                      id="CabinBaggage"
                      name="CabinBaggage"
                      value={x.CabinBaggage}
                      onChange={(e) => ChangeDepartDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        <Typography variant="subtitle2" sx={{ color: " red" }}>
                          Please enter cabin baggage name{" "}
                        </Typography>
                      </>
                    )}
                  </Stack>
                </Item>
              </Grid>
            </Grid>
          </Box>
          <Divider />
        </>
      ))}
      <Box sx={{ flexGrow: 1 }}>
        <Typography variant="h3" sx={{ marginLeft: 5, marginTop: 5 }}>
          Fare
        </Typography>
        <Grid
          container
          spacing={{ xs: 1, md: 1 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          <Grid
            item
            spacing={{ xs: 1, md: 1 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            <Item>
              {" "}
              <Stack>
                <TextField
                  sx={{ width: "20rem", margin: 2 }}
                  variant={"outlined"}
                  label="TotalFare"
                  id="TotalFare"
                  name="TotalFare"
                  value={departFare.TotalFare}
                  onChange={(e) => ChangeDepartFare(e, "TotalFare")}
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
                {validate === true && (
                  <>
                    <Typography variant="subtitle2" sx={{ color: " red" }}>
                      Please enter TotalFare{" "}
                    </Typography>
                  </>
                )}
              </Stack>
            </Item>
          </Grid>
          <Grid
            item
            spacing={{ xs: 1, md: 1 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            <Item>
              <Stack xs={12} sm={12} md={3}>
                <TextField
                  sx={{ width: "20rem", margin: 2 }}
                  variant={"outlined"}
                  label="BaseFare"
                  id="BaseFare"
                  name="BaseFare"
                  value={departFare.BaseFare}
                  onChange={(e) => ChangeDepartFare(e, "BaseFare")}
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
                {validate === true && (
                  <>
                    <Typography variant="subtitle2" sx={{ color: " red" }}>
                      Please enter Base Fare{" "}
                    </Typography>
                  </>
                )}
              </Stack>
            </Item>
          </Grid>
          <Grid
            item
            spacing={{ xs: 1, md: 1 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            <Item>
              {" "}
              <Stack xs={12} sm={12} md={3}>
                <TextField
                  sx={{ width: "20rem", margin: 2 }}
                  variant={"outlined"}
                  label="Taxes"
                  id="Surcharge"
                  name="Surcharge"
                  value={departFare.Surcharge}
                  onChange={(e) => ChangeDepartFare(e, "Surcharge")}
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
                {validate === true && (
                  <>
                    <Typography variant="subtitle2" sx={{ color: " red" }}>
                      Please enter taxes{" "}
                    </Typography>
                  </>
                )}
              </Stack>
            </Item>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
