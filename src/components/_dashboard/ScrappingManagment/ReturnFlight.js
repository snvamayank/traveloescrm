import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

// core components

import axios from "axios";
import {
  Stack,
  Button,
  Grid,
  TextField,
  Paper,
  Typography,
  Box,
  Divider,
} from "@material-ui/core";
import Page from "src/components/Page";
import { scrapping_Host } from "src/HostName";
import { styled } from "@mui/material/styles";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

DepartureFlight.propTypes = {
  item: PropTypes.array,
  returnFare: PropTypes.array,
  ChangeReturnDetails: PropTypes.func,
  AddMoreReturnFlightDetails: PropTypes.func,
  ChangeReturnFare: PropTypes.func,
  validate: PropTypes.bool,
};
export default function DepartureFlight({
  returnDetails,
  ChangeReturnFare,
  returnFare,
  ChangeReturnDetails,
  AddMoreReturnFlightDetails,
  validate,
  setShowAirport,
  filteredDataSource,
  ArrivalCityName,
  setArrivalReturnAirportCode,
  arrivalReturnAirportCode,
  setArrivalCityName,
  showAirport,
  setReturnAirportCode,
  returnAirportCode,
  ClickReturnAirport,
}) {
  return (
    <Box>
      {returnDetails.map((x, i) => (
        <>
          <Stack
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 5,
            }}
          >
            <Typography variant="h3" sx={{ marginLeft: 5 }}>
              {i + 1}.Return
            </Typography>
            <Button
              sx={{ marginRight: 5 }}
              variant={"contained"}
              onClick={() => AddMoreReturnFlightDetails()}
            >
              Add another Stop
            </Button>
          </Stack>
          <Box sx={{ flexGrow: 1 }}>
            <Grid
              container
              spacing={{ xs: 1, md: 1 }}
              columns={{ xs: 4, sm: 8, md: 12 }}
            >
              <Grid columns={{ xs: 4, sm: 8, md: 12 }}>
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      variant={"outlined"}
                      // label="DepartureFlightDate (MM/DD/YYYY)"
                      id="ReturnFlightDate"
                      name="ReturnFlightDate"
                      type="date"
                      // value={x.ReturnFlightDate}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                      sx={{ width: "20rem", margin: 2 }}
                    />
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      variant={"outlined"}
                      label="ReturnFlightTime (16:00)"
                      id="ReturnFlightTime"
                      name="ReturnFlightTime"
                      // value={x.ReturnFlightTime}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                      sx={{ width: "20rem", margin: 2 }}
                    />
                    {validate === true && (
                      <>
                        {x.ReturnFlightTime.length > 3 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter return flight time{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      variant={"outlined"}
                      // label="ArrivalFlightDate (MM/DD/YYYY)"
                      id="ArrivalFlightDate"
                      name="ArrivalFlightDate"
                      type="date"
                      // value={x.ArrivalFlightDate}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                      sx={{ width: "20rem", margin: 2 }}
                    />
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ArrivalFlightTime (16:00)"
                      id="ArrivalFlightTime"
                      name="ArrivalFlightTime"
                      value={x.ArrivalFlightTime}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <>
                        {x.ArrivalFlightTime.length > 3 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter arrival time{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3} className="position-relative">
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ReturnAirportCode (LAX)"
                      id="ReturnDepAirportCode"
                      name="ReturnDepAirportCode"
                      onClick={() => setShowAirport(5)}
                      value={x.ReturnDepAirportCode}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    <ul
                      style={{
                        position: "absolute",
                        top: "74px",
                        maxHeight: "400px",
                        overflowY: "scroll",
                        width: "100%",
                      }}
                      className="menuflitem-5  pl-0  pr-0  z-50
                bg-white shadow rounded "
                    >
                      {showAirport === 5 && (
                        <Stack
                          sx={{
                            justifyContent: "center",
                            overflow: "hidden",
                            // height: "50px",
                          }}
                        >
                          {filteredDataSource.map((item) => (
                            <Stack
                              sx={{
                                // width: "22rem",
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "space-between",
                              }}
                              onClick={() => {
                                ClickReturnAirport(
                                  {
                                    value: item.airportCode,
                                    name: "ReturnDepAirportCode",
                                  },
                                  i
                                );
                                setReturnAirportCode(item.airportCode);
                              }}
                            >
                              <Typography
                                onClick={() => setShowAirport(0)}
                                className="pl-2 pt-2"
                              >
                                {item.airportName}
                              </Typography>
                              <Typography
                                onClick={() => setShowAirport(0)}
                                className="pe-1 pt-2"
                              >
                                {item.airportCode}
                              </Typography>
                            </Stack>
                          ))}
                        </Stack>
                      )}
                    </ul>

                    {validate === true && (
                      <>
                        {x.ReturnCityName.length > 3 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter return airpot code{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack>
                  <Stack xs={12} sm={12} md={3} className="position-relative">
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ReturnAirportCode (MIA)"
                      id="ReturnArrivalAirportCode2"
                      name="ReturnArrivalAirportCode"
                      onClick={() => setShowAirport(6)}
                      value={x.ReturnArrivalAirportCode}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    <ul
                      style={{
                        position: "absolute",
                        top: "74px",
                        maxHeight: "400px",
                        overflowY: "scroll",
                        width: "100%",
                      }}
                      className="menuflitem-5  pl-0  pr-0  z-50
                bg-white shadow rounded "
                    >
                      {showAirport === 6 && (
                        <Stack
                          sx={{
                            justifyContent: "center",
                            overflow: "hidden",
                            // height: "50px",
                          }}
                        >
                          {filteredDataSource.map((item) => (
                            <Stack
                              sx={{
                                // width: "22rem",
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "space-between",
                              }}
                              onClick={() => {
                                ClickReturnAirport(
                                  {
                                    value: item.airportCode,
                                    name: "ReturnArrivalAirportCode",
                                  },
                                  i
                                );
                                setArrivalReturnAirportCode(item.airportCode);
                              }}
                            >
                              <Typography
                                onClick={() => setShowAirport(0)}
                                className="pl-2 pt-2"
                              >
                                {item.airportName}
                              </Typography>
                              <Typography
                                onClick={() => setShowAirport(0)}
                                className="pe-1 pt-2"
                              >
                                {item.airportCode}
                              </Typography>
                            </Stack>
                          ))}
                        </Stack>
                      )}
                    </ul>

                    {validate === true && (
                      <>
                        {x.ArrivalCityName.length > 3 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter arrival airport code{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack>
                  {/* <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="DepartureAirportName (San Francisco International Airport)"
                      id="DepartureAirportName"
                      name="DepartureAirportName"
                      value={x.DepartureAirportName}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter departure airport name{" "}
                      </Typography>
                    )}
                  </Stack> */}
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  {" "}
                  {/* <Stack xs={12} sm={12} md={3} className="position-relative">
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ReturnAirportCode (MIA)"
                      id="ReturnArrivalAirportCode2"
                      name="ReturnArrivalAirportCode"
                      onClick={() => setShowAirport(6)}
                      // value={arrivalReturnAirportCode}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    <ul
                      style={{
                        position: "absolute",
                        top: "74px",
                        maxHeight: "400px",
                        overflowY: "scroll",
                        width: "100%",
                      }}
                      className="menuflitem-5  pl-0  pr-0  z-50
                bg-white shadow rounded "
                    >
                      {showAirport === 6 && (
                        <Stack
                          sx={{
                            justifyContent: "center",
                            overflow: "hidden",
                            height: "50px",
                          }}
                        >
                          {filteredDataSource.map((item) => (
                            <Stack
                              sx={{
                                width: "22rem",
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "space-between",
                              }}
                              onClick={() =>
                                setArrivalReturnAirportCode(item.airportCode)
                              }
                            >
                              <Typography onClick={() => setShowAirport(0)}>
                                {item.airportCode}
                              </Typography>
                              <Typography onClick={() => setShowAirport(0)}>
                                {item.airportName}
                              </Typography>
                            </Stack>
                          ))}
                        </Stack>
                      )}
                    </ul>

                    {validate === true && (
                      <>
                        {x.ArrivalCityName.length > 3 ? (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            You have exceed the limits of entry
                          </Typography>
                        ) : (
                          <Typography
                            variant="subtitle2"
                            sx={{ color: " red" }}
                          >
                            Please enter arrival airport code{" "}
                          </Typography>
                        )}
                      </>
                    )}
                  </Stack> */}
                  {/* <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ArrivalAirportName (Los angeles)"
                      id="ArrivalAirportName"
                      name="ArrivalAirportName"
                      value={x.ArrivalAirportName}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter arrival airport code{" "}
                      </Typography>
                    )}
                  </Stack> */}
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="AirlineCode (6E)"
                      id="AirlineCode"
                      name="AirlineCode"
                      value={x.AirlineCode}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter airline code{" "}
                      </Typography>
                    )}
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="FlightNo (2154)"
                      id="FlightNo"
                      name="FlightNo"
                      value={x.FlightNo}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter flight no{" "}
                      </Typography>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ReturnTerminal (T-3)"
                      id="ReturnTerminal"
                      name="ReturnTerminal"
                      value={x.ReturnTerminal}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter return terminal{" "}
                      </Typography>
                    )}
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="ArrivalTerminal (T-3)"
                      id="ArrivalTerminal"
                      name="ArrivalTerminal"
                      value={x.ArrivalTerminal}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter arrival terminal{" "}
                      </Typography>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="CheckIn (7kg)"
                      id="CheckIn"
                      name="CheckIn"
                      value={x.CheckIn}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter check in{" "}
                      </Typography>
                    )}
                  </Stack>
                  <Stack>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="CabbinBaggage (27kg)"
                      id="CabbinBaggage"
                      name="CabbinBaggage"
                      value={x.CabbinBaggage}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter cabin baggage{" "}
                      </Typography>
                    )}
                  </Stack>
                </Item>
              </Grid>
              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="Baggage (7 kg)"
                      id="Baggage"
                      name="Baggage"
                      value={x.Baggage}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter baggage{" "}
                      </Typography>
                    )}
                  </Stack>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="Duration (400) "
                      id="Duration"
                      name="Duration"
                      value={x.Duration}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter duration{" "}
                      </Typography>
                    )}
                  </Stack>
                </Item>
              </Grid>

              <Grid
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Item>
                  <Stack xs={12} sm={12} md={3}>
                    <TextField
                      sx={{ width: "20rem", margin: 2 }}
                      variant={"outlined"}
                      label="AirlineName (British Airways)"
                      id="AirlineName"
                      name="AirlineName"
                      value={x.AirlineName}
                      onChange={(e) => ChangeReturnDetails(e, i)}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                    {validate === true && (
                      <Typography variant="subtitle2" sx={{ color: " red" }}>
                        Please enter airline name{" "}
                      </Typography>
                    )}
                  </Stack>
                </Item>
              </Grid>
            </Grid>
          </Box>
          <Divider />
        </>
      ))}
      {/* <Box sx={{ flexGrow: 1 }}>
        <Typography variant="h3" sx={{ marginLeft: 5, marginTop: 5 }}>
          Fare
        </Typography>
        <Grid
          container
          spacing={{ xs: 1, md: 1 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          <Grid
            item
            spacing={{ xs: 1, md: 1 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            <Item>
              {" "}
              <Stack>
                <TextField
                  sx={{ width: "20rem", margin: 2 }}
                  variant={"outlined"}
                  label="TotalFare"
                  id="TotalFare"
                  name="TotalFare"
                  value={returnFare.TotalFare}
                  onChange={(e) => ChangeReturnFare(e, "TotalFare")}
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
                {validate === true && (
                  <Typography variant="subtitle2" sx={{ color: " red" }}>
                    Please enter total fare{" "}
                  </Typography>
                )}
              </Stack>
            </Item>
          </Grid>
          <Grid
            item
            spacing={{ xs: 1, md: 1 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            <Item>
              <Stack xs={12} sm={12} md={3}>
                <TextField
                  sx={{ width: "20rem", margin: 2 }}
                  variant={"outlined"}
                  label="BaseFare"
                  id="BaseFare"
                  name="BaseFare"
                  value={returnFare.BaseFare}
                  onChange={(e) => ChangeReturnFare(e, "BaseFare")}
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
                {validate === true && (
                  <Typography variant="subtitle2" sx={{ color: " red" }}>
                    Please enter base fare{" "}
                  </Typography>
                )}
              </Stack>
            </Item>
          </Grid>
          <Grid
            item
            spacing={{ xs: 1, md: 1 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            <Item>
              {" "}
              <Stack xs={12} sm={12} md={3}>
                <TextField
                  sx={{ width: "20rem", margin: 2 }}
                  variant={"outlined"}
                  label="Taxes"
                  id="Surcharge"
                  name="Surcharge"
                  value={returnFare.Surcharge}
                  onChange={(e) => ChangeReturnFare(e, "Surcharge")}
                  formControlProps={{
                    fullWidth: true,
                  }}
                />
                {validate === true && (
                  <Typography variant="subtitle2" sx={{ color: " red" }}>
                    Please enter taxes{" "}
                  </Typography>
                )}
              </Stack>
            </Item>
          </Grid>
        </Grid>
      </Box> */}
    </Box>
  );
}
