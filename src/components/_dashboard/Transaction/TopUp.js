import * as Yup from "yup";

import { useSnackbar } from "notistack5";
import { useFormik, Form, FormikProvider } from "formik";
// material
import { useTheme, styled } from "@material-ui/core/styles";
import {
  Box,
  Card,
  Grid,
  Container,
  Typography,
  useMediaQuery,
  Button,
  Input,
} from "@material-ui/core";
// utils
import fakeRequest from "../../../utils/fakeRequest";
// components
import Page from "../../../components/Page";
import { makeStyles } from "@material-ui/styles";

import {
  WalletSummary,
  WalletMethods,
  WalletBillingAddress,
} from "../Transaction/index";
import { BookingDetails } from "../general-booking";
import { useEffect, useState } from "react";
import { hostname } from "src/HostName";
import useAuth from "src/hooks/useAuth";
import HeaderBreadcrumbs from "src/components/HeaderBreadcrumbs";
import { PATH_DASHBOARD } from "src/routes/paths";
import useSettings from "src/hooks/useSettings";

// ----------------------------------------------------------------------

const RootStyle = styled(Page)(({ theme }) => ({
  minHeight: "200%",
  padding: 50,
  paddingTop: theme.spacing(5),
  paddingBottom: theme.spacing(10),
}));
const useStyles = makeStyles({
  root: {
    background: "linear-gradient(45deg, #3b82f6 20%, #1e3a8a 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "none",
    width: "20rem",
    height: 48,
    marginTop: "20rem",
    padding: "0 32px",
    textDecorationLine: "none",
  },
});
// ----------------------------------------------------------------------

export default function Payment() {
  const { enqueueSnackbar } = useSnackbar();
  const theme = useTheme();
  const classes = useStyles();
  const [walletBalanceData, setwalletBalanceData] = useState([]);
  const [isloading, setIsloading] = useState([]);
  const [userId, setUserId] = useState("");
  // const { user } = useAuth();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  const { themeStretch } = useSettings();
  const upMd = useMediaQuery(theme.breakpoints.up("md"));
  const getAlltranscation = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      userid: user.userid,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(`${hostname}all/agent/transactions`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result) {
          setwalletBalanceData(result);
          setIsloading(false);
        }
      })
      .catch((error) => console.log("error"));
  };

  useEffect(() => getAlltranscation(), []);

  const PaymentSchema = Yup.object().shape({
    amount: Yup.string().required("Add any amount "),
    // phone: Yup.string().required("Phone is required"),
    // email: Yup.string()
    //   .email("Email must be a valid email address")
    //   .required("Email is required"),
    // address: Yup.string().required("Address is required"),
  });

  const formik = useFormik({
    initialValues: {
      amount: "",
      method: "",
    },
    validationSchema: PaymentSchema,
    onSubmit: async (values, { resetForm }) => {
      const submitData = {
        amount: values.amount,
      };
      await fakeRequest(500);
      if (values.method === "Net banking") {
        alert(
          JSON.stringify(
            {
              ...submitData,
              method: values.method,
            },
            null,
            2
          )
        );
      } else if (values.method === "cash") {
        alert(
          JSON.stringify(
            {
              ...submitData,
              method: values.method,
            },
            null,
            2
          )
        );
      } else if (values.method === "Cheque") {
        alert(
          JSON.stringify(
            {
              ...submitData,
              method: values.method,
            },
            null,
            2
          )
        );
      }

      resetForm();
      enqueueSnackbar("Payment success", { variant: "success" });
    },
  });
  const handleChange = (event) => {
    setUserId(event.target.value);
  };
  return (
    <RootStyle title="Wallet Topup | Traveloes">
      <Container maxWidth={themeStretch ? false : "xxxl"} spacing={1}>
        <HeaderBreadcrumbs
          heading="Add Balance in Wallet "
          links={[
            { name: "Dashboard", href: PATH_DASHBOARD.root },
            {
              name: "Top Up",
              href: PATH_DASHBOARD.TopUpMain.topup,
            },
          ]}
        />
        <Card>
          <FormikProvider value={formik}>
            <Form noValidate autoComplete="off" onSubmit={formik.handleSubmit}>
              <Grid container spacing={upMd ? 10 : 6}>
                <Grid item xs={12} md={4}>
                  <WalletBillingAddress formik={formik} ID={userId} />
                </Grid>
                <Grid item xs={12} md={4}>
                  <WalletMethods formik={formik} />
                </Grid>
              </Grid>
            </Form>
          </FormikProvider>
          {isloading ? (
            <Typography>Wait checking transactions</Typography>
          ) : (
            <BookingDetails
              walletdata={walletBalanceData}
              isloading={isloading}
            />
          )}
        </Card>
      </Container>
    </RootStyle>
  );
}
