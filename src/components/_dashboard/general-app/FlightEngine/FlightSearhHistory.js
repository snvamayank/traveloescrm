import { Icon } from "@iconify/react";
import { useEffect, useRef, useState } from "react";
import { sentenceCase } from "change-case";
import { Link as RouterLink } from "react-router-dom";
import shareFill from "@iconify/icons-eva/share-fill";
import printerFill from "@iconify/icons-eva/printer-fill";
import archiveFill from "@iconify/icons-eva/archive-fill";
import downloadFill from "@iconify/icons-eva/download-fill";
import trash2Outline from "@iconify/icons-eva/trash-2-outline";
import moreVerticalFill from "@iconify/icons-eva/more-vertical-fill";
import Label from "../../../../components/Label";
// material
import { useTheme } from "@material-ui/core/styles";
import arrowIosForwardFill from "@iconify/icons-eva/arrow-ios-forward-fill";
import {
  Box,
  Menu,
  Card,
  Table,
  Button,
  Divider,
  MenuItem,
  TableRow,
  TableBody,
  TableCell,
  TableHead,
  Typography,
  CardHeader,
  TableContainer,
} from "@material-ui/core";
import useAuth from "src/hooks/useAuth";
import Loading from "../../../../LottieView/99740-line-loading.json";
// utils
// import { fCurrency } from "../../../../utils/formatNumber";
//
// import PropTypes from "prop-types";

// import Label from "../../Label";
import Scrollbar from "../../../Scrollbar";
import { MIconButton } from "../../../@material-extend";
// import { useDispatch, useSelector } from "../../../redux/store";
// import { removeRuleFromTable } from "../../.././redux/slices/RemoveManually";
import moment from "moment";
import axios from "axios";
import { hostname } from "src/HostName";
import { useLottie } from "lottie-react";
// ----------------------------------------------------------------------

// const FlightHistory  = [...Array(5)].map((_, index) => ({
//   From: window.From,
//   To: window.To,
//   Type: window.Type,
//   Departure:window.Departure,
//   Arrival:window.Arrival,
//   Class:window.Class
// }));

// ----------------------------------------------------------------------

function MoreMenuButton() {
  const menuRef = useRef(null);
  const [open, setOpen] = useState(false);
  //   const dispatch = useDispatch();
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  /*  */
  return (
    <>
      <>
        <MIconButton ref={menuRef} size="large" onClick={handleOpen}>
          <Icon icon={moreVerticalFill} width={20} height={20} />
        </MIconButton>
      </>

      <Menu
        open={open}
        anchorEl={menuRef.current}
        onClose={handleClose}
        PaperProps={{
          sx: { width: 200, maxWidth: "100%" },
        }}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
      >
        <MenuItem>
          <Icon icon={downloadFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Download
          </Typography>
        </MenuItem>
        <MenuItem>
          <Icon icon={printerFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Print
          </Typography>
        </MenuItem>
        <MenuItem>
          <Icon icon={shareFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Share
          </Typography>
        </MenuItem>
        <MenuItem>
          <Icon icon={archiveFill} width={20} height={20} />
          <Typography variant="body2" sx={{ ml: 2 }}>
            Archive
          </Typography>
        </MenuItem>

        <Divider />
        <MenuItem sx={{ color: "error.main" }}>
          <Icon icon={trash2Outline} width={20} height={20} />
          {/* onclick={dispatch(removeRuleFromTable())}  */}
          {/*  */}
          <Button /*  onClick={() => dispatch(removeRuleFromTable())} */>
            Delete
          </Button>
        </MenuItem>
      </Menu>
    </>
  );
}
// FlightSearchHistory.propTypes = {
//   RuleData: PropTypes.object,
// };

export default function FlightSearchHistory({ loading, RuleData }) {
  const theme = useTheme();
  const isLight = theme.palette.mode === "light";

  const [ruleData, setRuleData] = useState([]);
  const [isloading, setIsloading] = useState(true);
  //   const [isloading, setIsloading] = useState(true); const [ruleData, setRuleData] = useState([]);
  // const { user } = useAuth();
  const LoginData = localStorage.getItem("LoginData");
  const user = JSON.parse(LoginData);
  const GetAllRuleMaded = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      userid: user.userid,
    });
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    const fetching = await fetch(
      `${hostname}get/agent/search/history`,
      requestOptions
    );
    const RuleFetchedOverHere = await fetching.json();

    if (RuleFetchedOverHere) {
      setRuleData(RuleFetchedOverHere);
      setIsloading(false);
    }
  };

  useEffect(() => GetAllRuleMaded(), []);

  const options = {
    animationData: Loading,
    loop: true,
    autoplay: true,
  };
  const { View } = useLottie(options);
  function sortHistory(a, b) {
    if (a.createdAt > b.createdAt) {
      return -1;
    } else if (a.createdAt < b.createdAt) {
      return 1;
    }
    return 0;
  }
  ruleData.sort(sortHistory);

  if (isloading) {
    return <div>{View}</div>;
  }

  return (
    <Card>
      <CardHeader title="All Details" sx={{ mb: 3 }} />

      <Scrollbar>
        <TableContainer sx={{ minWidth: 720 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>From</TableCell>
                <TableCell>To</TableCell>
                <TableCell>Type</TableCell>
                <TableCell sx={{ ml: 2 }}>Departure</TableCell>
                <TableCell sx={{ ml: 2 }}>Arrival</TableCell>
              </TableRow>
            </TableHead>
            {/* {RuleData.} */}
            {ruleData.map((row) => (
              <TableRow>
                <TableCell>{row.flyingfrom}</TableCell>
                <TableCell>{row.flyingto}</TableCell>
                <TableCell>
                  <Label
                    color={
                      (ruleData.triptype === "2" && "success") || "success"
                    }
                  >
                    {row.triptype} way
                  </Label>
                </TableCell>
                {row.triptype === "2" ? (
                  <>
                    <TableCell>
                      <Label>{row.onewaydate}</Label>{" "}
                    </TableCell>
                    <TableCell>
                      <Label>{row.twowaydate}</Label>
                    </TableCell>
                  </>
                ) : (
                  <TableCell>
                    <Label>{row.onewaydate}</Label>
                  </TableCell>
                )}
              </TableRow>
            ))}
            {/* <button>fsdfsdfsd</button> */}

            <TableBody>
              {/* <TableCell align="right">
                    <MoreMenuButton />
                  </TableCell> */}
            </TableBody>
          </Table>
        </TableContainer>
      </Scrollbar>

      <Divider />

      <Box sx={{ p: 2, textAlign: "right" }}>
        <Button
          to="#"
          size="small"
          color="inherit"
          component={RouterLink}
          endIcon={<Icon icon={arrowIosForwardFill} />}
        >
          View All
        </Button>
      </Box>
    </Card>
  );
}
/*   const mergePostApiAndDispatchFunc= () => {
    PostApi()
    dispatc
  } */
/*  {ruleData.data.map((row) => (
                <TableRow>
                  <TableCell>{row.flyingfrom}</TableCell>
                  <TableCell>{row.flyingto}</TableCell>
                  {/* <TableCell>
                    <Label
                      color={
                        (ruleData.Type === "success" && "success") ||
                        "success"
                      }
                    >
                      success
                    </Label>
                  </TableCell> 
                   <TableCell>
                    <Label>{row.onewaydate.split("T")[0]}</Label>
                    <Label>{row.twowaydate.split("T")[1]}</Label>
                   </TableCell>
                   <TableCell>
                    <Label>
                      {row.amount}
                      {"  "}
                      {row.rule}
                    </Label>
                  </TableCell> 
                 </TableRow>
              ))} */
