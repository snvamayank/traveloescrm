import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import { Link } from "react-router-dom";
// import plane from "../../Image/plane6.gif";
import { useSelector } from "react-redux";
import { SelectedData } from "../redux/slices/Action";
import moment from "moment";
import {
  Typography,
  Card,
  CardContent,
  Box,
  Container,
  Grid,
  Stack,
  Button,
  Slider,
  Divider,
} from "@material-ui/core";

const TwoFooter = (props) => {
  const { first, second } = props;
  const [Result, setResult] = useState([]);
  const [AirResult, setAirResult] = useState([]);
  const [show1, setShow1] = useState(false);
  const [trans, setTrans] = useState(0);

  const handleClose1 = () => setShow1(false);
  function openModal(e) {
    setShow1(true);
    setTrans(e);
  }
  // ----------------------------Reduux-----------------------------------
  // const data = useSelector(SelectedData);

  const bodyDataa = localStorage.getItem("body");
  const data = JSON.parse(bodyDataa);

  /* -----------------------------------------------time convert Function---------------------------------------------------------------- */
  const ConvertMinsToTime = ({ data }) => {
    let hours = Math.floor(data / 60);
    let minutes = data % 60;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    return `${hours}h:${minutes}m`;
  };
  /* -----------------------------------------------time convert Function---------------------------------------------------------------- */
  return (
    <>
      <Grid
        container
        spacing={2}
        style={{ position: "fixed", bottom: 0, right: 0 }}
        className="bg-gray-900 bg-opacity-80 text-white"
      >
        <Grid item xs={12} xl={4} md={6}>
          <div>
            <h4 className="text-xs mb-1">Departure</h4>
            {first === undefined ? null : (
              <>
                {first.outBound.map((items, i) => {
                  return (
                    <div>
                      <div className="d-flex justify-content-between">
                        <p>{items.toAirport}</p> <p>{items.fromAirport}</p>
                      </div>
                      <div className="d-flex justify-content-between">
                        <p>{moment(items.depDate).format("MMM DD, YYYY")}</p>
                        <p>{moment(items.reachDate).format("MMM DD, YYYY")}</p>
                      </div>
                      <div className="d-flex justify-content-between">
                        <p>{moment(items.depDate).format("HH MM A")}</p>
                        <p></p>
                      </div>
                    </div>
                  );
                })}
              </>
            )}
          </div>
        </Grid>
        <Grid item xs={12} xl={4} md={6}>
          <div>
            <h4 className="text-xs mb-1">Return</h4>
            {second === undefined ? null : (
              <>
                {second.inBound.map((item) => {
                  return (
                    <div>
                      <div className="d-flex justify-content-between">
                        <p>{item.toAirport}</p> <p>{item.fromAirport}</p>
                      </div>
                      <div className="d-flex justify-content-between">
                        <p>{moment(item.depDate).format("MMM DD, YYYY")}</p>
                        <p>{moment(item.reachDate).format("MMM DD, YYYY")}</p>
                      </div>
                      <div className="d-flex justify-content-between">
                        <p>{moment(item.depDate).format("HH MM A")}</p>
                        <p></p>
                      </div>
                    </div>
                  );
                })}
              </>
            )}
            <p className="text-xs font-bold mt-0 mb-1"></p>
          </div>
        </Grid>
        <Grid item xs={12} xl={4} md={6}></Grid>
      </Grid>
    </>
  );
};

export default TwoFooter;
