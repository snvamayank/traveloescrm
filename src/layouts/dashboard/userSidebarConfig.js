// routes
import { PATH_DASHBOARD, PATH_PAGE } from "../../routes/paths";
// components
import Label from "../../components/Label";
import SvgIconStyle from "../../components/SvgIconStyle";
import useAuth from "src/hooks/useAuth";

// ----------------------------------------------------------------------
const getIcon = (name) => (
  <SvgIconStyle
    src={`/static/icons/navbar/${name}.svg`}
    sx={{ width: "100%", height: "100%" }}
  />
);

const ICONS = {
  blog: getIcon("ic_blog"),
  cart: getIcon("ic_cart"),
  chat: getIcon("ic_chat"),
  mail: getIcon("ic_mail"),
  user: getIcon("ic_user"),
  kanban: getIcon("ic_kanban"),
  banking: getIcon("ic_banking"),
  calendar: getIcon("ic_calendar"),
  ecommerce: getIcon("ic_ecommerce"),
  analytics: getIcon("ic_analytics"),
  dashboard: getIcon("ic_dashboard"),
  booking: getIcon("ic_booking"),
  home: getIcon("ic_home"),
  rule: getIcon("ic_rules"),
  topUp: getIcon("ic_topup"),
  transaction: getIcon("ic_transaction"),
  airport: getIcon("ic_airport"),
  home: getIcon("ic_home"),
  wallet: getIcon("ic_wallet"),
  bookingConfirmed: getIcon("ic_booking-confirmed"),
};

const sidebarConfig = [
  // GENERAL
  {
    subheader: "Dashboard",
    items: [
      {
        title: "Dashboard",
        path: PATH_DASHBOARD.root,
        icon: ICONS.home,
      },
      // { title: 'banking', path: PATH_DASHBOARD.general.banking, icon: ICONS.banking },
      // { title: 'booking', path: PATH_DASHBOARD.general.booking, icon: ICONS.booking }
    ],
  },

  // ----------------------------------------------------------------------
  {
    subheader: "Bookings",
    items: [
      {
        title: "Book Flights",
        path: PATH_PAGE.flightBooking,
        icon: ICONS.airport,
      },
      {
        title: "Bookings",
        path: PATH_DASHBOARD.history.book,
        icon: ICONS.bookingConfirmed,
      },
      {
        title: "Cancellations",
        path: PATH_DASHBOARD.general.cancellations,
        icon: ICONS.banking,
      },
    ],
  },

  // MANAGEMENT
  // ----------------------------------------------------------------------

  // APP
  // ----------------------------------------------------------------------
  // {
  //   subheader: 'Wallet',
  //   items: [
  //     {
  //       title: 'Balance',
  //       //  path: PATH_DASHBOARD.mail.root,
  //       path: PATH_PAGE.comingSoon,
  //       icon: ICONS.wallet,
  //     },
  //     {
  //       title: 'Top Up',
  //       path: PATH_DASHBOARD.TopUpMain.topup,
  //       icon: ICONS.topUp,
  //     },
  //     {
  //       title: 'Your Transaction',
  //       path: PATH_DASHBOARD.AccTrans.agentLedger,
  //       icon: ICONS.transaction,
  //     },
  //   ],
  // },

  {
    subheader: "fareRule",
    items: [
      {
        title: "AllFareRule",
        //  path: PATH_DASHBOARD.mail.root,
        path: PATH_DASHBOARD.farerule.checkAgentRules,
        icon: ICONS.rule,
      },
    ],
  },
  {
    subheader: "Booking History",
    items: [
      // {
      //   title: "Hotels Booking ",
      //   path: PATH_DASHBOARD.BookingHistory.hotelsBooking,
      //   icon: ICONS.rule,
      // },
      {
        title: "Flight Booking ",
        path: PATH_DASHBOARD.BookingHistory.FlightBooking,
        icon: ICONS.rule,
      },
    ],
  },
  {
    subheader: "About Us",
    items: [
      {
        title: "Privacy Policy",
        path: PATH_DASHBOARD.aboutus.privacy,
        icon: ICONS.rule,
      },
      {
        title: "Terms and Conditions",
        path: PATH_DASHBOARD.aboutus.terms,
        icon: ICONS.rule,
      },
    ],
  },
];

export default sidebarConfig;
