import axios from "axios";
import { scrapping_Host } from "src/HostName";

export const replicateCallApi = (
  fromDate,
  toDate,
  id,
  handleClose1,
  setRefresh,
  refresh
) => {
  const datas = {
    id: id,
    fromDate: fromDate,
    toDate: toDate,
  };
  var options = {
    method: "POST",
    url: `${scrapping_Host}replicate/records`,
    headers: { "Content-Type": "application/json" },
    data: datas,
  };
  axios
    .request(options)
    .then(function (response) {
      if (response.data.status === 200) {
        alert("Data replicated");
        handleClose1();
        setRefresh(!refresh);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
};
