export const MONTH = [
  {
    id: 1,
    value: 1,
    month: "January",
  },
  {
    id: 2,
    value: 2,
    month: "February",
  },
  {
    id: 3,
    value: 3,
    month: "March",
  },
  {
    id: 4,
    value: 4,
    month: "April",
  },
  {
    id: 5,
    value: 5,
    month: "May",
  },
  {
    id: 6,
    value: 6,
    month: "June",
  },
  {
    id: 7,
    value: 7,
    month: "July",
  },
  {
    id: 8,
    value: 8,
    month: "August",
  },
  {
    id: 9,
    value: 9,
    month: "September",
  },
  {
    id: 10,
    value: 10,
    month: "October",
  },
  {
    id: 11,
    value: 11,
    month: "November",
  },
  {
    id: 12,
    value: 12,
    month: "December",
  },
];
