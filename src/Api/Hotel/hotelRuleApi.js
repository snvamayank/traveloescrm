import axios from "axios";
import { useEffect, useState } from "react";
import { hotelhostname } from "src/HostName";

export const addHotelRule = async (hotelRule, userid, gdsType) => {
  const bodyData = {
    userid: userid,
    gdsType: gdsType,
    hotelMarkup: hotelRule,
    hotelMarkup: {
      ruleName: hotelRule.ruleName,
      fromDate: hotelRule.fromDate,
      toDate: hotelRule.toDate,
      rooms: Number(hotelRule.rooms),
      nights: Number(hotelRule.nights),
      pax: Number(hotelRule.pax),
      markupType: hotelRule.markupType,
      markupAmount: Number(hotelRule.markupAmount),
    },
  };
  try {
    const result = await axios.post(
      `${hotelhostname}/add/hotels/farerule`,
      bodyData
    );
    return result.data;
  } catch (error) {
    console.log("error", error);
    return error.name;
  }
};

export const ruleUpdateApi = async (updateRule, userid, gdsType) => {
  const bodyData = {
    userid: userid,
    gdsType: gdsType,
    ruleName: updateRule.ruleName,
    hotelMarkup: {
      ruleName: updateRule.ruleName,
      fromDate: updateRule.fromDate,
      toDate: updateRule.toDate,
      rooms: Number(updateRule.rooms),
      nights: Number(updateRule.nights),
      pax: Number(updateRule.pax),
      markupType: updateRule.markupType,
      markupAmount: Number(updateRule.markupAmount),
    },
  };
  try {
    const result = await axios.patch(
      `${hotelhostname}/update/hotels/farerule`,
      bodyData
    );
    return result.data;
  } catch (error) {
    console.log("error", error);
    return error.name;
  }
};

export const ruleDeleteAPi = async (deleteRule, userid, gdsType) => {
  const bodyData = {
    userid: userid,
    gdsType: gdsType,
    ruleName: deleteRule.ruleName,
  };
  try {
    const result = await axios.patch(
      `${hotelhostname}/delete/hotel/farerule`,
      bodyData
    );
    return result.data;
  } catch (error) {
    console.log("error", error);
    return error.name;
  }
};
