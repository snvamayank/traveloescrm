import { hotelhostname, hostname } from "src/HostName";

export const gethotelsBooking = `${hotelhostname}/get/hotel/booking`;
export const userFullBookingHistory = `${hotelhostname}/user/hotel/fullbooking`;
export const gethotelfarerule = `${hostname}get/farerule/info`;