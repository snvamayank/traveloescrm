import axios from "axios";
import { useEffect, useState } from "react";

export const useGetFlightBoooking = (url) => {
  const [flightData, setFlightData] = useState({
    loading: true,
    error: null,
    data: {},
  });
  useEffect(() => {
    (async () => {
      try {
        const result = await axios.post(url);
        setFlightData({ ...flightData, data: result.data, loading: false });
      } catch (error) {
        console.log("error.message", error.message);
        setFlightData({ ...flightData, error: error.message, loading: false });
      }
    })();
  }, []);
  return flightData;
};

export const useTicketVerfiy = (url) => {
  const [result, setresult] = useState({
    data: {},
    error: null,
  });
  const verfiy = async (item) => {
    const body = {
      searchID: item.sessionId,
      supp: 52,
    };
    try {
      const data = await axios.post(url, body);
      if (data.data.msg !== "") {
        alert(data.data.msg);
      }
    } catch (error) {
      console.log("error", error.name);
      setresult({ ...result, error: error.name });
    }
  };
  return { result, verfiy };
};
